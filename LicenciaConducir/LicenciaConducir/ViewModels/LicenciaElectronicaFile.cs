﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LicenciaConducir.ViewModels
{
   public class LicenciaElectronicaFile
    {
        public string uniqueIdField { get; set; }

        public string nombreField { get; set; }

        public string baseurlField { get; set; }

        public string tipoField { get; set; }

        public byte[] archivoField { get; set; }

        public string codeResultField { get; set; }

        public string descResultField { get; set; }
    }
}
