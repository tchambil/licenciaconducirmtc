﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq; 
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using LicenciaConducir.Lib.MTCEntidadComplementaria;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Maps; 

namespace LicenciaConducir.ViewModels
{
    public class MapPageViewModel : INotifyPropertyChanged
    {
        private ObservableCollection<EntidadComplementaria> _items;

        public ObservableCollection<EntidadComplementaria> items
        {
            get => _items;
            set
            {
                _items = value;
                OnPropertyChanged(nameof(items));
            }
        }

        public MapPageViewModel()
        {
            MessagingCenter.Subscribe<MapSpan>(this, "searchMarkers", (lastVisibleRegion) => {
                //consultarItems(LastVisibleRegion);
                consultarItemstTipo(MTCEComplementariaApp._tipo.ToString());
            });
            MessagingCenter.Subscribe<string>(this, "searchMarkersTipo", consultarItemstTipo);
            showLastMarket = true;
        }

        private MapSpan _visibleView;
        public MapSpan VisibleView
        {
            get => _visibleView;
            set
            {
                _visibleView = value;
                OnPropertyChanged(nameof(VisibleView));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private bool showLastMarket;
        protected async void consultarItemstTipo(string tipo)
        {

            try
            {
                ObservableCollection<EntidadComplementaria> itemss = new ObservableCollection<EntidadComplementaria>();
                     items = tipo == "0" ? MTCEComplementariaApp.vListaEntidades : new ObservableCollection<EntidadComplementaria>(MTCEComplementariaApp.vListaEntidades.Where(c => c.Tipo == tipo));
              

            }
            catch (Exception e)
            {
                await App.Current.MainPage.DisplayAlert("Error", "No se pudo conectar a la red. Por favor verifique su conexión a internet", "Aceptar");
            }

        }

    }
}
