﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace LicenciaConducir.ViewModels
{
    public class Ciudadano
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
   
        public string ClaseCategoria { get; set; }

        public string Estado { get; set; }

        public string FechaEmision { get; set; }

        public string FechaExpedicion { get; set; }

        public string FechaRevalidacion { get; set; }

        public string MensajeVecimiento { get; set; } 
        public string NombresCompletos { get; set; }

        public string NumeroCodAdministrado { get; set; }
        [Indexed]
        public string NumeroDocumento { get; set; }
        [Indexed]
        public string NumeroLicencia { get; set; }

        public string TipoDocumento { get; set; }
        public string Password { get; set; }
        public string PathFile { get; set; }
        public byte[] File { get; set; } 
        public DateTime CreationTime { get; set; }
        public Ciudadano()
        {
            CreationTime = DateTime.Now;
        }
    }
}
