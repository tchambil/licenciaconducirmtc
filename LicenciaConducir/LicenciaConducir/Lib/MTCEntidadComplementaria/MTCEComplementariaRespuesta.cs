﻿ 
using MTC.ServicioEntidadComple.Dominio.Entidades;
  
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace LicenciaConducir.Lib.MTCEntidadComplementaria
{
    public enum MTCEComplementariaRespuestaEstado
    {
        DatoVacio = 0,
        NumeroIncorecto = 1,
        ErrorInternet = 2,
        ErrorSistema = 3,
        NoEncontrado = 4,
        EsperandoRespuesta = 9,
        InformacionObtenida = 10
    }

    public class MTCEComplementariaRespuesta
    {
       
        public MTCEComplementariaRespuestaEstado EstadoRespuesta { get; set; }
        public ObservableCollection<EntidadComplementaria> lEntidadComplementaria
        {
            get; private set;
        }
        public MTCEComplementariaRespuesta()
        {
            EstadoRespuesta = MTCEComplementariaRespuestaEstado.InformacionObtenida;
            lEntidadComplementaria = new ObservableCollection<EntidadComplementaria>();
        }
        public MTCEComplementariaRespuesta(MTCEComplementariaRespuestaEstado resultado)
        {
            EstadoRespuesta = resultado;
        }
        public MTCEComplementariaRespuesta(MTCEComplementariaRespuestaEstado resultado, TM_Entidad[] result = null)
        {
            EstadoRespuesta = resultado;
            if (result != null)
            {
                lEntidadComplementaria = new ObservableCollection<EntidadComplementaria>();             
                foreach (var item in result)
                {                    
                    var Entidad = new EntidadComplementaria();
                    Entidad.Region = item.Departamento;
                    Entidad.Provincia = item.Provincia;
                    Entidad.Distrito = item.Distrito;
                    Entidad.RazonSocial = item.RazonSocial;
                    Entidad.Ubicacion = item.Direccion; 
                    Entidad.Latitud = item.Latitud;
                    Entidad.Longitud = item.Longitud;
                    Entidad.Tipo = item.tipoEntidad.ToString();
                    Entidad.Id = item.IdEntididad.ToString();
                    lEntidadComplementaria.Add(Entidad);
                }
            }
        }
        public MTCEComplementariaRespuesta(MTCEComplementariaRespuestaEstado resultado, TM_LocalTerminal[] result = null)
        {

            EstadoRespuesta = resultado;
            if (result != null)
            {
                lEntidadComplementaria = new ObservableCollection<EntidadComplementaria>();
                foreach (var item in result)
                {
                    var Entidad = new EntidadComplementaria();
                    Entidad.Region = item.Departamento;
                    Entidad.Provincia = item.Provincia;
                    Entidad.Distrito = item.Distrito;
                    Entidad.RazonSocial = item.RazonSocial;
                    Entidad.Ubicacion = item.Direccion;
                    Entidad.Latitud = item.Latitud;
                    Entidad.Longitud = item.Longitud; 
                    Entidad.Tipo = "10";// item.tipoEntidad.ToString();
                    Entidad.Id = item.IdLocalTerminal.ToString();
                    lEntidadComplementaria.Add(Entidad);
                }
            }
        }
    }
}
