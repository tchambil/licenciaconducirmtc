﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text; 
using LicenciaConducir.Lib.Include;
using MTC.ServicioEntidadComple.Dominio.Entidades;
using System.ServiceModel;

namespace LicenciaConducir.Lib.MTCEntidadComplementaria
{

    public class MTCEComplementariaApp
    {
        public static int _tipo { get; set; }

        private static TA_EntidadClient _clienteEntidad;
        private static TM_Ubigeo UbigeoRequest;
        public static ObservableCollection<EntidadComplementaria> vListaEntidades { get; set; }
        public static ObservableCollection<EntidadComplementaria> vListaTerminales { get; set; }

        public static MTCEComplementariaRespuesta CurrentResultado; 
        public static TM_Entidad CurrentEntidad { get; internal set; }

        public static event TerminalConsultadoEncontradoDelegado TerminalConsultado;
        public delegate void TerminalConsultadoEncontradoDelegado(MTCEComplementariaRespuesta respuesta);


        public static event EntidadConsultadoEncontradoDelegado EntidadConsultado;
        public delegate void EntidadConsultadoEncontradoDelegado(MTCEComplementariaRespuesta respuesta);

        public static MTCEComplementariaRespuestaEstado ConsultaTerminal()
        {
            if (!Helper.TieneInternet())
            {
                return MTCEComplementariaRespuestaEstado.ErrorInternet;
            }

            _clienteEntidad = new TA_EntidadClient(Helper.ServicioSoapBinding(), new EndpointAddress(Config.Services.ecomplementarias));
            try
            {
                _clienteEntidad.SelTerminalesCompleted += _clienteEntidad_SelTerminalesCompleted; 
                _clienteEntidad.SelTerminalesAsync("5","");
                return MTCEComplementariaRespuestaEstado.EsperandoRespuesta;
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                if (_clienteEntidad.State == CommunicationState.Opened)
                {
                    _clienteEntidad.Close();
                }
                return MTCEComplementariaRespuestaEstado.ErrorSistema;
            }
        }

        private static void _clienteEntidad_SelTerminalesCompleted(object sender, SelTerminalesCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                TerminalConsultado(new MTCEComplementariaRespuesta(MTCEComplementariaRespuestaEstado.ErrorSistema));
                return;
            }
            if (e.Cancelled)
            {
                TerminalConsultado(new MTCEComplementariaRespuesta(MTCEComplementariaRespuestaEstado.ErrorSistema));
                return;
            }
            if (e.Result == null)
            {
                TerminalConsultado(new MTCEComplementariaRespuesta(MTCEComplementariaRespuestaEstado.NoEncontrado));
                return;
            }

            CurrentResultado = new MTCEComplementariaRespuesta(MTCEComplementariaRespuestaEstado.InformacionObtenida, e.Result);
            TerminalConsultado(CurrentResultado);
            if (_clienteEntidad.State == CommunicationState.Opened)
            {
                _clienteEntidad.Close();
            }

        }

        public  static MTCEComplementariaRespuestaEstado ConsultarEntidadGeolocal()
        {
             
            if (!Helper.TieneInternet())
            {
                return MTCEComplementariaRespuestaEstado.ErrorInternet;
            }
            UbigeoRequest = new TM_Ubigeo();
            UbigeoRequest.tipoEntidad = _tipo;

            _clienteEntidad = new TA_EntidadClient(Helper.ServicioSoapBinding(), new EndpointAddress(Config.Services.ecomplementarias));
            try
            {
                _clienteEntidad.SelEntidadesCompletsTodoCompleted +=_SelEntidadesCompletsTodoCompleted;
                _clienteEntidad.SelEntidadesCompletsTodoAsync(UbigeoRequest);
                return MTCEComplementariaRespuestaEstado.EsperandoRespuesta;
            }
            catch(Exception ex)
            {
                Console.Write(ex);
                if (_clienteEntidad.State == CommunicationState.Opened)
                {
                    _clienteEntidad.Close();
                }
                return MTCEComplementariaRespuestaEstado.ErrorSistema;
            } 
        }

        private static void _SelEntidadesCompletsTodoCompleted(object sender, SelEntidadesCompletsTodoCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                EntidadConsultado(new MTCEComplementariaRespuesta(MTCEComplementariaRespuestaEstado.ErrorSistema));
                return;
            }
            if (e.Cancelled)
            {
                EntidadConsultado(new MTCEComplementariaRespuesta(MTCEComplementariaRespuestaEstado.ErrorSistema));
                return;
            }
            if (e.Result == null)
            {
                EntidadConsultado(new MTCEComplementariaRespuesta(MTCEComplementariaRespuestaEstado.NoEncontrado));
                return;
            }

            CurrentResultado = new MTCEComplementariaRespuesta(MTCEComplementariaRespuestaEstado.InformacionObtenida, e.Result);
            EntidadConsultado(CurrentResultado);
            if (_clienteEntidad.State == CommunicationState.Opened)
            {
                _clienteEntidad.Close();
            }
        } 
    }
}
