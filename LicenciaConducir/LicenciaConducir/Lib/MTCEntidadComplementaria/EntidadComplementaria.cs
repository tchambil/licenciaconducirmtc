﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LicenciaConducir.Lib.MTCEntidadComplementaria
{
    public class EntidadComplementaria
    {
        public string Id { get; set; }
        public string Region { get; set; }
        public string Provincia { get; set; }
        public string Distrito { get; set; }
        public string RazonSocial { get; set; }
        public string Ubicacion { get; set; }
        public string Estado { get; set; }
        public string RdAutorizacion { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public string Tipo { get; set; }

    }
}
