﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LicenciaConducir.Lib.MTCEntidadComplementaria
{
    public class MTCTipoEntidadComplementaria
    {
        public const int CMEDICO = 13;
        public const int ESCUELACONDUCTOR = 14;
 
        public static List<KeyValuePair<int, string>> Tipos()
        {
            return new List<KeyValuePair<int, string>>() {
        new KeyValuePair<int, string>(CMEDICO, "CENTROS MÉDICOS"), 
        new KeyValuePair<int, string>(ESCUELACONDUCTOR, "ESCUELA DE CONDUCTORES") 
        };
        }
    }
}
