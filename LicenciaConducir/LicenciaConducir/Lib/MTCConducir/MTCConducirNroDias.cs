﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LicenciaConducir.Lib.MTCConducir
{
    public class MTCConducirNroDias
    {
        public const int DIAS_45 = 45;
        public const int DIAS_30 = 30;
        public const int DIAS_15 = 15;
        public const int DIAS_05 = 5;
        
        public static List<KeyValuePair<int, string>> Tipos()
        {
            return new List<KeyValuePair<int, string>>() {
        new KeyValuePair<int, string>(DIAS_45, "45 Días"), 
        new KeyValuePair<int, string>(DIAS_30, "30 Días"),
        new KeyValuePair<int, string>(DIAS_15, "15 Días"),
        new KeyValuePair<int, string>(DIAS_05, "05 Días"),
      };
        }
    }
}
