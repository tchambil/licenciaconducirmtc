﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LicenciaConducir.Lib.MTCConducir
{
    public class MTCConducirTipoDocumento
    {
        public const int DNI = 2;
        public const int CARNET_EXTRANJERIA = 4;
        public const int CARNET_SOLICITANTE = 5;
        public const int TARJETA_IDENTIDAD = 13;
        //public const int TARJETA_IDENTIDAD = 9;

        public const int RECORD = 0;


        public static List<KeyValuePair<int, string>> Tipos()
        {
            return new List<KeyValuePair<int, string>>() {
                    new KeyValuePair<int, string>(DNI, "DNI"),
                     //       new KeyValuePair<int, string>(DNI, "DOCUMENTO NACIONAL DE IDENTIDAD"),
                    new KeyValuePair<int, string>(CARNET_EXTRANJERIA, "CARNET DE EXTRANJERÍA"),
                    new KeyValuePair<int, string>(CARNET_SOLICITANTE, "CARNET DEL SOLICITANTE"),
                    new KeyValuePair<int, string>(TARJETA_IDENTIDAD, "TARJETA DE IDENTIDAD"),
                    };
        }
    }
}
 