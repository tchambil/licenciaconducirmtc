﻿using System;
using System.Collections.Generic;
using System.Text;
using MTC.ServiciosPuntos.Dominio.Entidades;
using MTC.ServiciosRecord.Dominio.Entidades;

namespace LicenciaConducir.Lib.MTCConducir {
  public enum MTCConductorEstado {
    ErrorInternet = 2,
    ErrorSistema = 3,
    NoEncontrado = 4,
    SinLicencia = 5,
    EsperandoRespuesta = 9,
    InformacionObtenida = 10
  }
  public class MTCConductor {
    public MTCConductor(MTCConductorEstado resultado) {
      EstadoRespuesta = resultado; 
    }
    public MTCConductor(MTCConductorEstado resultado, LC_TM_Persona _persona, LC_TM_Papeletas[] _papeletas = null, LC_TM_Bonificacion[] _bonificaciones = null, Sanciones.LC_TM_Sanciones[] _sanciones = null, LC_TM_CursoJornada[] _jornadas = null) {
      EstadoRespuesta = resultado;
      Persona = _persona;
      Papeletas = _papeletas;
      Bonificaciones = _bonificaciones;
      Sanciones = _sanciones;
      CursosJornadas = _jornadas;
    }
    public MTCConductor(MTCConductorEstado resultado, TM_Record _record) {
      Record = _record;
      RecordSanciones = _record.Sanciones;
      RecordPapeletas = _record.Papeletas;
    } 

        public MTCConductorEstado EstadoRespuesta { get; private set; }
    public LC_TM_Persona Persona { get; set; }
    public TM_Record Record { get; set; }
    public LC_TM_Papeletas[] Papeletas { get; set; }
    public LC_TM_Bonificacion[] Bonificaciones { get; set; }
    public Sanciones.LC_TM_Sanciones[] Sanciones { get; set; }
    public LC_TM_CursoJornada[] CursosJornadas { get; set; }

    public bool PapeletasConsultado { get; set; }
    public bool BonificacionesConsultado { get; set; }
    public bool BonificacionesDetalleConsultado { get; set; }
    public bool SancionesConsultado { get; set; }
    public bool CursosJornadaConsultado { get; set; }
    public TD_Papeletas[] RecordPapeletas { get; private set; }
    public TD_Sanciones[] RecordSanciones { get; private set; }
  }
}
