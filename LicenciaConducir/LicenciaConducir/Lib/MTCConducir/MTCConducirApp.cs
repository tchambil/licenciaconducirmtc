﻿using LicenciaConducir.Lib.Include;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Text;
using LicenciaConducir.Sanciones;
using MTC.ServiciosPuntos.Dominio.Entidades;
using MTC.ServiciosRecord.Dominio.Entidades;

namespace LicenciaConducir.Lib.MTCConducir
{
    public class MTCConducirApp
    {
        private static LC_TM_PersonaClient _clientePersona;
        private static TA_RecordWebClient _clienteRecord;
        private static LC_TM_PapeletasClient _clientePapeletas;
        private static LC_TM_BonificacionClient _clienteBonificaciones;
        private static LC_TM_SancionesClient _clienteSanciones;
        private static LC_TM_CursosJornadasClient _clienteJornadas;

        private static MTC.ServiciosPuntos.Dominio.Entidades.LC_TM_Persona PersonaRequest;
        private static TM_Persona PersonaRecordRequest;

        public enum EnumTipoBusquedaConsulta
        {
            PorNroDocumento = 0,
            PorNroLicencia = 1,
            PorNombresApellidos = 2
        }
        public enum EnumTipoBusquedaRecord
        {
            PorNroDocumento = 1,
            PorNroRecord = 2    
        }
        public enum EnumTipoSalida
        {
            Puntos = 0,
            Record = 1,
            Email = 2
        }
        public static EnumTipoSalida ModuloConsulta { get; set; }
        public static EnumTipoBusquedaConsulta TipoBusquedaConsultaPuntos { get; set; }
        public static EnumTipoBusquedaRecord TipoBusquedaRecordConsulta { get; set; }
        public static int Tipo { get; set; }
        public static string NroDocumento { get; set; }
        public static string NroLicencia { get; set; }
        public static string NroRecord { get; set; }
        public static string Nombres { get; set; }
        public static string ApellidoPaterno { get; set; }
        public static string ApellidoMaterno { get; set; }
        public static string Email { get; set; }
        public static string NroRecordSend { get; set; }
        public static int TipoConsultaUsuario { get; set; }
        public static MTCConductor CurrentResultado;
        #region "Para vistas únicas"
        public static LC_TM_Papeletas CurrentPapeleta { get; internal set; }
        public static TD_Papeletas CurrentPapeletaEnRecord { get; internal set; }
        public static LC_TM_Bonificacion CurrentBonificacion { get; internal set; }
        public static TD_Sanciones CurrentSancion { get; set; }
        public static TM_Correo CurrentCorreo { get; set; }
        public static LC_TM_CursoJornada CurrentCurso { get; set; }
        #endregion

        public static event PuntosConsultadoEncontradoDelegado ConductorConsultado;
        public delegate void PuntosConsultadoEncontradoDelegado(MTCConductor respuesta);


        public static event CursoConsultadoEncontradoDelegado CursoConsultado;
        public delegate void CursoConsultadoEncontradoDelegado(LC_TM_CursoJornada respuesta);


        private static MTCConductorEstado Consultar(EnumTipoSalida tipoSalida)
        {
            ModuloConsulta = tipoSalida;
            if (ModuloConsulta == EnumTipoSalida.Puntos)
            {
                PersonaRequest = new MTC.ServiciosPuntos.Dominio.Entidades.LC_TM_Persona();
                switch (TipoBusquedaConsultaPuntos)
                {
                    case EnumTipoBusquedaConsulta.PorNroDocumento:
                        PersonaRequest.Tipo_Busqueda = (int)EnumTipoBusquedaConsulta.PorNroDocumento;
                        PersonaRequest.Tipo_Documento = Tipo.ToString();
                        PersonaRequest.Numero_Documento = NroDocumento;
                        break;
                    case EnumTipoBusquedaConsulta.PorNroLicencia:
                        PersonaRequest.Tipo_Busqueda = (int)EnumTipoBusquedaConsulta.PorNroLicencia;
                        PersonaRequest.Numero_Licencia = NroLicencia;
                        break;
                    case EnumTipoBusquedaConsulta.PorNombresApellidos:
                        PersonaRequest.Tipo_Busqueda = (int)EnumTipoBusquedaConsulta.PorNombresApellidos;
                        PersonaRequest.Nombres = Nombres;
                        PersonaRequest.Apellido_Paterno = ApellidoPaterno;
                        PersonaRequest.Apellido_Materno = ApellidoMaterno;
                        break;
                }
                _clientePersona = new LC_TM_PersonaClient(Helper.ServicioSoapBinding(), new EndpointAddress(Config.Services.puntospersona));
                try
                {
                    _clientePersona.SelBuscarDatosPersonaLisCompleted += ConsultaCompletadaPersona;
                    _clientePersona.SelBuscarDatosPersonaLisAsync(PersonaRequest);
                    return MTCConductorEstado.EsperandoRespuesta;
                }
                catch (Exception e)
                {
                    if (_clientePersona.State == CommunicationState.Opened)
                    {
                        _clientePersona.Close();
                    }
                    return MTCConductorEstado.ErrorSistema;
                }
            }
            else if (ModuloConsulta == EnumTipoSalida.Email)
            {
                try
                {
                    CurrentCorreo = new TM_Correo();
                    CurrentCorreo.num_recordweb =Convert.ToInt64(NroRecordSend);
                    CurrentCorreo.strCorreoDestino = Email;
                    CurrentCorreo.strAsuntoMensaje= "RECORD DE CONDUCTOR - MTC";
                    CurrentCorreo.strAsuntoCorreo = "Estimado(a) Sr(a). Ciudadano(a), mediante la presente reciba su record de conductor solicitado a través de la aplicación móvil \"Licencia de conducir\".";
                    _clienteRecord = new TA_RecordWebClient(Helper.ServicioSoapBinding(), new EndpointAddress(Config.Services.record));
                    _clienteRecord.EnvioCorreoCompleted += SendCompletadaRecord;
                    _clienteRecord.EnvioCorreoAsync(CurrentCorreo);
                    return MTCConductorEstado.EsperandoRespuesta;
                }
                catch (Exception e)
                {
                    if (_clientePersona.State == CommunicationState.Opened)
                    {
                        _clientePersona.Close();
                    }
                    return MTCConductorEstado.ErrorSistema;
                }

            }
            else
            {
                PersonaRecordRequest = new TM_Persona();
                switch (TipoBusquedaRecordConsulta)
                {
                    case EnumTipoBusquedaRecord.PorNroDocumento:
                        PersonaRecordRequest.Tipo_Documento = Tipo.ToString();
                        PersonaRecordRequest.Numero_Documento = NroDocumento;
                        break;
                    case EnumTipoBusquedaRecord.PorNroRecord:
                        PersonaRecordRequest.Numero_Record = NroRecord;
                        break;
                }
                _clienteRecord = new TA_RecordWebClient(Helper.ServicioSoapBinding(), new EndpointAddress(Config.Services.record));
                try
                {
                    switch (TipoBusquedaRecordConsulta)
                    {
                        case EnumTipoBusquedaRecord.PorNroDocumento:
                            _clienteRecord.SelRecordLisCompleted += ConsultaCompletadaRecord;
                            _clienteRecord.SelRecordLisAsync(PersonaRecordRequest);
                            break;
                        case EnumTipoBusquedaRecord.PorNroRecord:
                            _clienteRecord.SelRecordNroLisCompleted += ConsultaNroRecordecord;
                            _clienteRecord.SelRecordNroLisAsync(PersonaRecordRequest);
                            break;
                    }
                   
                    return MTCConductorEstado.EsperandoRespuesta;
                }
                catch (Exception e)
                {
                    if (_clienteRecord.State == CommunicationState.Opened)
                    {
                        _clienteRecord.Close();
                    }
                    return MTCConductorEstado.ErrorSistema;
                }
            }

        }

        private static void ConsultaNroRecordecord(object sender, SelRecordNroLisCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                ConductorConsultado(new MTCConductor(MTCConductorEstado.ErrorSistema));
                return;
            }
            if (e.Cancelled)
            {
                ConductorConsultado(new MTCConductor(MTCConductorEstado.ErrorSistema));
                return;
            }
            if (e.Result == null)
            {
                ConductorConsultado(new MTCConductor(MTCConductorEstado.NoEncontrado));
                return;
            }
            if (e.Result.Estado.Trim().ToUpper() == "SIN LICENCIA")
            {
                ConductorConsultado(new MTCConductor(MTCConductorEstado.SinLicencia));
                return;
            }
            CurrentResultado = new MTCConductor(MTCConductorEstado.InformacionObtenida, e.Result);
            ConductorConsultado(CurrentResultado);
            if (_clienteRecord.State == CommunicationState.Opened)
            {
                _clienteRecord.Close();
            }
        }

        private static void SendCompletadaRecord(object sender, EnvioCorreoCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                ConductorConsultado(new MTCConductor(MTCConductorEstado.ErrorSistema));
                return;
            }
            if (e.Cancelled)
            {
                ConductorConsultado(new MTCConductor(MTCConductorEstado.ErrorSistema));
                return;
            }
            if (e.Result == false)
            {
                ConductorConsultado(new MTCConductor(MTCConductorEstado.NoEncontrado));
                return;
            }           
            CurrentResultado = new MTCConductor(MTCConductorEstado.InformacionObtenida);
            ConductorConsultado(CurrentResultado);
            if (_clienteRecord.State == CommunicationState.Opened)
            {
                _clienteRecord.Close();
            }
        }

        private static void ConsultaCompletadaRecord(object sender, SelRecordLisCompletedEventArgs e)
        {

            if (e.Error != null)
            {
                ConductorConsultado(new MTCConductor(MTCConductorEstado.ErrorSistema));
                return;
            }
            if (e.Cancelled)
            {
                ConductorConsultado(new MTCConductor(MTCConductorEstado.ErrorSistema));
                return;
            }
            if (e.Result == null)
            {
                ConductorConsultado(new MTCConductor(MTCConductorEstado.NoEncontrado));
                return;
            }
            if (e.Result.Estado.Trim().ToUpper() == "SIN LICENCIA")
            {
                ConductorConsultado(new MTCConductor(MTCConductorEstado.SinLicencia));
                return;
            }
            CurrentResultado = new MTCConductor(MTCConductorEstado.InformacionObtenida, e.Result);
            ConductorConsultado(CurrentResultado);
            if (_clienteRecord.State == CommunicationState.Opened)
            {
                _clienteRecord.Close();
            }
        }

        public static MTCConductorEstado ConsultarPuntos()
        {
            return Consultar(EnumTipoSalida.Puntos);
        }
        public static MTCConductorEstado ConsultarRecord()
        {
            return Consultar(EnumTipoSalida.Record);
        }
        public static MTCConductorEstado SendRecord()
        {
            return Consultar(EnumTipoSalida.Email);
        }


        #region "Consultas Servicios adicionales"
        private static void ConsultarPapeletas(MTC.ServiciosPuntos.Dominio.Entidades.LC_TM_Persona persona)
        {
            try
            {
                _clientePapeletas = new LC_TM_PapeletasClient(Helper.ServicioSoapBinding(), new EndpointAddress(Config.Services.puntospapeletas));
                _clientePapeletas.SelBuscarLisCompleted += ConsultarPapeletasCompletado;
                _clientePapeletas.SelBuscarLisAsync(persona);
            }
            catch (Exception error)
            {
                if (_clientePapeletas.State == CommunicationState.Opened)
                {
                    _clientePapeletas.Close();
                }
            }
        }
        private static void ConsultarPapeletasCompletado(object sender, SelBuscarLisCompletedEventArgs e)
        {
            CurrentResultado.PapeletasConsultado = true;
            if (e.Error != null || e.Cancelled || e.Result == null)
            {
                RevisarEstadoConsulta();
                return;
            }
            CurrentResultado.Papeletas = e.Result;
            RevisarEstadoConsulta();
        }
        private static void ConsultarBonificaciones(MTC.ServiciosPuntos.Dominio.Entidades.LC_TM_Persona persona)
        {
            try
            {

                _clienteBonificaciones = new LC_TM_BonificacionClient(Helper.ServicioSoapBinding(), new EndpointAddress(Config.Services.puntosbonificaciones));
                _clienteBonificaciones.SelBuscarLisCompleted += ConsultarBonificacionCompletado;
                _clienteBonificaciones.SelBuscarLisAsync(persona);
            }
            catch (Exception error)
            {
                if (_clienteBonificaciones.State == CommunicationState.Opened)
                {
                    _clienteBonificaciones.Close();
                }
            }
        }
        private static void ConsultarBonificacionCompletado(object sender, SelBuscarLisBonosCompletedEventArgs e)
        {
            CurrentResultado.BonificacionesConsultado = true;
            if (e.Error != null || e.Cancelled || e.Result == null)
            {
                RevisarEstadoConsulta();
                return;
            }
            CurrentResultado.Bonificaciones = e.Result;
            RevisarEstadoConsulta();
        }
        private static void ConsultarBonificacionesDetalle(MTC.ServiciosPuntos.Dominio.Entidades.LC_TM_Bonificacion bonificacion)
        {
            try
            {

                _clienteBonificaciones = new LC_TM_BonificacionClient(Helper.ServicioSoapBinding(), new EndpointAddress(Config.Services.puntosbonificaciones));
                _clienteBonificaciones.SelBuscarLisCompleted += ConsultarBonificacionCompletadoDetalle;
                _clienteBonificaciones.SelBuscarDetalleLisAsync(bonificacion);
            }
            catch (Exception error)
            {
                if (_clienteBonificaciones.State == CommunicationState.Opened)
                {
                    _clienteBonificaciones.Close();
                }
            }
        }
        private static void ConsultarBonificacionCompletadoDetalle(object sender, SelBuscarLisBonosCompletedEventArgs e)
        {
            CurrentResultado.BonificacionesDetalleConsultado = true;
            if (e.Error != null || e.Cancelled || e.Result == null)
            {
                RevisarEstadoConsulta();
                return;
            }
            CurrentResultado.Bonificaciones = e.Result;
            RevisarEstadoConsulta();
        }

        private static void ConsultarSancion(MTC.ServiciosPuntos.Dominio.Entidades.LC_TM_Persona persona)
        {
            try
            {
                _clienteSanciones = new LC_TM_SancionesClient(Helper.ServicioSoapBinding(), new EndpointAddress(Config.Services.puntossanciones));
                _clienteSanciones.SelBuscarLisCompleted += ConsultarSancionCompletado;
                var persona2 = new Sanciones.LC_TM_Persona();
                switch (TipoBusquedaConsultaPuntos)
                {
                    case EnumTipoBusquedaConsulta.PorNroDocumento:
                        persona2.Tipo_Busqueda = (int)EnumTipoBusquedaConsulta.PorNroDocumento;
                        persona2.Tipo_Documento = PersonaRequest.Tipo_Documento;
                        persona2.Numero_Documento = PersonaRequest.Numero_Documento;
                        break;
                    case EnumTipoBusquedaConsulta.PorNroLicencia:
                        persona2.Tipo_Busqueda = PersonaRequest.Tipo_Busqueda;
                        persona2.Numero_Licencia = PersonaRequest.Numero_Licencia;
                        break;
                    case EnumTipoBusquedaConsulta.PorNombresApellidos:
                        persona2.Tipo_Busqueda = PersonaRequest.Tipo_Busqueda;
                        persona2.Nombres = PersonaRequest.Nombres;
                        persona2.Apellido_Paterno = PersonaRequest.Apellido_Paterno;
                        persona2.Apellido_Materno = PersonaRequest.Apellido_Materno;
                        break;
                }
                _clienteSanciones.SelBuscarLisAsync(persona2);
            }
            catch (Exception error)
            {
                if (_clienteSanciones.State == CommunicationState.Opened)
                {
                    _clienteSanciones.Close();
                }
            }
        }

        private static void ConsultarSancionCompletado(object sender, Sanciones.SelBuscarLisCompletedEventArgs e)
        {
            CurrentResultado.SancionesConsultado = true;
            if (e.Error != null || e.Cancelled || e.Result == null)
            {
                RevisarEstadoConsulta();
                return;
            }
            CurrentResultado.Sanciones = e.Result;
            RevisarEstadoConsulta();
        }
        private static void ConsultarCursosJornadaCompletada(object sender, SelBuscarLisCursosJornadaCompletedEventArgs e)
        {
            CurrentResultado.CursosJornadaConsultado = true;
            if (e.Error != null || e.Cancelled || e.Result == null)
            {
                RevisarEstadoConsulta();
                return;
            }
            CurrentResultado.CursosJornadas = e.Result;
            RevisarEstadoConsulta();
        }
        private static void ConsultarCursosJornada(MTC.ServiciosPuntos.Dominio.Entidades.LC_TM_Persona persona)
        {
            try
            {
                _clienteJornadas = new LC_TM_CursosJornadasClient(Helper.ServicioSoapBinding(), new EndpointAddress(Config.Services.puntoscursosjornadas));
                _clienteJornadas.SelBuscarLisCompleted += ConsultarCursosJornadaCompletada;
                _clienteJornadas.SelBuscarLisAsync(persona);
            }
            catch (Exception error)
            {
                if (_clienteJornadas.State == CommunicationState.Opened)
                {
                    _clienteJornadas.Close();
                }
            }
        }
        private static void RevisarEstadoConsulta()
        {
            if (CurrentResultado == null)
            {
                return;
            }
            switch (ModuloConsulta)
            {
                case EnumTipoSalida.Puntos:
                    if (CurrentResultado.BonificacionesConsultado && CurrentResultado.PapeletasConsultado && CurrentResultado.CursosJornadaConsultado)
                    {
                        ConductorConsultado(CurrentResultado);
                    }
                    break;
                case EnumTipoSalida.Record:
                    if (CurrentResultado.SancionesConsultado && CurrentResultado.PapeletasConsultado)
                    {
                        ConductorConsultado(CurrentResultado);
                    }
                    break;
            }
        }
        #endregion
        
        private static void ConsultaCompletadaPersona(object sender, SelBuscarDatosPersonaLisCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                ConductorConsultado(new MTCConductor(MTCConductorEstado.ErrorSistema));
                return;
            }
            if (e.Cancelled)
            {
                ConductorConsultado(new MTCConductor(MTCConductorEstado.ErrorSistema));
                return;
            }
            if (e.Result == null)
            {
                ConductorConsultado(new MTCConductor(MTCConductorEstado.NoEncontrado));
                return;
            }
            if (e.Result.Estado.Trim().ToUpper() == "SIN LICENCIA")
            {
                ConductorConsultado(new MTCConductor(MTCConductorEstado.SinLicencia));
                return;
            }
            CurrentResultado = new MTCConductor(MTCConductorEstado.InformacionObtenida, e.Result);
            switch (ModuloConsulta)
            {
                case EnumTipoSalida.Puntos:
                    ConsultarPapeletas(PersonaRequest);
                    ConsultarBonificaciones(PersonaRequest);
                    ConsultarCursosJornada(PersonaRequest);
                    break;
                case EnumTipoSalida.Record:
                    ConsultarPapeletas(PersonaRequest);
                    ConsultarSancion(PersonaRequest);
                    break;
            }
            if (_clientePersona.State == CommunicationState.Opened)
            {
                _clientePersona.Close();
            }
        }
    }
}
