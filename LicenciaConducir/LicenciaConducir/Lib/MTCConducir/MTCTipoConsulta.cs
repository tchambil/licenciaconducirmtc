﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LicenciaConducir.Lib.MTCConducir
{
    public class MTCTipoConsulta
    {
         public const int DOCUMENTO = 1;
        public const int RECORD = 2;
        public const int LICENCIACONDUCIR = 3;
        public const int APELLIDOS = 4;
        //public const int TARJETA_IDENTIDAD = 9;

        

        public static List<KeyValuePair<int, string>> Tipos()
        {
            return new List<KeyValuePair<int, string>>() {
                    new KeyValuePair<int, string>(DOCUMENTO, "DOCUMENTO"),
                     //       new KeyValuePair<int, string>(DNI, "DOCUMENTO NACIONAL DE IDENTIDAD"),
                    new KeyValuePair<int, string>(RECORD, "RECORD"),
                    new KeyValuePair<int, string>(LICENCIACONDUCIR, "LICENCIACONDUCIR"),
                    new KeyValuePair<int, string>(APELLIDOS, "APELLIDOS"),
                    };
        }
    }
}
