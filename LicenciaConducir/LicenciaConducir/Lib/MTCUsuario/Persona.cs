﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LicenciaConducir.Lib.MTCUsuario
{
    public class Persona
    {
        public string Codigo_Usuario { get; set; }
        public int Tipo_Documento { get; set; }
        public string Numero_Documento { get; set; }
        public string Apellidos { get; set; }
        public string Nombres { get; set; }
        public string Fecha_Nacimiento { get; set; }
        public string Fecha_Emision { get; set; }
        public string Email { get; set; }
        public string Token { get; set; }
        public string IsNotification { get; set; }
        public string IsActivo { get; set; }
        public string IsDelete { get; set; }
        public string Nro_Telefono { get; set; }
        public string IsTelfConfirma { get; set; }
        public string IsEmailConfirma { get; set; }
        public string NotificacionDias { get; set; }

    }
}
