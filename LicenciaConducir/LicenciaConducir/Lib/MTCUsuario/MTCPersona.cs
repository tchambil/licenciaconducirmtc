﻿using System;
using System.Collections.Generic;
using System.Text;
using MTC.ServiciosUsuario.Dominio.Entidades;
namespace LicenciaConducir.Lib.MTCUsuario
{
    public enum MTCPersonaEstado
    {
        ErrorInternet = 2,
        ErrorSistema = 3,
        PhoneExiste = 4,
        Registrado = 5,
        EsperandoRespuesta = 9,
        NoExistePhone= 10,
        ErrorEnBD = 5,
        DatosInvalidos = 1,
        NoEncontrado =0,
        ExistePersona =100
    }
    public class MTCPersona
    {
        public MTCPersona(MTCPersonaEstado resultado)
        {
            EstadoRespuesta = resultado;
        }
        public MTCPersona(MTCPersonaEstado resultado, TM_USUARIO _usuario)
        {
            EstadoRespuesta = resultado;
            Usuario = _usuario;   
        } 
        public MTCPersona(MTCPersonaEstado resultado, Retorno _retorno)
        {
            EstadoRespuesta = resultado;
            retorno = _retorno;
        }  
        public MTCPersonaEstado EstadoRespuesta { get; private set; }
        public TM_USUARIO Usuario { get; set; }
        public Retorno retorno { get; set; }
    public bool UsuarioConsultado { get; set; } 
    }
}
