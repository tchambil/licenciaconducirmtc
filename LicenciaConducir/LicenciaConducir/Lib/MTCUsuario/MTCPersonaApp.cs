﻿using System;
using System.Collections.Generic;
using System.Text;
using MTC.ServiciosUsuario.Dominio.Entidades;
using LicenciaConducir.Lib.Include;
using System.ServiceModel;

namespace LicenciaConducir.Lib.MTCUsuario
{
    public class MTCPersonaApp
    {
        private static TA_UsuarioWebUsuarioClient _clientUser;

        private static TM_USUARIO UsuarioRequest;
        

        private static TM_USUARIOINPUT UserInput;
        //private static TM_USUARIO UsuarioRecordRequest;
        public enum EnumTipoBusquedaConsulta
        {
            PorNroDocumento = 0
        }
        public enum EnumTipoBusquedaLicencia
        {
            PorNroDocumento = 1
        }
        public enum EnumTipoSalida
        {
            Licencia = 0
        }
        public static EnumTipoSalida ModuloConsulta { get; set; }
        public static EnumTipoBusquedaConsulta TipoBusquedaConsultaPersona { get; set; }
        public static EnumTipoBusquedaLicencia TipoBusquedaLicenciaConsulta { get; set; }
        public static int Tipo { get; set; }
        public static string NroDocumento { get; set; }
        public static string Nombres { get; set; }
        public static string Apellidos { get; set; }
        public static string Email { get; set; }
        public static string Token { get; set; }
        public static string NroTelefono { get; set; }
        public static string CodigoVerificado { get; set; }
        public static string Numero_Documento { get; set; }
        public static string IsNotification { get; set; }
        public static int NotificacionDias { get; set; }
        public static string Fecha_Emision { get; set; } 
        public static string Vencimiento { get; set; }
        public static bool SinLicencia { get; set; }
        public static bool Recuerdame { get; set; }
        public static string Contrasenia { get; set; } 
        public static bool IsElectronica { get; set; }
        public static string FileElectronica { get; set; }
        public static MTCPersona CurrentResultado;
        public static TM_USUARIO CurrentUsuario { get; internal set; }
        public static event UsuarioInsertEncontradoDelegado InsertConsultado;
        public delegate void UsuarioInsertEncontradoDelegado(MTCPersona respuesta);
        public static event UsuarioConsultadoEncontradoDelegado UsuarioConsultado;
        public delegate void UsuarioConsultadoEncontradoDelegado(MTCPersona respuesta);
        public static event UsuarioAutenticadoDelegado UsuarioAutenticado;
        public delegate void UsuarioAutenticadoDelegado(MTCPersona respuesta);

        public static event UsuarioValidaPhonelegado UsuarioPhone;
        public delegate void UsuarioValidaPhonelegado(MTCPersona respuesta);

        public static event UsuarioValidaTokenDelegado UsuarioToken;
        public delegate void UsuarioValidaTokenDelegado(MTCPersona respuesta);

        public static Persona _actual { get; set; }
        public static Persona CurrentPersona { get; internal set; }
        public static Persona PersonaActual
        {
            get
            {
                if (_actual == null)
                {
                    _actual = new Persona();
                }
                return _actual;
            }
        }

        public static MTCPersonaEstado ValidarPhone()
        {
            if (!Helper.TieneInternet())
            {
                return MTCPersonaEstado.ErrorInternet;
            } 
            UsuarioRequest = new TM_USUARIO();
            UsuarioRequest.Tipo_Documento = Tipo.ToString();
            UsuarioRequest.Numero_Documento = Numero_Documento;
            UsuarioRequest.Nombres = Nombres;
            UsuarioRequest.Apellidos = Apellidos;
            UsuarioRequest.Email = Email;
            UsuarioRequest.Fecha_Emision = Fecha_Emision;
            UsuarioRequest.Fecha_Nacimiento = "";
            UsuarioRequest.Token = Token;
            UsuarioRequest.Nro_Telefono = NroTelefono;
            UsuarioRequest.NotificacionDias = NotificacionDias.ToString();
            UsuarioRequest.IsNotificacion = IsNotification;

            _clientUser = new TA_UsuarioWebUsuarioClient(Helper.ServicioSoapBinding(), new EndpointAddress(Config.Services.usuario));
            try
            {
                _clientUser.SelTelefonoUsuarioCompleted += _SelTelefonoUsuarioCompleted; ;
                _clientUser.SelTelefonoUsuarioAsync(UsuarioRequest);
                return MTCPersonaEstado.EsperandoRespuesta;
            }
            catch (Exception e)
            {
                if (_clientUser.State == CommunicationState.Opened)
                {
                    _clientUser.Close();
                }
                return MTCPersonaEstado.ErrorSistema;
            }

        }
        private static void _SelTelefonoUsuarioCompleted(object sender, SelTelefonoUsuarioCompletedEventArgs e)
        {
            if (e.Error != null || e.Result.Ok == null)
            {
                UsuarioPhone(new MTCPersona(MTCPersonaEstado.ErrorSistema));
                return;
            }
            if (e.Cancelled)
            {
                UsuarioPhone(new MTCPersona(MTCPersonaEstado.ErrorSistema));
                return;
            }
            if ((e.Result.Ok == true && e.Result.RetVal == "RegistraUsuario"))
            {
                UsuarioPhone(new MTCPersona(MTCPersonaEstado.Registrado));
                return;
            }
            if ((e.Result.Ok == false && e.Result.RetVal == "RegistraUsuario"))
            {
                UsuarioPhone(new MTCPersona(MTCPersonaEstado.ExistePersona));
                return;
            }
            if ((e.Result.Ok==true && e.Result.RetVal== "ValidaPhone")) 
            {
                UsuarioPhone(new MTCPersona(MTCPersonaEstado.PhoneExiste));
                return;
            }
            if ((e.Result.Ok == false && e.Result.RetVal == "ValidaPhone"))
            {
                UsuarioPhone(new MTCPersona(MTCPersonaEstado.NoExistePhone));
                return;
            } 
            if (_clientUser.State == CommunicationState.Opened)
            {
                _clientUser.Close();
            }
        }
        public static MTCPersonaEstado ValidarToken()
        {
            if (!Helper.TieneInternet())
            {
                return MTCPersonaEstado.ErrorInternet;
            }
            UserInput = new TM_USUARIOINPUT();
            UserInput.Nro_Telefono = NroTelefono;
            UserInput.Tipo_Documento = Tipo.ToString();
            UserInput.Numero_Documento = Numero_Documento;
            UserInput.Token = CodigoVerificado;

            _clientUser = new TA_UsuarioWebUsuarioClient(Helper.ServicioSoapBinding(), new EndpointAddress(Config.Services.usuario));
            try
            {
                _clientUser.UpdateCodeVerificacionCompleted +=_UpdateCodeVerificacionCompleted; ;
                _clientUser.UpdateCodeVerificacionAsync(UserInput);
                return MTCPersonaEstado.EsperandoRespuesta;
            }
            catch (Exception e)
            {
                if (_clientUser.State == CommunicationState.Opened)
                {
                    _clientUser.Close();
                }
                return MTCPersonaEstado.ErrorSistema;
            }

        }
        private static void _UpdateCodeVerificacionCompleted(object sender, UpdateCodeVerificacionCompletedEventArgs e)
        {
            if (e.Error != null || e.Result.Ok == null)
            {
                UsuarioToken(new MTCPersona(MTCPersonaEstado.ErrorSistema));
                return;
            }
            if (e.Cancelled)
            {
                UsuarioToken(new MTCPersona(MTCPersonaEstado.ErrorSistema));
                return;
            }
            if (e.Result.Ok == false)
            {
                UsuarioToken(new MTCPersona(MTCPersonaEstado.NoEncontrado));
                return;
            }
            CurrentResultado = new MTCPersona(MTCPersonaEstado.Registrado);
            UsuarioToken(CurrentResultado);
            if (_clientUser.State == CommunicationState.Opened)
            {
                _clientUser.Close();
            }
        }

        public static MTCPersonaEstado EnviarFormulario()
        {
            if (!Helper.TieneInternet())
            {
                return MTCPersonaEstado.ErrorInternet;
            }

            UsuarioRequest = new TM_USUARIO();
            UsuarioRequest.Tipo_Documento = Tipo.ToString();
            UsuarioRequest.Numero_Documento = Numero_Documento;
            UsuarioRequest.Nombres = Nombres;
            UsuarioRequest.Apellidos = Apellidos;
            UsuarioRequest.Email = Email;
            UsuarioRequest.Fecha_Emision = Fecha_Emision;
            UsuarioRequest.Fecha_Nacimiento = "";
            UsuarioRequest.Token = Token;
            UsuarioRequest.Nro_Telefono = "";
            UsuarioRequest.NotificacionDias = NotificacionDias.ToString();
            UsuarioRequest.IsNotificacion = IsNotification;

            _clientUser = new TA_UsuarioWebUsuarioClient(Helper.ServicioSoapBinding(), new EndpointAddress(Config.Services.usuario));
            try
            {
                _clientUser.InsertarUsuarioCompleted += InsertCompletadaUsuario;
                _clientUser.InsertarUsuarioAsync(UsuarioRequest);
                return MTCPersonaEstado.EsperandoRespuesta;
            }
            catch (Exception e)
            {
                if (_clientUser.State == CommunicationState.Opened)
                {
                    _clientUser.Close();
                }
                return MTCPersonaEstado.ErrorSistema;
            }

        }

        private static void InsertCompletadaUsuario(object sender, InsertarUsuarioCompletedEventArgs e)
        {
            if (e.Error != null || e.Result.Ok==null)
            {
                InsertConsultado(new MTCPersona(MTCPersonaEstado.ErrorSistema));
                return;
            }
            if (e.Cancelled)
            {
                InsertConsultado(new MTCPersona(MTCPersonaEstado.ErrorSistema));
                return;
            }
            if (e.Result.Ok == false)
            {
                InsertConsultado(new MTCPersona(MTCPersonaEstado.ErrorEnBD));
                return;
            }            
            CurrentResultado = new MTCPersona(MTCPersonaEstado.Registrado, e.Result);
            InsertConsultado(CurrentResultado);
            if (_clientUser.State == CommunicationState.Opened)
            {
                _clientUser.Close();
            }
        }

        private static MTCPersonaEstado Consultar(EnumTipoSalida tipoSalida)
        {
            ModuloConsulta = tipoSalida;
            UsuarioRequest = new TM_USUARIO();
            UsuarioRequest.Tipo_Documento = Tipo.ToString();
            UsuarioRequest.Numero_Documento = NroDocumento;
                _clientUser = new TA_UsuarioWebUsuarioClient(Helper.ServicioSoapBinding(), new EndpointAddress(Config.Services.usuario));
            try
            {
                _clientUser.SelDatosUsuarioCompleted +=ConsultaCompletadaUsuario;
                _clientUser.SelDatosUsuarioAsync(UsuarioRequest);
                return MTCPersonaEstado.EsperandoRespuesta;
            }
            catch (Exception e)
            {
                if (_clientUser.State == CommunicationState.Opened)
                {
                    _clientUser.Close();
                }
                return MTCPersonaEstado.ErrorSistema;
            }
        }
        public static MTCPersonaEstado AutenticarUsuario()
        {
            return AutenticaUsuarioElectronica();
        }
        private static MTCPersonaEstado AutenticaUsuarioElectronica()
        {
            UserInput = new TM_USUARIOINPUT();
            UserInput.Tipo_Documento = Tipo.ToString();
            UserInput.Numero_Documento = NroDocumento;
            UserInput.Contrasenia = Contrasenia;

            _clientUser = new TA_UsuarioWebUsuarioClient(Helper.ServicioSoapBinding(), new EndpointAddress(Config.Services.usuario));
            try
            {
                _clientUser.AutenticarLicenciaElectronicaCompleted += _clientUser_AutenticarLicenciaElectronicaCompleted; 
                _clientUser.AutenticarLicenciaElectronicaAsync(UserInput);
                return MTCPersonaEstado.EsperandoRespuesta;
            }
            catch (Exception e)
            {
                if (_clientUser.State == CommunicationState.Opened)
                {
                    _clientUser.Close();
                }
                return MTCPersonaEstado.ErrorSistema;
            }
        }

        private static void _clientUser_AutenticarLicenciaElectronicaCompleted(object sender, AutenticarLicenciaElectronicaCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                UsuarioAutenticado(new MTCPersona(MTCPersonaEstado.ErrorSistema));
                return;
            }
            if (e.Cancelled)
            {
                UsuarioAutenticado(new MTCPersona(MTCPersonaEstado.ErrorSistema));
                return;
            }
            if (e.Result == null)
            {
                UsuarioAutenticado(new MTCPersona(MTCPersonaEstado.NoEncontrado));
                return;
            }
            if (e.Result.Ok==false)
            {
                UsuarioAutenticado(new MTCPersona(MTCPersonaEstado.NoEncontrado));
            }


            CurrentResultado = new MTCPersona(MTCPersonaEstado.ExistePersona, e.Result);
            UsuarioAutenticado(CurrentResultado);

            if (_clientUser.State == CommunicationState.Opened)
            {
                _clientUser.Close();
            }
        }

        public static MTCPersonaEstado ConsultarUsuario()
        {
            return Consultar(EnumTipoSalida.Licencia);
        }
        private static void ConsultaCompletadaUsuario(object sender, SelDatosUsuarioCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                UsuarioConsultado(new MTCPersona(MTCPersonaEstado.ErrorSistema));
                return;
            }
            if (e.Cancelled)
            {
                UsuarioConsultado(new MTCPersona(MTCPersonaEstado.ErrorSistema));
                return;
            }
            if (e.Result == null)
            {
                UsuarioConsultado(new MTCPersona(MTCPersonaEstado.NoEncontrado));
                return;
            }
            if (e.Result.Codigo_Usuario != null)
            {
                UsuarioConsultado(new MTCPersona(MTCPersonaEstado.ExistePersona));
            }
         

            //CurrentResultado = new MTCPersona(MTCPersonaEstado.Registrado, e.Result);
            //UsuarioConsultado(CurrentResultado);

            if (_clientUser.State == CommunicationState.Opened)
            {
                _clientUser.Close();
            }

        }


    }
}
