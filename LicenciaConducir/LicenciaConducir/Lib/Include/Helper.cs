﻿ 
using System;
using System.Collections.Generic;
using System.Net;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using LicenciaConducir.Vistas.Lib;

namespace LicenciaConducir.Lib.Include {
  public class Helper {
    public static bool TieneInternet() {
      string CheckUrl = "http://google.com";
      try {
        HttpWebRequest iNetRequest = (HttpWebRequest)WebRequest.Create(CheckUrl);
        iNetRequest.Timeout = 5000;
        WebResponse iNetResponse = iNetRequest.GetResponse();
        iNetResponse.Close();
        return true;
      } catch (WebException ex) {
        return true;
        //TODO: Quitar validación de internet
      }
    }
        public static bool VerificaDominio(string dominio)
        { 
            try
            {
                HttpWebRequest iNetRequest = (HttpWebRequest)WebRequest.Create(dominio.Trim());
                iNetRequest.Timeout = 5000;
                WebResponse iNetResponse = iNetRequest.GetResponse();
                iNetResponse.Close();

                return true;
            }
            catch (WebException ex)
            {
                return false;
                //TODO: Quitar validación de internet
            }
        }
        public static bool ValidaDominio(string dominio)
        {
            var url = dominio.Split('@');
            try
            {
                if (VerificaDominio("http://" + url[1]) | VerificaDominio("https://" + url[1]))
                {
                    return true;
                }
                return false;
            }
            catch (WebException ex)
            {
                return false;
                //TODO: Quitar validación de internet
            }
        }

        public enum TipoSubida {
      FotoDeGaleria = 0,
      FotoDeCamara = 1,
      VideoDeGaleria = 2,
      VideoDeCamara = 3
    }

    public static DateTime? ConvertirAFechaReal(string strfecha) {
      try {
        var soloFecha = strfecha.Trim().Split(' ')[0];
        var fechaPartes = soloFecha.Split('/');
        return new DateTime(int.Parse(fechaPartes[2]), int.Parse(fechaPartes[1]), int.Parse(fechaPartes[0]));
      }
      catch(Exception e) {
        return null;
      }
    }

        public static string ConvertirAFechaStringLocal(string strfecha)
        {
            var fecha = ConvertirAFechaReal(strfecha);
            if (fecha == null)
            {
                return strfecha;
            }
            return fecha.Value.ToString("MM/dd/yyyy");
        }

        public static string ConvertirAFechaString(string strfecha) {
      var fecha = ConvertirAFechaReal(strfecha);
      if (fecha == null) {
        return strfecha;
      }
      return fecha.Value.ToString("dd/MM/yyyy");
    }

    public static BasicHttpBinding ServicioSoapBinding() { 
      var binding = new BasicHttpBinding() {
        Name = "basicHttpBinding",
        MaxReceivedMessageSize = 67108864,
        
      };

      binding.ReaderQuotas = new System.Xml.XmlDictionaryReaderQuotas() {
        MaxArrayLength = 2147483646,
        MaxStringContentLength = 5242880,
      };

      var timeout = new TimeSpan(0, 1, 0);
      binding.SendTimeout = timeout;
      binding.OpenTimeout = timeout;
      binding.ReceiveTimeout = timeout;
      return binding;
    }

    
  }
}