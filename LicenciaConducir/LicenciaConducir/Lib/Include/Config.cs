﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using Xamarin.Forms;

namespace LicenciaConducir.Lib.Include {
  public class Config {
      public static class Services
        {
            //<!--SLCP-->
            public static string ecomplementarias="http://wsecdgtt.mtc.gob.pe/EntidadComplementaria.svc";

            public static string  puntospersona ="http://wsslcpm.mtc.gob.pe/PuntosPersona.svc";

            public static string puntospapeletas = "http://wsslcpm.mtc.gob.pe/PuntosPapeletas.svc";
            public static string puntosbonificaciones = "http://wsslcpm.mtc.gob.pe/PuntosBonificacion.svc";
            public static string puntoscursosjornadas="http://wsslcpm.mtc.gob.pe/PuntosCursosJornadas.svc";
            public static string puntossanciones="http://wsslcpm.mtc.gob.pe/PuntosSanciones.svc";
            //<!--RECORD CONDUCTOR-->
            public static string record="http://wsrcm.mtc.gob.pe/RecordWeb.svc";
            //<!--REGISTRO DE USUARIOS APP MOVIL-->
            public static string usuario = "http://wsdgttm.mtc.gob.pe/Usuario.svc";
             
        }
        private static double ObtenerDouble(string llave, string valor) {
      double outvar;
      if (double.TryParse(valor, out outvar)) {
        return outvar;
      }
      throw new Exception("La variable '" + llave + "' debe ser un entero (valor: " + valor + ")");
    }
    private static decimal ObtenerDecimal(string llave, string valor) {
      decimal outvar;
      if (decimal.TryParse(valor, out outvar)) {
        return outvar;
      }
      throw new Exception("La variable '" + llave + "' debe ser un entero (valor: " + valor + ")");
    }
    private static int ObtenerInt(string llave, string valor) {
      int outvar;
      if (int.TryParse(valor, out outvar)) {
        return outvar;
      }
      throw new Exception("La variable '" + llave + "' debe ser un entero (valor: " + valor + ")");
    }
    private static T GetObject<T>() where T : new() {
      return new T();
    }
    private static T ObtenerObjeto<T>(string llave, string valor) {
      try {
        var obj = JsonConvert.DeserializeAnonymousType<T>(valor, (T)Activator.CreateInstance(typeof(T)));
        return obj;
      } catch (Exception e) {
        throw new Exception("La variable '" + llave + "' debe ser deserializable a un tipo adecuado.");
      }
    }
        
  }
}
