﻿using System;
using Xamarin.Forms; 
using LicenciaConducir.Services;  
using LicenciaConducir.Vistas.Paginas.Usuario;

namespace LicenciaConducir
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();          
            MainPage = new Account();
        }
        protected override void OnStart()
        {

        }

        protected override void OnSleep()
        {

        }

        protected override void OnResume()
        {

        }
        static TodoItemDatabase database; 
        public static TodoItemDatabase Database
        {
            get
            {
                if (database == null)
                {
                    database = new TodoItemDatabase(DependencyService.Get<IDataStoreSql>().GetDatabasePath());
                }
                return database;
            }
        }

        public int ResumeAtTodoId { get; set; }
    }
}
