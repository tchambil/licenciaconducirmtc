﻿using LicenciaConducir.Vistas.Paginas;
using LicenciaConducir.Vistas.Paginas.EComplementarias;
using LicenciaConducir.Vistas.Paginas.Pregunta;
using LicenciaConducir.Vistas.Paginas.Puntos;
using LicenciaConducir.Vistas.Paginas.Record;
using LicenciaConducir.Vistas.Paginas.RSociales;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LicenciaConducir.Vistas.Inc
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MasterDetailLicenciaMaster : ContentPage
    {
        public ListView ListView;

        public MasterDetailLicenciaMaster()
        {
            InitializeComponent();

            BindingContext = new MasterDetailLicenciaMasterViewModel();
            ListView = MenuItemsListView;
        }

        class MasterDetailLicenciaMasterViewModel : INotifyPropertyChanged
        {
            public ObservableCollection<MasterDetailLicenciaMasterMenuItem> MenuItems { get; set; }

            public MasterDetailLicenciaMasterViewModel()
            {
                MenuItems = new ObservableCollection<MasterDetailLicenciaMasterMenuItem>(new[]
                {
                  new MasterDetailLicenciaMasterMenuItem
                    {
                        Titulo = "Inicio",
                        Icono = "IconoInicio.png",
                        PaginaACargar = typeof (Principal)
                    },

                    new MasterDetailLicenciaMasterMenuItem
                    {
                        Titulo = "Licencia de conducir por puntos",
                        Icono = "IconoPuntos.png",
                        PaginaACargar = typeof (SearchPuntos)
                    },

                    new MasterDetailLicenciaMasterMenuItem
                    {
                        Titulo = "Récord de conductor",
                        Icono = "IconoRecord.png",
                        PaginaACargar = typeof (SearchRecord)
                    },
                    //new MasterDetailLicenciaMasterMenuItem
                    //{
                    //    Titulo = "Centros Médicos, Escuelas Conductores, CITV",
                    //    Icono = "IconoRecord.png",
                    //    PaginaACargar = typeof (MapPage)
                    //},
                     new MasterDetailLicenciaMasterMenuItem
                    {
                        Titulo = "Licencia Electrónica",
                        Icono = "IconoRecord.png",
                        PaginaACargar = typeof (Electronica)
                    },
                    new MasterDetailLicenciaMasterMenuItem
                    {
                        Titulo = "Preguntas frecuentes",
                        Icono = "IconoRecord.png",
                        PaginaACargar = typeof (PreguntasFrecuentes)
                    },

                    new MasterDetailLicenciaMasterMenuItem
                    {
                        Titulo = "Redes sociales",
                        Icono = "IconoRecord.png",
                        PaginaACargar = typeof (RedesSociales)
                    }
                });
            }

            #region INotifyPropertyChanged Implementation
            public event PropertyChangedEventHandler PropertyChanged;
            void OnPropertyChanged([CallerMemberName] string propertyName = "")
            {
                if (PropertyChanged == null)
                    return;

                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
            #endregion
            public void SalirDeLaAplicacion(object sender, EventArgs e)
            {
#if __ANDROID__
      var activity = (Android.App.Activity)Forms.Context;
      activity.FinishAffinity(); 
#elif __IOS__
            throw new Exception();
#endif
            }
        }

        private void SalirDeLaAplicacion(object sender, EventArgs e)
        {
#if __ANDROID__
      var activity = (Android.App.Activity)Forms.Context;
      activity.FinishAffinity(); 
#elif __IOS__
            throw new Exception();
#endif
        }
    }
}