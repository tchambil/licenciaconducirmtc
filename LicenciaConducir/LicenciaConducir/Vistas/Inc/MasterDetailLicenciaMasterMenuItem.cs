﻿using LicenciaConducir.Vistas.Paginas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LicenciaConducir.Vistas.Inc
{

    public class MasterDetailLicenciaMasterMenuItem
    {
        public MasterDetailLicenciaMasterMenuItem()
        {
            PaginaACargar = typeof(Bievenida);
        }
        public int Id { get; set; }
        public string Title { get; set; }

        public Type PaginaACargar { get; set; }

        public string Titulo { get; set; }
        public string Icono { get; set; }
        public bool EsInicio { get; set; }
    }
}