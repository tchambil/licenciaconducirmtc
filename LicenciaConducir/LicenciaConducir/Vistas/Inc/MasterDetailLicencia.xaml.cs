﻿using LicenciaConducir.Lib.MTCConducir;
using LicenciaConducir.Lib.MTCUsuario;
using LicenciaConducir.Services;
using Plugin.XamarinFormsSaveOpenPDFPackage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LicenciaConducir.Vistas.Inc
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MasterDetailLicencia : MasterDetailPage
    {
        public NavigationService NavigationService { get; set; }
        private static MasterDetailLicencia _detalleActual;
        public static MasterDetailLicencia DetalleActual
        {
            get { return _detalleActual; }
        }
        public MasterDetailLicencia()
        {
            NavigationService = new NavigationService(Navigation);
            InitializeComponent();
            _detalleActual = this;
            MasterPage.ListView.ItemSelected += ListView_ItemSelected;
        }

        private async void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as MasterDetailLicenciaMasterMenuItem;
            if (item == null)
                return;
            if(item.Titulo== "Licencia Electrónica")
            {
                if (MTCPersonaApp.IsElectronica)
                {
                    var db = TodoItemDatabase.getDatabase();
                    var Persona = await db.GetCiudadano(new ViewModels.Ciudadano()
                    {
                        NumeroDocumento = MTCPersonaApp.NroDocumento,
                        TipoDocumento = MTCConducirApp.Tipo.ToString(),
                    });
                    using (var memoryStream = new MemoryStream(Persona.File))
                    {
                        await CrossXamarinFormsSaveOpenPDFPackage.Current.SaveAndView(Guid.NewGuid() + ".pdf", "application/pdf", memoryStream, PDFOpenContext.ChooseApp);
                    }
                }
                return;
            }
            var page = (Page)Activator.CreateInstance(item.PaginaACargar);
            page.Title = item.Titulo;

            Detail = new NavigationPage(page);
            IsPresented = false;

            MasterPage.ListView.SelectedItem = null;
        }
        public void CargarPaginaResetNav(Type pagina)
        {
            var displayPage = (Page)Activator.CreateInstance(pagina);
            Detail = new NavigationPage(displayPage);
            NavPage = (NavigationPage)Detail;
        }

        public async void CargarPagina(Type pagina, bool incluirHistoria = true)
        {

            var displayPage = (Page)Activator.CreateInstance(pagina);
            Detail = new NavigationPage(displayPage);
            NavigationPage.SetBackButtonTitle(this, "");
            IsPresented = false;


        }

        public async void CargarModal(Type pagina)
        {
            var displayPage = (Page)Activator.CreateInstance(pagina);
            await NavPage.Navigation.PushModalAsync(displayPage, true);
        }
    }
}