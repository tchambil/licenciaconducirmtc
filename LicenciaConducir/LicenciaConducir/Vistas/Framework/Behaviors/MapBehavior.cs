﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using System.Threading.Tasks;
using LicenciaConducir.Lib.MTCEntidadComplementaria;
using LicenciaConducir.Vistas.Lib;
using LicenciaConducir.Vistas.Paginas.EComplementarias;

namespace LicenciaConducir.Framework.Behaviors
{
    public class MapBehavior : BindableBehavior<Map>
    {
        public static readonly BindableProperty ItemsSourceProperty =
            BindableProperty.Create(nameof(ItemsSource), typeof(IEnumerable<EntidadComplementaria>), typeof(MapBehavior), null, BindingMode.Default, propertyChanged: ItemsSourceChanged);

        public IEnumerable<EntidadComplementaria> ItemsSource
        {
            get => (IEnumerable<EntidadComplementaria>)GetValue(ItemsSourceProperty);
            set => SetValue(ItemsSourceProperty, value);
        }

        private static void ItemsSourceChanged(BindableObject bindable, object oldValue, object newValue)
        {
            if (!(bindable is MapBehavior behavior)) return;
            behavior.AddPins();
        }

        private async void AddPins()
        {
            var map = (CustomMap)AssociatedObject;

            var pins = await Task.Run(() => {
                return ItemsSource.Select(x =>
                {
                    var pin = new CustomPin
                    {
                        Type = PinType.Place,
                        Position = new Position(Convert.ToDouble( x.Latitud),Convert.ToDouble(x.Longitud)),
                        Label = x.RazonSocial,
                        Object = x
                    };

                    return pin;
                }).ToArray();
            });

            map.Pins.Clear();
            map.CustomPins = pins.ToList();
            foreach (var pin in pins)
                map.Pins.Add(pin);
            
        }
    }
}