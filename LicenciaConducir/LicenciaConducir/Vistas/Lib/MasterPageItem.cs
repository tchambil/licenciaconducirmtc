﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LicenciaConducir.Vistas.Lib {
  public class MasterPageItem {
    public string Titulo { get; set; }
    public string Icono { get; set; }
    public System.Type PaginaACargar { get; set; }
    public bool EsInicio { get; set; } 
  }
}
