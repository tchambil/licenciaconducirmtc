﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LicenciaConducir.Vistas.Lib
{
    public enum ReturnType
    {
        Go,
        Next,
        Done,
        Send,
        Search
    }
}
