﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using System.Text.RegularExpressions;

namespace LicenciaConducir.Vistas.Lib
{
    public class ValidadorEntry : Behavior<Entry>
    {


        //Result message
        public static readonly BindableProperty MessageProperty = BindableProperty.Create("Message", typeof(string), typeof(ValidadorEntry), string.Empty);
        public string Message
        {
            get { return (string)GetValue(MessageProperty); }
            private set { SetValue(MessageProperty, value); }
        }

        private static readonly BindablePropertyKey IsValidPropertyKey = BindableProperty.CreateReadOnly("EsValido", typeof(bool), typeof(ValidadorEntry), false);
        public static readonly BindableProperty IsValidProperty = IsValidPropertyKey.BindableProperty;
        public bool EsValido
        {
            get
            {
                return (bool)GetValue(IsValidProperty);
            }
            private set
            {
                SetValue(IsValidPropertyKey, value);
            }
        }

        private static readonly BindablePropertyKey MostrarMensajeKey = BindableProperty.CreateReadOnly("MostrarMensaje", typeof(bool), typeof(ValidadorEntry), false);
        public static readonly BindableProperty MostrarMensajeProperty = MostrarMensajeKey.BindableProperty;
        public bool MostrarMensaje
        {
            get
            {
                return !EsValido && string.IsNullOrEmpty(Message);
            }
        }


        public static readonly BindableProperty EsTextoProperty = BindableProperty.Create("EsTexto", typeof(bool), typeof(ValidadorEntry), false);
        public bool EsTexto
        {
            get { return (bool)GetValue(EsTextoProperty); }
            set { SetValue(EsTextoProperty, value); }
        }
        public static readonly BindableProperty EsCorreoProperty = BindableProperty.Create("EsCorreo", typeof(bool), typeof(ValidadorEntry), false);
        public bool EsCorreo
        {
            get { return (bool)GetValue(EsCorreoProperty); }
            set { SetValue(EsCorreoProperty, value); }
        }
        public static readonly BindableProperty EsNombreProperty = BindableProperty.Create("EsNombre", typeof(bool), typeof(ValidadorEntry), false);
        public bool EsNombre
        {
            get { return (bool)GetValue(EsNombreProperty); }
            set { SetValue(EsNombreProperty, value); }
        }
        public static readonly BindableProperty EsApellidoProperty = BindableProperty.Create("EsApellido", typeof(bool), typeof(ValidadorEntry), false);
        public bool EsApellido
        {
            get { return (bool)GetValue(EsApellidoProperty); }
            set { SetValue(EsApellidoProperty, value); }
        }
        public static readonly BindableProperty EsApellidoPaternoProperty = BindableProperty.Create("EsApellidoPaterno", typeof(bool), typeof(ValidadorEntry), false);
        public bool EsApellidoPaterno
        {
            get { return (bool)GetValue(EsApellidoPaternoProperty); }
            set { SetValue(EsApellidoPaternoProperty, value); }
        }
        public static readonly BindableProperty EsApellidoMaternoProperty = BindableProperty.Create("EsApellidoMaterno", typeof(bool), typeof(ValidadorEntry), false);
        public bool EsApellidoMaterno
        {
            get { return (bool)GetValue(EsApellidoMaternoProperty); }
            set { SetValue(EsApellidoMaternoProperty, value); }
        }

        public static readonly BindableProperty EsTextoSCProperty = BindableProperty.Create("EsTextoSC", typeof(bool), typeof(ValidadorEntry), false);
        public bool EsTextoSC
        {
            get { return (bool)GetValue(EsTextoSCProperty); }
            set { SetValue(EsTextoSCProperty, value); }
        }
        public static readonly BindableProperty EsPasswordProperty = BindableProperty.Create("EsPassword", typeof(bool), typeof(ValidadorEntry), false);
        public bool EsPassword
        {
            get { return (bool)GetValue(EsPasswordProperty); }
            set { SetValue(EsPasswordProperty, value); }
        }

        public static readonly BindableProperty EsNumeroProperty = BindableProperty.Create("EsNumero", typeof(bool), typeof(ValidadorEntry), false);
        public bool EsNumero
        {
            get { return (bool)GetValue(EsNumeroProperty); }
            set { SetValue(EsNumeroProperty, value); }
        }

        public static readonly BindableProperty EsLicenciaProperty = BindableProperty.Create("EsLicencia", typeof(bool), typeof(ValidadorEntry), false);
        public bool EsLicencia
        {
            get { return (bool)GetValue(EsLicenciaProperty); }
            set { SetValue(EsLicenciaProperty, value); }
        }

        protected override void OnAttachedTo(Entry entry)
        {
            entry.TextChanged += OnEntryTextChanged;
            EsValido = false;
            Message = "";
            base.OnAttachedTo(entry);
        }

        protected override void OnDetachingFrom(Entry entry)
        {
            entry.TextChanged -= OnEntryTextChanged;
            base.OnDetachingFrom(entry);
        }
        public bool Validar(string texto)
        {
            if (EsTexto)
            {
                EsValido = !string.IsNullOrWhiteSpace(texto);
                if (!EsValido)
                {
                    Message = "Debe ingresar texto.";
                }
            }
            if (EsLicencia)
            { 
                EsValido = !string.IsNullOrWhiteSpace(texto);
                if (!EsValido)
                {
                    Message = "Debe ingresar número de licencia.";
                }
                else
                {
                    var NombreRegex = @"^[\p{L}\p{N} \.'\-]+$"; 
                    EsValido = (Regex.IsMatch(texto, NombreRegex, RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250)));
                    if (!EsValido)
                    {
                        Message = "Debe ingresar número de licencia válida.";
                    }
                }
            }
            if (EsApellido)
            {
                int n = 0;
                EsValido = !string.IsNullOrWhiteSpace(texto) && !int.TryParse(texto, out n);
                if (!EsValido)
                {
                    Message = "Debe ingresar apellido válido.";
                }
                else
                {
                    var NombreRegex = @"^[\p{L} \.'\-]+$";
                    EsValido = (Regex.IsMatch(texto, NombreRegex, RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250)));
                    if (!EsValido)
                    {
                        Message = "Debe ingresar apellido válido.";
                    }
                }
            }
            if (EsApellidoPaterno)
            {
                int n = 0;
                EsValido = !string.IsNullOrWhiteSpace(texto) && !int.TryParse(texto, out n);
                if (!EsValido)
                {
                    Message = "Debe ingresar apellido paterno válido.";
                }
                else
                {
                    var NombreRegex = @"^[\p{L} \.'\-]+$";
                    EsValido = (Regex.IsMatch(texto, NombreRegex, RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250)));
                    if (!EsValido)
                    {
                        Message = "Debe ingresar apellido paterno válido.";
                    }
                }
            }
            if (EsApellidoMaterno)
            {
                int n = 0;
                EsValido = !string.IsNullOrWhiteSpace(texto) && !int.TryParse(texto, out n);
                if (!EsValido)
                {
                    Message = "Debe ingresar apellido materno válido.";
                }
                else
                {
                    var NombreRegex = @"^[\p{L} \.'\-]+$";
                    EsValido = (Regex.IsMatch(texto, NombreRegex, RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250)));
                    if (!EsValido)
                    {
                        Message = "Debe ingresar apellido materno válido.";
                    }
                }
            }
            if (EsNombre)
            {
                int n = 0;
                EsValido = !string.IsNullOrWhiteSpace(texto) && !int.TryParse(texto, out n);
                if (!EsValido)
                {
                    Message = "Debe ingresar nombre válido.";
                }
                else
                {
                    var NombreRegex = @"^[\p{L} \s\.'\-]+$";
                    EsValido = (Regex.IsMatch(texto, NombreRegex, RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250)));
                    if (!EsValido)
                    {
                        Message = "Debe ingresar nombre válido.";
                    }
                }
            }
            if (EsTextoSC)
            {
                int n = 0;
                EsValido = !string.IsNullOrWhiteSpace(texto) && !int.TryParse(texto, out n);
                if (!EsValido)
                {
                    Message = "Debe ingresar texto sin carecteres especiales.";
                }
                else
                {
                    var NombreRegex = @"^[\p{L} \.'\-]+$";
                    EsValido = (Regex.IsMatch(texto, NombreRegex, RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250)));
                    if (!EsValido)
                    {
                        Message = "Debe ingresar un texto válido.";
                    }
                }
            }
            if (EsCorreo)
            {
                if (texto == null)
                {
                    EsValido = false;
                }
                else
                {
                    var EmailRegex = @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" + @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$";
                    EsValido = (Regex.IsMatch(texto, EmailRegex, RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250)));
                }
                if (!EsValido)
                {
                    Message = "Debe ingresar un correo válido.";
                }
            }
            if (EsPassword)
            {
                if (texto == null)
                {
                    EsValido = false;
                }
                else
                {
                    var PassRegex = @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*¿?&()/])([A-Za-z\d$@$!%*?&]|[^ ]){6,12}$";
                    EsValido = (Regex.IsMatch(texto, PassRegex, RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250)));
                }
                if (!EsValido)
                {
                    Message = "Ingrese mayúscula, minúscula, número y caracter especial";                
                }
            }

            if (EsNumero)
            {
                if (texto == null)
                {
                    EsValido = false;
                }
                else
                {
                    long n2 = 0;
                    EsValido = long.TryParse(texto, out n2);
                }
                if (!EsValido)
                {
                    Message = "Debe ingresar un número válido.";
                }
            }
            if (EsValido)
            {
                Message = string.Empty;
            }
            return EsValido;
        }

        void OnEntryTextChanged(object sender, TextChangedEventArgs args)
        {
            Validar(args.NewTextValue);
        }
    }
}
