﻿using LicenciaConducir.Lib.MTCConducir;
using LicenciaConducir.Lib.MTCUsuario;
using LicenciaConducir.Services;
using LicenciaConducir.ViewModels;
using LicenciaConducir.Vistas.Inc;
using Plugin.XamarinFormsSaveOpenPDFPackage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LicenciaConducir.Vistas.Paginas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Electronica : ContentPage
    {
        private FeatureService FeatureService;
        public Electronica()
        {
            InitializeComponent();
            FeatureService = DependencyService.Get<FeatureService>();
        
        }
        protected override async void OnAppearing()
        {
            base.OnAppearing();

          
        }

        private async void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            if (MTCPersonaApp.IsElectronica)
            {
                var db = TodoItemDatabase.getDatabase();
                var Persona = await db.GetCiudadano(new ViewModels.Ciudadano()
                {
                    NumeroDocumento = MTCPersonaApp.NroDocumento,
                    TipoDocumento = MTCConducirApp.Tipo.ToString(),
                });
                using (var memoryStream = new MemoryStream(Persona.File))
                {
                    await CrossXamarinFormsSaveOpenPDFPackage.Current.SaveAndView(Guid.NewGuid() + ".pdf", "application/pdf", memoryStream, PDFOpenContext.ChooseApp);
                }
            }
        }
        protected override bool OnBackButtonPressed()
        {
            base.OnBackButtonPressed();
            MasterDetailLicencia.DetalleActual.CargarPagina(typeof(Principal), false);
            return true;
        }
    }
}