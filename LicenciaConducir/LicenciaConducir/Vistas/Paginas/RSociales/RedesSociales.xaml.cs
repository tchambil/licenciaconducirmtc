﻿using LicenciaConducir.Vistas.Inc;
using LicenciaConducir.Vistas.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LicenciaConducir.Vistas.Paginas.RSociales
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RedesSociales : BasePage
    {
        public RedesSociales()
        {
            InitializeComponent();
        }
        protected override bool OnBackButtonPressed()
        {
            base.OnBackButtonPressed();
            MasterDetailLicencia.DetalleActual.CargarPagina(typeof(Principal), false);
            return true;
        }
        public void Onyoutube(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("https://www.youtube.com/user/MTCPERUOFICIAL/"));

        }
        public void Ontwitter(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("https://twitter.com/MTC_GobPeru/"));
        }
        public void Onflickr(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("https://www.flickr.com/photos/mtc_peru/"));
        }
        public void Onfacebook(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("https://www.facebook.com/MTC.Peru/"));
        }
    }
}