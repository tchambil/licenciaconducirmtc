﻿using LicenciaConducir.Lib.MTCConducir;
using LicenciaConducir.Vistas.Lib;
using LicenciaConducir.Vistas.Paginas.Puntos;
using MTC.ServiciosRecord.Dominio.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LicenciaConducir.Vistas.Paginas.Record
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ConsultaRecordSanciones : BasePage
    {
        public TD_Sanciones Sancion
        {
            get
            {
                return MTCConducirApp.CurrentSancion;
            }
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            var lista = new List<ConductorDetalleItem>();
            lista.Add(new ConductorDetalleItem() { Campo = "Tipo de sanción", Valor = Sancion.Tipo_Sancion });
            lista.Add(new ConductorDetalleItem() { Campo = "Nro. de resolución", Valor = Sancion.Numero_Resolucion });
            lista.Add(new ConductorDetalleItem() { Campo = "Fecha de resolución", Valor = Sancion.Fecha_Resolucion });
            lista.Add(new ConductorDetalleItem() { Campo = "Entidad", Valor = Sancion.Entidad_Nombre });
            lista.Add(new ConductorDetalleItem() { Campo = "Fecha de inicio de sanción", Valor = Sancion.Fecha_Inicio_Sancion });
            lista.Add(new ConductorDetalleItem() { Campo = "Fecha de fin de sanción", Valor = Sancion.Fecha_Fin_Sancion });
            lista.Add(new ConductorDetalleItem() { Campo = "Consecuencia", Valor = Sancion.Consecuencia });
            listaDetalle.ItemsSource = lista;
        }
        public ConsultaRecordSanciones()
        {
            InitializeComponent();
            NavigationPage.SetBackButtonTitle(this, "");
        }
        void PrevenirSeleccion(object sender, ItemTappedEventArgs e)
        {
            if (e == null) return; // has been set to null, do not 'process' tapped event 
            ((ListView)sender).SelectedItem = null; // de-select the row
        }
    }
}