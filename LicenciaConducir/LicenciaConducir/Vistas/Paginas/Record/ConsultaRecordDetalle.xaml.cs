﻿using LicenciaConducir.Lib.Include;
using LicenciaConducir.Lib.MTCConducir;
using LicenciaConducir.Services;
using LicenciaConducir.Vistas.Inc;
using LicenciaConducir.Vistas.Lib;
using LicenciaConducir.Vistas.Paginas.Puntos;
using MTC.ServiciosRecord.Dominio.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LicenciaConducir.Vistas.Paginas.Record
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public class RecordDetalle
    {
        public string Campo { get; set; }
        public double FontSize { get; set; }
        public string Valor { get; set; }
        public RecordDetalle()
        {
            FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label));
        }
    }
    public partial class ConsultaRecordDetalle : BasePage
    {
        public bool Cargado = false;
        public string nroRecord = "";
        public NavigationService NavigationService { get; set; }
        public ConsultaRecordDetalle()
        {
            InitializeComponent();
            NavigationService = new NavigationService(Navigation);
            NavigationPage.SetBackButtonTitle(this, "");
        }
        protected override async void OnAppearing()
        {
            base.OnAppearing();
            if (Cargado) return;
            MTCConductorEstado resultado = MTCConductorEstado.ErrorSistema;
            await Task.Run(() =>
            {
                MTCConducirApp.ConductorConsultado += RespuestaConsultaPuntos;
                resultado = MTCConducirApp.ConsultarRecord();
            });
            if (resultado != MTCConductorEstado.EsperandoRespuesta)
            {
                switch (resultado)
                {
                    case MTCConductorEstado.ErrorInternet:
                        await DisplayAlert("Aviso", "Su dispositivo no cuenta con internet en estos momentos.", "Aceptar");
                        break;
                    case MTCConductorEstado.ErrorSistema:
                        await DisplayAlert("Aviso", "Hubo un problema en la comunicación con el servidor, por favor intente después.", "Aceptar");
                        break;
                }
                Navigation.RemovePage(this);
            }
        }
        public void OnPapeletas(object sender, EventArgs e)
        {
            listaPapeletas.IsVisible = !listaPapeletas.IsVisible;
            if (listaPapeletas.IsVisible)
            {
                img3.Source = "iconCollapseOut.png";

                //datos 
                listaDetalle.IsVisible = false;
                imgdatos.Source = "iconCollapse.png";
                //Sanciones 
                listaSanciones.IsVisible = false;
                img1.Source = "iconCollapse.png";

            }
            else
            {
                img3.Source = "iconCollapse.png";
            }

        }
        public void OnSanciones(object sender, EventArgs e)
        {
            listaSanciones.IsVisible = !listaSanciones.IsVisible;
            if (listaSanciones.IsVisible)
            {


                //datos 
                listaDetalle.IsVisible = false;
                imgdatos.Source = "iconCollapse.png";
                img1.Source = "iconCollapseOut.png";
                //Sanciones 
                listaPapeletas.IsVisible = false;
                img3.Source = "iconCollapse.png";

            }
            else
            {
                img1.Source = "iconCollapse.png";
            }

        }
        public void OnDatos(object sender, EventArgs e)
        {

            listaDetalle.IsVisible = !listaDetalle.IsVisible;
            if (listaDetalle.IsVisible)
            {
                imgdatos.Source = "iconCollapseOut.png";
                //Sanciones 
                listaSanciones.IsVisible = false;
                img1.Source = "iconCollapse.png";
                ////Papeletas
                listaPapeletas.IsVisible = false;
                img3.Source = "iconCollapse.png";

            }
            else
            {
                imgdatos.Source = "iconCollapse.png";
            }
        }
        void PrevenirSeleccion(object sender, ItemTappedEventArgs e)
        {
            if (e == null) return; // has been set to null, do not 'process' tapped event 
            ((ListView)sender).SelectedItem = null; // de-select the row
        }
        private void RespuestaConsultaPuntos(MTCConductor respuesta)
        {
            MTCConducirApp.ConductorConsultado -= RespuestaConsultaPuntos;
            Device.BeginInvokeOnMainThread(async () =>
            {
                string menssage = string.Empty;

                if (MTCConducirApp.TipoConsultaUsuario == MTCTipoConsulta.DOCUMENTO)
                {
                    menssage = "No se encontró información del número de documento ingresado.";
                }
                else if (MTCConducirApp.TipoConsultaUsuario == MTCTipoConsulta.RECORD)
                {
                    menssage = "No se encontró información del número de record ingresado.";
                }
                else
                {
                    menssage = "No se encontró información del número de documento ingresado.";
                }

                await Task.Delay(500);
                if (respuesta.EstadoRespuesta == MTCConductorEstado.NoEncontrado)
                {
                    await DisplayAlert("Aviso", menssage, "Aceptar");
                    Navigation.RemovePage(this);
                    return;
                }
                if (respuesta.EstadoRespuesta == MTCConductorEstado.ErrorSistema)
                {
                    await DisplayAlert("Aviso", "Hubo un problema en la comunicación con el servidor, por favor intente después.", "Aceptar");
                    Navigation.RemovePage(this);
                    return;
                }
                if (respuesta.EstadoRespuesta == MTCConductorEstado.SinLicencia)
                {
                    await DisplayAlert("Información", menssage, "Aceptar");
                    Navigation.RemovePage(this);
                    return;
                }

                var lista = new List<ConductorDetalleItem>();
                nroRecord = respuesta.Record.IdRecord.ToString();
                lista.Add(new ConductorDetalleItem() { Campo = "Récord", Valor = respuesta.Record.IdRecord.ToString() });
                lista.Add(new ConductorDetalleItem() { Campo = "Nombre completo", Valor = respuesta.Record.Apellido_Paterno + " " + respuesta.Record.Apellido_Materno + " " + respuesta.Record.Nombres });
                lista.Add(new ConductorDetalleItem() { Campo = "Nro. de documento", Valor = respuesta.Record.Numero_Documento });
                lista.Add(new ConductorDetalleItem() { Campo = "Nro. de licencia", Valor = respuesta.Record.Licencia });
                lista.Add(new ConductorDetalleItem() { Campo = "Clase y categoría", Valor = respuesta.Record.Categoria });
                lista.Add(new ConductorDetalleItem() { Campo = "Estado", Valor = respuesta.Record.Estado });
                lista.Add(new ConductorDetalleItem() { Campo = "Fecha de expedición", Valor = Helper.ConvertirAFechaString(respuesta.Record.Fecha_Expedicion) });
                lista.Add(new ConductorDetalleItem() { Campo = "Fecha de revalidación", Valor = Helper.ConvertirAFechaString(respuesta.Record.Fecha_Revalidacion) });
                lista.Add(new ConductorDetalleItem() { Campo = "Restricciones", Valor = respuesta.Record.Restriccion_1 + " " + respuesta.Record.Restriccion_2 });
                lblNameCiudadano.Text = "¡Hola " + respuesta.Record.Nombres + "!";
                lblDniCiudadano.Text = respuesta.Record.Numero_Documento;
                listaDetalle.ItemsSource = lista;
                listaDetalle.HeightRequest = lista.Count() * (listaDetalle.RowHeight + 1);


                if (respuesta.RecordPapeletas != null)
                {
                    int count = 0;
                    foreach (var item in respuesta.RecordPapeletas)
                    {
                        count++;
                        item.Papeleta_Fecha = Helper.ConvertirAFechaString(item.Papeleta_Fecha);
                    }
                    if (count > 0)
                    {
                        listaPapeletas.HeightRequest = respuesta.RecordPapeletas.Count() * (listaPapeletas.RowHeight + 1);
                        listaPapeletas.ItemTapped += MostrarPapeleta;
                        listaPapeletas.ItemsSource = respuesta.RecordPapeletas;
                    }
                    else
                    {
                        var AnyRecordPapeletas = new[]
                           { new TD_Papeletas() {
                                 Papeleta_Fecha = "No Registra Papeletas",
                                 Falta="0"
                            }
                            };
                        listaPapeletas.ItemsSource = AnyRecordPapeletas;
                    }

                }
                else
                {
                    var AnyRecordPapeletas = new[]
                          { new TD_Papeletas() {
                                 Papeleta_Fecha = "No Registra Papeletas",
                                 Falta="0"
                            }
                            };
                    listaPapeletas.ItemsSource = AnyRecordPapeletas;
                }

                if (respuesta.RecordSanciones != null)
                {
                    int count = 0;
                    listaSanciones.HeightRequest = respuesta.RecordSanciones.Count() * (listaSanciones.RowHeight + 1);
                    foreach (var itemsancion in respuesta.RecordSanciones)
                    {
                        count++;
                        itemsancion.Fecha_Resolucion = Helper.ConvertirAFechaString(itemsancion.Fecha_Resolucion);
                        itemsancion.Fecha_Inicio_Sancion = Helper.ConvertirAFechaString(itemsancion.Fecha_Inicio_Sancion);
                        itemsancion.Fecha_Fin_Sancion = Helper.ConvertirAFechaString(itemsancion.Fecha_Fin_Sancion);
                        itemsancion.Numero_Resolucion = "R " + itemsancion.Numero_Resolucion;
                    }
                    if (count > 0)
                    {
                        listaSanciones.ItemTapped += MostrarSanciones;
                        listaSanciones.ItemsSource = respuesta.RecordSanciones;
                    }
                    else
                    {
                        var AnyRecordSanciones = new[]
                        {
                            new TD_Sanciones() {
                            Numero_Resolucion = "No Registra Sanciones",
                            Consecuencia = "0" }
                    };
                        listaSanciones.ItemsSource = AnyRecordSanciones;


                    }
                }
                else
                {
                    var AnyRecordSanciones = new[]
                         {
                            new TD_Sanciones() {
                            Numero_Resolucion = "No Registra Sanciones",
                            Consecuencia = "0" }
                    };
                    listaSanciones.ItemsSource = AnyRecordSanciones;

                }

                await IndicadorDeCarga.FadeTo(0, 25);
                IndicadorDeCarga.IsVisible = false;
                administrado_wrapper.IsVisible = true;
                listaDetalle.IsVisible = false;
                framedatos.IsVisible = true;
                listaSanciones.IsVisible = false;
                sanciones.IsVisible = true;
                papeletas.IsVisible = true;
                listaPapeletas.IsVisible = false;
                frameEnviaremail.IsVisible = true;
                administrado_wrapper.Opacity = 0;
                listaDetalle.Opacity = 0;
                frSanciones.IsVisible = true;
                frPapeletas.IsVisible = true;
                frameEnviaremail.IsVisible = true;
                await administrado_wrapper.FadeTo(1, 150);
                await listaDetalle.FadeTo(1, 150);
                Cargado = true;
            });
        }

        private async void MostrarPapeleta(object sender, ItemTappedEventArgs e)
        {
            if (listaPapeletas.SelectedItem != null)
            {
                MTCConducirApp.CurrentPapeletaEnRecord = (TD_Papeletas)e.Item;
                //MasterDetail.DetalleActual.CargarPagina(typeof(ConsultaRecordPapeleta));
                await NavigationService.PushAsync(new ConsultaRecordPapeleta());
                ((ListView)sender).SelectedItem = null; // de-select the row 
            }
        }

        private async void MostrarSanciones(object sender, ItemTappedEventArgs e)
        {
            if (listaSanciones.SelectedItem != null)
            {
                MTCConducirApp.CurrentSancion = (TD_Sanciones)e.Item;
                // MasterDetail.DetalleActual.CargarPagina(typeof(ConsultaRecordSanciones));
                await NavigationService.PushAsync(new ConsultaRecordSanciones());
                ((ListView)sender).SelectedItem = null; // de-select the row 
            }
        }
        private void MostrarAyudaPapeletas(object sender, EventArgs e)
        {
            // MasterDetail.DetalleActual.CargarPagina(typeof(FaltaAyuda));
            //  await NavigationService.PushAsync(new FaltaAyuda());
        }
        public async void OnSendEmail(object sender, EventArgs e)
        {
            if (Email.Text == null)
            {
                await DisplayAlert("Aviso", "Por favor ingrese un correo válido.", "Aceptar");
                return;
            }

            var valido = EmailValidar.Validar(Email.Text.Trim());
            if (!valido)
            {
                DisplayAlert("Aviso", "Por favor ingrese un correo válido.", "Aceptar");
                return;
            }

            valido = VerificaEmail(Email.Text.Trim());
            if (!valido)
            {
                DisplayAlert("Aviso", "Por favor ingrese un correo válido.", "Aceptar");
                return;
            }

            if (nroRecord == "")
            {
                DisplayAlert("Error", "Ha ocurrido un error al obtener Nro de record", "Aceptar");
                return;
            }
            MTCConducirApp.NroRecordSend = nroRecord.ToString();
            MTCConducirApp.Email = Email.Text.Trim();
            MTCConducirApp.TipoBusquedaRecordConsulta = MTCConducirApp.EnumTipoBusquedaRecord.PorNroRecord;
            MasterDetailLicencia.DetalleActual.CargarPagina(typeof(EnviarEmailRecord), false);

        }

        void Entry_EmailTextChanged(object sender, TextChangedEventArgs e)
        {
            var EmailRegex = @"^[\p{L}\p{N}\.@_-\-_]+$";
            Email.Text = e.NewTextValue.ToUpper();
            string _text = Email.Text;
            bool EsValido = (Regex.IsMatch(e.NewTextValue, EmailRegex, RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250)));
            if (!EsValido)
            {
                if (_text.Length > 1)
                {
                    _text = _text.Remove(_text.Length - 1);
                    Email.Text = _text.ToUpper();
                }
                else
                {
                    Email.Text = "";
                }

            }


        }
        private class aEmail
        {
            public string texto { get; set; }
        }
        private bool VerificaEmail(string strEmail)
        {
            var newEmail = strEmail.Split('@');
            List<aEmail> nlista = new List<aEmail>();
            nlista.Add(new aEmail() { texto = newEmail[1] });
            foreach (char i in nlista[0].texto)
            {
                string caracter = "";
                for (int j = 1; j <= 3; j++)
                {
                    caracter += i;
                }
                if (((from o in nlista
                      where o.texto.Contains(caracter)
                      select new { o.texto }).ToList()).Count() > 0)
                {
                    return false;
                }
            }
            return true;
        }
    }
}