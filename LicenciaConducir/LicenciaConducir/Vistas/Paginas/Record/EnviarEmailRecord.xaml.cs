﻿using LicenciaConducir.Lib.MTCConducir;
using LicenciaConducir.Vistas.Inc;
using LicenciaConducir.Vistas.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LicenciaConducir.Vistas.Paginas.Record
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EnviarEmailRecord : BasePage
    {
        public bool Cargado = false;
        public EnviarEmailRecord()
        {
            InitializeComponent();

        }
        protected override async void OnAppearing()
        {
            base.OnAppearing();
            if (Cargado) return;
            MTCConductorEstado resultado = MTCConductorEstado.ErrorSistema;
            await Task.Run(() =>
            {
                MTCConducirApp.ConductorConsultado += RespuestaEmailPuntos;
                resultado = MTCConducirApp.SendRecord();
            });
            if (resultado != MTCConductorEstado.EsperandoRespuesta)
            {
                switch (resultado)
                {
                    case MTCConductorEstado.ErrorInternet:
                        await DisplayAlert("Aviso", "Su dispositivo no cuenta con internet en estos momentos.", "Aceptar");
                        break;
                    case MTCConductorEstado.ErrorSistema:
                        await DisplayAlert("Aviso", "Hubo un problema en la comunicación con el servidor, por favor intente después.", "Aceptar");
                        break;
                }
                Navigation.RemovePage(this);
            }
        }

        private void RespuestaEmailPuntos(MTCConductor respuesta)
        {

            MTCConducirApp.ConductorConsultado -= RespuestaEmailPuntos;
            Device.BeginInvokeOnMainThread(async () =>
            {
                await Task.Delay(500);
                if (respuesta.EstadoRespuesta == MTCConductorEstado.NoEncontrado)
                {
                    await DisplayAlert("Aviso", "Hubo un problema, no se ha enviado el record al correo especificado.", "Aceptar");

                    return;
                }
                if (respuesta.EstadoRespuesta == MTCConductorEstado.ErrorSistema)
                {
                    await DisplayAlert("Aviso", "Hubo un problema en la comunicación con el servidor, por favor intente después.", "Aceptar");

                    return;
                }
                if (respuesta.EstadoRespuesta == MTCConductorEstado.InformacionObtenida)
                {
                    IndicadorDeCarga.IsVisible = false;
                    Grilla.IsVisible = true;
                }

            });
        }

        public void IrAlInicio(object sender, EventArgs e)
        {
            MasterDetailLicencia.DetalleActual.CargarPaginaResetNav(typeof(Principal));
        }
    }
}