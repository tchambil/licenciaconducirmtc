﻿using LicenciaConducir.Lib.MTCConducir;
using LicenciaConducir.Vistas.Lib;
using LicenciaConducir.Vistas.Paginas.Puntos;
using MTC.ServiciosRecord.Dominio.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LicenciaConducir.Vistas.Paginas.Record
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ConsultaRecordPapeleta : BasePage
    {
        public TD_Papeletas Papeleta
        {
            get
            {
                return MTCConducirApp.CurrentPapeletaEnRecord;
            }
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            LabelFechaInfraccion.Text = Papeleta.Papeleta_Fecha;
            LabelEntidad.Text = Papeleta.Entidad_Nombre;
            LabelCodFalta.Text = Papeleta.Falta;
            var lista = new List<ConductorDetalleItem>();
            lista.Add(new ConductorDetalleItem() { Campo = "Nro. de papeleta", Valor = Papeleta.Papeleta_Numero });
            lista.Add(new ConductorDetalleItem() { Campo = "Nro. Entidad", Valor = Papeleta.Entidad_Numero.HasValue ? Papeleta.Entidad_Numero.ToString() : "" });
            lista.Add(new ConductorDetalleItem() { Campo = "Entidad", Valor = Papeleta.Entidad_Nombre });
            lista.Add(new ConductorDetalleItem() { Campo = "Fecha", Valor = Papeleta.Papeleta_Fecha });
            lista.Add(new ConductorDetalleItem() { Campo = "Falta", Valor = Papeleta.Falta });
            listaDetalle.ItemsSource = lista;
        }
        public ConsultaRecordPapeleta()
        {
            InitializeComponent();
            NavigationPage.SetBackButtonTitle(this, "");
        }
        void PrevenirSeleccion(object sender, ItemTappedEventArgs e)
        {
            if (e == null) return; // has been set to null, do not 'process' tapped event 
            ((ListView)sender).SelectedItem = null; // de-select the row
        }
    }
}