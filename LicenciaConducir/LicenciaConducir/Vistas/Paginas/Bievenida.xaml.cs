﻿using LicenciaConducir.Lib.Include;
using LicenciaConducir.Lib.MTCConducir;
using LicenciaConducir.Lib.MTCUsuario;
using LicenciaConducir.Services;
using LicenciaConducir.ViewModels;
using LicenciaConducir.Vistas.Lib;
using LicenciaConducir.Vistas.Paginas.Puntos;
using LicenciaConducir.Vistas.Paginas.Usuario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LicenciaConducir.Vistas.Paginas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Bievenida : BasePage
    {
        static int contador = 0;
        public bool Cargado = false;
        public bool FlagExiste = false;
        public Bievenida()
        {
            InitializeComponent();
            contador = 0;
            NavigationPage.SetBackButtonTitle(this, "");
        }
        protected override async void OnAppearing()
        {
            base.OnAppearing();
            if (Cargado) return;
            var current = Connectivity.NetworkAccess;
            if (current == NetworkAccess.Internet)
            {
                if (Cargado) return;


                if (MTCPersonaApp.IsElectronica)
                {
                    MTCPersonaEstado resultElectronica = MTCPersonaEstado.ErrorSistema;
                    await Task.Run(async () =>
                    {
                        MTCPersonaApp.UsuarioAutenticado += MTCPersonaApp_UsuarioAutenticado;
                        resultElectronica = MTCPersonaApp.AutenticarUsuario();
                    }
                    );
                    if (resultElectronica != MTCPersonaEstado.EsperandoRespuesta)
                    {
                        switch (resultElectronica)
                        {
                            case MTCPersonaEstado.ErrorInternet:
                                await DisplayAlert("Aviso", "Su dispositivo no cuenta con internet en estos momentos.", "Aceptar");
                                break;
                            case MTCPersonaEstado.ErrorSistema:
                                await DisplayAlert("Aviso", "Hubo un problema de comunicación con el servidor, por favor intente después.", "Aceptar");
                                break;
                            case MTCPersonaEstado.NoEncontrado:
                                await DisplayAlert("Aviso", "No se encontró información del número documento ingresado.", "Aceptar");
                                break;
                        }
                    }
                }
                else
                {
                    ObtenerLicenciaConducir();
                }
            }
            else
            {
                var db = TodoItemDatabase.getDatabase();
                Ciudadano Persona = new Ciudadano();
                if (MTCPersonaApp.IsElectronica)
                {
                    Persona = await db.GetCiudadano(new ViewModels.Ciudadano()
                    {
                        NumeroDocumento = MTCPersonaApp.NroDocumento,
                        TipoDocumento = MTCConducirApp.Tipo.ToString(),
                        Password = MTCPersonaApp.Contrasenia
                    });

                    if (Persona == null)
                    {
                        await DisplayAlert("Aviso de modo Offline (Desconectado)", "No se encontró información del número documento ingresado o la contraseña no es correcta.", "Aceptar");
                        App.Current.Properties["NDocumento"] = "";
                        App.Current.Properties["TDocucumento"] = 0;
                        MTCPersonaApp.SinLicencia = true;
                        App.Current.MainPage = new Account();
                        return; 
                    }
                }
                else
                {
                    Persona = await db.GetCiudadano(new ViewModels.Ciudadano()
                    {
                        NumeroDocumento = MTCPersonaApp.NroDocumento,
                        TipoDocumento = MTCConducirApp.Tipo.ToString(),
                    });
                    if (Persona == null)
                    {
                        await DisplayAlert("Aviso de modo Offline (Desconectado)", "No se encontró información del número documento ingresado.", "Aceptar");
                        App.Current.Properties["NDocumento"] = "";
                        App.Current.Properties["TDocucumento"] = 0;
                        MTCPersonaApp.SinLicencia = true;
                        App.Current.MainPage = new Account();
                        return;
                    }
                } 

                var lista = new List<ConductorDetalleItem>();
                lista.Add(new ConductorDetalleItem() { Campo = "Nro. de licencia", Valor = Persona.NumeroLicencia });
                lista.Add(new ConductorDetalleItem() { Campo = "Clase y categoría", Valor = Persona.ClaseCategoria });
                lista.Add(new ConductorDetalleItem() { Campo = "Vigente desde", Valor = Helper.ConvertirAFechaString(Persona.FechaExpedicion) });
                lista.Add(new ConductorDetalleItem() { Campo = "Fecha de revalidación", Valor = Helper.ConvertirAFechaString(Persona.FechaRevalidacion) });
                lista.Add(new ConductorDetalleItem() { Campo = "Estado de licencia", Valor = Persona.Estado });
                App.Current.Properties["NDocumento"] = Persona.NumeroDocumento;
                App.Current.Properties["TDocucumento"] = MTCPersonaApp.Tipo;
                MTCPersonaApp.SinLicencia = false;

                MTCPersonaApp.Numero_Documento = Persona.NumeroDocumento;
                MTCPersonaApp.NroDocumento = Persona.NumeroDocumento;
                MTCPersonaApp.Fecha_Emision = Helper.ConvertirAFechaString(Persona.FechaRevalidacion);
                MTCPersonaApp.Vencimiento = Persona.MensajeVecimiento;
                Nombres.Text = "!Hola " + Persona.NombresCompletos + " !";
                MTCPersonaApp.Nombres = Persona.NombresCompletos;
                Vencimiento.Text = (Persona.MensajeVecimiento).ToLower();
                listaDetalle.ItemsSource = lista;
                IndicadorDeCarga.IsVisible = false;
                administrado_wrapper.IsVisible = true;
                listaDetalle.IsVisible = true;
                framedatos.IsVisible = true;
                titulovencimiento_wrapper.IsVisible = true;
                stackbutton.IsVisible = true;
                administrado_wrapper.Opacity = 0;
                listaDetalle.Opacity = 0;
                await administrado_wrapper.FadeTo(1, 150);
                await listaDetalle.FadeTo(1, 150);
                await IndicadorDeCarga.FadeTo(0, 25);
                Cargado = true;
                FlagExiste = true;
            }
        }
        private async void ObtenerLicenciaConducir()
        {
            MTCPersonaEstado resultadopersona = MTCPersonaEstado.ErrorSistema;
            await Task.Run(async () =>
            {
                MTCPersonaApp.UsuarioConsultado += RespuestaConsultaUsuario;
                resultadopersona = MTCPersonaApp.ConsultarUsuario();
            });
            if (resultadopersona != MTCPersonaEstado.EsperandoRespuesta)
            {
                switch (resultadopersona)
                {
                    case MTCPersonaEstado.ErrorInternet:
                        await DisplayAlert("Aviso", "Su dispositivo no cuenta con internet en estos momentos.", "Aceptar");
                        break;
                    case MTCPersonaEstado.ErrorSistema:
                        await DisplayAlert("Aviso", "Hubo un problema de comunicación con el servidor, por favor intente después.", "Aceptar");
                        break;
                }
            }

            MTCConductorEstado resultado = MTCConductorEstado.ErrorSistema;
            await Task.Run(async () =>
            {
                MTCConducirApp.ConductorConsultado += RespuestaConsultaPuntos;
                resultado = MTCConducirApp.ConsultarPuntos();
            });
            if (resultado != MTCConductorEstado.EsperandoRespuesta)
            {
                switch (resultado)
                {
                    case MTCConductorEstado.ErrorInternet:
                        await DisplayAlert("Aviso", "Su dispositivo no cuenta con internet en estos momentos.", "Aceptar");
                        break;
                    case MTCConductorEstado.ErrorSistema:
                        await DisplayAlert("Aviso", "Hubo un problema de comunicación con el servidor, por favor intente después.", "Aceptar");
                        break;
                    case MTCConductorEstado.NoEncontrado:
                        await DisplayAlert("Aviso", "No se encontró información del número documento ingresado.", "Aceptar");
                        break;
                }
            }
        }
        private void MTCPersonaApp_UsuarioAutenticado(MTCPersona respuesta)
        {
            MTCPersonaApp.UsuarioAutenticado -= MTCPersonaApp_UsuarioAutenticado;
            Device.BeginInvokeOnMainThread(async () =>
            {
                await Task.Delay(500);
                if (respuesta.EstadoRespuesta == MTCPersonaEstado.ExistePersona)
                {

                    MTCPersonaApp.FileElectronica = respuesta.retorno.Mensaje;
                    ObtenerLicenciaConducir();
                }
                else
                {
                    await DisplayAlert("Aviso", "No se encontró información del número documento ingresado.", "Aceptar");
                    App.Current.Properties["NDocumento"] = "";
                    App.Current.Properties["TDocucumento"] = 0;
                    MTCPersonaApp.SinLicencia = true;
                    App.Current.MainPage = new Account();
                    return;
                }
                if (respuesta.EstadoRespuesta == MTCPersonaEstado.ErrorSistema)
                {
                    await DisplayAlert("Aviso", "Hubo un problema en la comunicación con el servidor, por favor intente después.", "Aceptar");
                    return;
                }
            });
        }

        void PrevenirSeleccion(object sender, ItemTappedEventArgs e)
        {
            if (e == null) return; // has been set to null, do not 'process' tapped event 
            ((ListView)sender).SelectedItem = null; // de-select the row
        }
        private void RespuestaConsultaPuntos(MTCConductor respuesta)
        {
            MTCConducirApp.ConductorConsultado -= RespuestaConsultaPuntos;
            Device.BeginInvokeOnMainThread(async () =>
            {
                await Task.Delay(500);
                if (respuesta.EstadoRespuesta == MTCConductorEstado.NoEncontrado)
                {
                    // await DisplayAlert("Aviso", "No hemos encontrado un registro que coincida con sus parámetros.", "Aceptar");
                    await DisplayAlert("Aviso", "No se encontró información del número documento ingresado.", "Aceptar");
                    App.Current.Properties["NDocumento"] = "";
                    App.Current.Properties["TDocucumento"] = 0;
                    MTCPersonaApp.SinLicencia = true;
                    App.Current.MainPage = new Account();
                    return;
                }
                if (respuesta.EstadoRespuesta == MTCConductorEstado.ErrorSistema)
                {
                    await DisplayAlert("Aviso", "Hubo un problema en la comunicación con el servidor, por favor intente después.", "Aceptar");
                    return;
                }
                if (respuesta.EstadoRespuesta == MTCConductorEstado.SinLicencia)
                {
                    await DisplayAlert("Información", "No se encontró información del número documento ingresado.", "Aceptar");
                    App.Current.Properties["NDocumento"] = "";
                    App.Current.Properties["TDocucumento"] = 0;
                    MTCPersonaApp.SinLicencia = true;
                    App.Current.MainPage = new Account();
                    return;
                }
                var lista = new List<ConductorDetalleItem>();
                lista.Add(new ConductorDetalleItem() { Campo = "Nro. de licencia", Valor = respuesta.Persona.Numero_Licencia });
                lista.Add(new ConductorDetalleItem() { Campo = "Clase y categoría", Valor = respuesta.Persona.Clase_Categoria });
                lista.Add(new ConductorDetalleItem() { Campo = "Vigente desde", Valor = Helper.ConvertirAFechaString(respuesta.Persona.Fecha_Expedicion) });
                lista.Add(new ConductorDetalleItem() { Campo = "Fecha de revalidación", Valor = Helper.ConvertirAFechaString(respuesta.Persona.Fecha_Revalidacion) });
                lista.Add(new ConductorDetalleItem() { Campo = "Estado de licencia", Valor = respuesta.Persona.Estado });
                App.Current.Properties["NDocumento"] = respuesta.Persona.Numero_Documento;
                App.Current.Properties["TDocucumento"] = MTCPersonaApp.Tipo;
                MTCPersonaApp.SinLicencia = false;
                MTCPersonaApp.Numero_Documento = respuesta.Persona.Numero_Documento;
                MTCPersonaApp.NroDocumento = respuesta.Persona.Numero_Documento;
                MTCPersonaApp.Apellidos = respuesta.Persona.Apellido_Paterno + " " + respuesta.Persona.Apellido_Materno;
                MTCPersonaApp.Nombres = respuesta.Persona.Nombres;
                MTCPersonaApp.Fecha_Emision = Helper.ConvertirAFechaString(respuesta.Persona.Fecha_Revalidacion);
                MTCPersonaApp.Vencimiento = respuesta.Persona.MensajeVecimiento;
                Nombres.Text = "!Hola " + respuesta.Persona.Nombres_Completos + " !";
                MTCPersonaApp.Nombres = respuesta.Persona.Nombres_Completos;
                Vencimiento.Text = (respuesta.Persona.MensajeVecimiento).ToLower();


                var db = TodoItemDatabase.getDatabase();
                var Persona = await db.GetCiudadano(new ViewModels.Ciudadano()
                {
                    NumeroDocumento = MTCPersonaApp.NroDocumento,
                    TipoDocumento = MTCConducirApp.Tipo.ToString(),
                });
                if (Persona != null)
                {
                    Persona.NumeroDocumento = respuesta.Persona.Numero_Documento;
                    Persona.NumeroLicencia = respuesta.Persona.Numero_Licencia;
                    Persona.NombresCompletos = respuesta.Persona.Nombres_Completos;
                    Persona.ClaseCategoria = respuesta.Persona.Clase_Categoria;
                    Persona.Estado = respuesta.Persona.Estado;
                    Persona.FechaEmision = respuesta.Persona.Fecha_Emision;
                    Persona.FechaExpedicion = respuesta.Persona.Fecha_Expedicion;
                    Persona.FechaRevalidacion = respuesta.Persona.Fecha_Revalidacion;
                    Persona.MensajeVecimiento = respuesta.Persona.MensajeVecimiento;
                    Persona.CreationTime = DateTime.Now;

                    if (Persona.File == null && MTCPersonaApp.IsElectronica)
                    {
                        LicenciaElectronica licenciaElectronica = new LicenciaElectronica();
                        var result = await licenciaElectronica.GetFileOutput(MTCPersonaApp.FileElectronica);
                        Persona.File = result.archivoField;
                        await db.UpdateCiudadano(Persona);
                    }
                }
                else
                {
                    LicenciaElectronicaFile file = new LicenciaElectronicaFile();
                    if (MTCPersonaApp.IsElectronica)
                    {
                        LicenciaElectronica licenciaElectronica = new LicenciaElectronica();
                        file = await licenciaElectronica.GetFileOutput(MTCPersonaApp.FileElectronica);
                    }
                    await db.AddCiudadano(new ViewModels.Ciudadano()
                    {
                        NumeroDocumento = respuesta.Persona.Numero_Documento,
                        NumeroLicencia = respuesta.Persona.Numero_Licencia,
                        NombresCompletos = respuesta.Persona.Nombres_Completos,
                        ClaseCategoria = respuesta.Persona.Clase_Categoria,
                        Estado = respuesta.Persona.Estado,
                        FechaEmision = respuesta.Persona.Fecha_Emision,
                        FechaExpedicion = respuesta.Persona.Fecha_Expedicion,
                        FechaRevalidacion = respuesta.Persona.Fecha_Revalidacion,
                        MensajeVecimiento = respuesta.Persona.MensajeVecimiento,
                        TipoDocumento = MTCConducirApp.Tipo.ToString(),
                        PathFile = MTCPersonaApp.FileElectronica,
                        Password = MTCPersonaApp.Contrasenia,
                        File = file.archivoField,
                        
                    });
                }
                listaDetalle.ItemsSource = lista;
                IndicadorDeCarga.IsVisible = false;
                administrado_wrapper.IsVisible = true;
                listaDetalle.IsVisible = true;
                framedatos.IsVisible = true;
                titulovencimiento_wrapper.IsVisible = true;
                stackbutton.IsVisible = true;
                administrado_wrapper.Opacity = 0;
                listaDetalle.Opacity = 0;
                await administrado_wrapper.FadeTo(1, 150);
                await listaDetalle.FadeTo(1, 150);
                await IndicadorDeCarga.FadeTo(0, 25);
                Cargado = true;
            });
        }

        private void RespuestaConsultaUsuario(MTCPersona respuesta)
        {
            MTCPersonaApp.UsuarioConsultado -= RespuestaConsultaUsuario;
            Device.BeginInvokeOnMainThread(async () =>
            {
                await Task.Delay(500);
                if (respuesta.EstadoRespuesta == MTCPersonaEstado.ExistePersona)
                {
                    FlagExiste = true;
                }
            });
        }
        public async void OnPrincipal(object sender, EventArgs e)
        {
            if (FlagExiste)
                App.Current.MainPage = new Vistas.Inc.MasterDetailLicencia();
            else
                App.Current.MainPage = new Question();
        }

    }
}