﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LicenciaConducir.Vistas.Paginas.EComplementarias
{
   public class DisplayEntity
    {
        public string KeyEntidad { get; set; }
        public string ValueEntidade { get; set; }
    }
}
