﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms.Maps;

namespace LicenciaConducir.Vistas.Paginas.EComplementarias
{ 
    public class CustomMap : Map
    {
        public List<CustomPin> CustomPins { get; set; }
        public List<LocationPoly> PolyLine { get; set; }
    }
}
