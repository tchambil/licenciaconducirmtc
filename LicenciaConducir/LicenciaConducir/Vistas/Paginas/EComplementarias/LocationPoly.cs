﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LicenciaConducir.Vistas.Paginas.EComplementarias
{
    public class LocationPoly
    {
        public double Latitude { set; get; }

        public double Longitude { set; get; }
    }
}
