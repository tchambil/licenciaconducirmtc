﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace LicenciaConducir.Vistas.Paginas.EComplementarias
{
    class RouteResponse
    {
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }
         

            [JsonProperty(PropertyName = "routes")]
            public Routes[] routes { get; set; }

            public class Data
            {
                public int Value { get; set; }
                public string Text { get; set; }
            }

        public class PolyLine
        {
            public string Points { get; set; }
        }

        public class Steps
        {
            public Data Duration { get; set; }
            public Data Distance { get; set; }
            public PolyLine Polyline { get; set; }
        }
            public class legs
        {
              
                public Data Duration { get; set; }
                public Data Distance { get; set; }
                public Steps[] Steps { get; set; }
        }

            public class Routes
        {
                public legs[] Legs { get; set; }
            }
        }
    }
