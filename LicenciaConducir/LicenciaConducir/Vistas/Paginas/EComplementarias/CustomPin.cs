﻿using System;
using System.Collections.Generic;
using System.Text;
using LicenciaConducir.Lib.MTCEntidadComplementaria;
using LicenciaConducir.Vistas.Lib;
using Xamarin.Forms.Maps;

namespace LicenciaConducir.Vistas.Paginas.EComplementarias
{
    public class CustomPin : Pin
    {
        public EntidadComplementaria Object { get; set; }
        public string Url { get; set; }
    }
}
