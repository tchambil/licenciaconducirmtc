﻿using LicenciaConducir.Lib.MTCConducir;
using LicenciaConducir.Lib.MTCUsuario;
using LicenciaConducir.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LicenciaConducir.Vistas.Paginas.Usuario
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Account : ContentPage
    {
        public bool Cargado = false;
        private int _isOwned = 0;
        public Account()
        {
            InitializeComponent();
         
        }
        protected override async void OnAppearing()
        {
            base.OnAppearing();
           
            if (Cargado) return;
            var current = Connectivity.NetworkAccess;

            if (current == NetworkAccess.Internet)
            {
                this.EstadoRed.Text = "Conectado a la RED";
            }
            else
            {
                this.EstadoRed.Text = "Modo OffLine - Desconectado";
            }       
            if (selectorTipoDocumento.Items.Count <= 0)
            {
                foreach (var item in MTCConducirTipoDocumento.Tipos())
                {
                    selectorTipoDocumento.Items.Add(item.Value);
                }
                selectorTipoDocumento.SelectedIndex = -1;
            }           
            selectorTipoDocumento.SelectedItem = MTCConducirTipoDocumento.Tipos().Where(p => p.Key == MTCPersonaApp.Tipo).Select(p => p.Value).FirstOrDefault(); ;
            NroDocumento.Text = MTCPersonaApp.NroDocumento;
    
            IndicadorDeCarga.IsVisible = false;
            stacklogin.IsVisible = true; 
        }
        public void switcher_Toggled(object sender, ToggledEventArgs e)
        {
            MTCPersonaApp.Recuerdame = e.Value;
        }
        public void switcher_ToggledLicenciaElectronica(object sender, ToggledEventArgs e)
        {
            if(e.Value == true)
            {
                this.Contrasenia.IsVisible = true; 
            }
            else
            {
                this.Contrasenia.IsVisible = false; 
            }

            MTCPersonaApp.IsElectronica = e.Value;
        }
        public void CambiarTipoDoc(object sender, EventArgs e)
        {
            try
            {
                TipoDocumento.Text = selectorTipoDocumento.Items[selectorTipoDocumento.SelectedIndex];

            }
            catch (Exception xe)
            {
            }
        }

        public void AbrirPicker(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                TipoDocumento.Unfocus();
                selectorTipoDocumento.Focus();
            });
        }
       public  async void OnRegister(object sender, EventArgs e)
        {
            App.Current.MainPage = new Register();
        }
       public  void Entry_TextChangedDocumento(object sender, TextChangedEventArgs e)
        {
            if (selectorTipoDocumento.SelectedIndex < 0)
            {
                if (NroDocumento.Text != "")
                {
                    string _text = NroDocumento.Text;
                    _text = _text.Remove(_text.Length - 1);
                    NroDocumento.Text = _text;
                }

                return;
            }
            if (NroDocumento.Text.Trim().Length > 12)
            {
                return;
            }

            var tipodoc = MTCConducirTipoDocumento.Tipos().Where(p => p.Value == selectorTipoDocumento.Items.ElementAt(selectorTipoDocumento.SelectedIndex)).Select(p => p.Key).FirstOrDefault();
            if (tipodoc == 2)
            {

                NroDocumento.Text = e.NewTextValue.Replace(".", "");
                string _text = NroDocumento.Text;
                if (_text.Length > 8)
                {
                    _text = _text.Remove(_text.Length - 1);
                    NroDocumento.Text = _text;
                }

            }
            if (tipodoc == 4)
            {

                NroDocumento.Text = e.NewTextValue.Replace(".", "");
                string _text = NroDocumento.Text;
                if (_text.Length > 11)
                {
                    _text = _text.Remove(_text.Length - 1);
                    NroDocumento.Text = _text;
                }

            }
            if (tipodoc == 5)
            {

                NroDocumento.Text = e.NewTextValue.Replace(".", "");
                string _text = NroDocumento.Text;
                if (_text.Length > 5)
                {
                    _text = _text.Remove(_text.Length - 1);
                    NroDocumento.Text = _text;
                }

            }
            if (tipodoc == 13)
            {

                NroDocumento.Text = e.NewTextValue.Replace(".", "");
                string _text = NroDocumento.Text;
                if (_text.Length > 9)
                {
                    _text = _text.Remove(_text.Length - 1);
                    NroDocumento.Text = _text;
                }

            }

        }
       public async void OnLogin(object sender, EventArgs e)
        {
            if (selectorTipoDocumento.SelectedIndex < 0)
            {
                await DisplayAlert("Aviso", "Seleccione el tipo de documento.", "Aceptar");
                return;
            }

            var tipodoc = MTCConducirTipoDocumento.Tipos().Where(p => p.Value == selectorTipoDocumento.Items.ElementAt(selectorTipoDocumento.SelectedIndex)).Select(p => p.Key).FirstOrDefault();

            long n2 = 0;
            if (NroDocumento.Text == null)
            {
                await DisplayAlert("Aviso", "Ingrese número de documento.", "Aceptar");
                return;
            }
            if (NroDocumento.Text.Length <= 0 | NroDocumento.Text == string.Empty)
            {
                await DisplayAlert("Aviso", "Ingrese número de documento.", "Aceptar");
                return;
            }
            bool IsDigito = long.TryParse(NroDocumento.Text.Trim(), out n2);
            if (tipodoc == MTCConducirTipoDocumento.DNI)
            {
                if (IsDigito)
                {
                    if ((NroDocumento.Text.Length < 8 | NroDocumento.Text.Length > 8))
                    {
                        await DisplayAlert("Aviso", "Ingrese número de documento de 8 dígitos.", "Aceptar");
                        return;
                    }
                }
                else
                {
                    await DisplayAlert("Aviso", "Ingrese número de documento de 8 dígitos.", "Aceptar");
                    return;
                }
            }
            if (tipodoc == MTCConducirTipoDocumento.CARNET_EXTRANJERIA)
            {
                if (IsDigito)
                {
                    if ((NroDocumento.Text.Length < 5 | NroDocumento.Text.Length > 11))
                    {
                        await DisplayAlert("Aviso", "Ingrese número de documento válido.", "Aceptar");
                        return;
                    }
                }
                else
                {
                    await DisplayAlert("Aviso", "Ingrese número de documento válido.", "Aceptar");
                    return;
                }
            }
            if (tipodoc == MTCConducirTipoDocumento.CARNET_SOLICITANTE)
            {
                if (IsDigito)
                {
                    if ((NroDocumento.Text.Length < 5 | NroDocumento.Text.Length > 5))
                    {
                        await DisplayAlert("Aviso", "Ingrese número de documento de 5 dígitos.", "Aceptar");
                        return;
                    }
                }
                else
                {
                    await DisplayAlert("Aviso", "Ingrese número de documento de 5 dígitos.", "Aceptar");
                    return;
                }
            }
            if (tipodoc == MTCConducirTipoDocumento.TARJETA_IDENTIDAD)
            {
                if (IsDigito)
                {
                    if ((NroDocumento.Text.Length < 9 | NroDocumento.Text.Length > 9))
                    {
                        await DisplayAlert("Aviso", "Ingrese número de documento de 9 dígitos.", "Aceptar");
                        return;
                    }
                }
                else
                {
                    await DisplayAlert("Aviso", "Ingrese número de documento de 9 dígitos.", "Aceptar");
                    return;
                }
            }
            if (MTCPersonaApp.IsElectronica)
            {
                if(Contrasenia.Text== null || Contrasenia.Text == "")
                {
                    await DisplayAlert("Aviso", "Ingrese la contraseña que fue notificado en su correo electronico.", "Aceptar");
                    return;
                }
            }

       

            MTCPersonaApp.Tipo = tipodoc;
            MTCPersonaApp.TipoBusquedaConsultaPersona = MTCPersonaApp.EnumTipoBusquedaConsulta.PorNroDocumento;
            MTCPersonaApp.NroDocumento = NroDocumento.Text;
            MTCConducirApp.Tipo = MTCConducirTipoDocumento.Tipos().Where(p => p.Value == selectorTipoDocumento.Items.ElementAt(selectorTipoDocumento.SelectedIndex)).Select(p => p.Key).FirstOrDefault();
            MTCConducirApp.TipoBusquedaConsultaPuntos = MTCConducirApp.EnumTipoBusquedaConsulta.PorNroDocumento;
            MTCConducirApp.NroDocumento = NroDocumento.Text;
            MTCPersonaApp.Contrasenia = Contrasenia.Text;
            App.Current.MainPage = new NavigationPage(new Bievenida());        }
        //private async void RespuestaConsultaUsuario(MTCPersona respuesta)
        //{
        //    MTCPersonaApp.UsuarioConsultado -= RespuestaConsultaUsuario;
        //    Device.BeginInvokeOnMainThread(async () =>
        //    {
        //        await Task.Delay(500);
        //        if (respuesta.EstadoRespuesta == MTCPersonaEstado.NoEncontrado)
        //        {
        //            IndicadorDeCarga.IsVisible = false;
        //            stacklogin.IsVisible = true;
        //            // stackfooter.IsVisible = true;

        //        }
        //        if (respuesta.EstadoRespuesta == MTCPersonaEstado.ErrorSistema)
        //        {
        //            await DisplayAlert("Aviso", "Hubo un problema en la comunicación con el servidor, por favor intente después.", "Aceptar");
        //            return;
        //        }

        //        if (respuesta.EstadoRespuesta == MTCPersonaEstado.Registrado)
        //        {
        //            App.Current.Properties["dni"] = MTCPersonaApp.NroDocumento;
        //            App.Current.Properties["typedoc"] = MTCPersonaApp.Tipo;
        //            App.Current.MainPage = new Vistas.Inc.MasterDetailLicencia();
        //            // return;
        //        }          
        //    });
        //}
    }
}