﻿using LicenciaConducir.Lib.MTCUsuario;
using LicenciaConducir.Vistas.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LicenciaConducir.Vistas.Paginas.Usuario
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class VerifyEmail : BasePage
    {
        public VerifyEmail()
        {
            InitializeComponent();
            SNombre.Text = "Sr.(a) " + MTCPersonaApp.Nombres;
        }

        private async void BtnNext_OnClicked(object sender, EventArgs e)
        {

            MTCPersonaApp.Email = Email.Text;

            if (Email.Text == null)
            {
                await DisplayAlert("Aviso", "Ingrese correo electrónico valido", "Aceptar");
                return;
            }
            var EmailRegex = @"^[\p{L}\p{N}\.@_-\-_]+$";

            if (Email.Text.Length <= 0 | Email.Text == string.Empty)
            {
                await DisplayAlert("Aviso", "Ingrese correo electrónico valido.", "Aceptar");
                return;
            }
            bool EsValido = (Regex.IsMatch(Email.Text, EmailRegex, RegexOptions.IgnoreCase,
                TimeSpan.FromMilliseconds(250)));

            if (!EsValido)
            {

                await DisplayAlert("Aviso", "Ingrese correo electrónico valido.", "Aceptar");
                return;

            }
            App.Current.MainPage = new VerifyPhone();
        }

        private void Email_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            {
                if (Email.Text.Trim().Length > 50)
                {
                    return;
                }

                var EmailRegex = @"^[\p{L}\p{N}\.@_-\-_]+$";
                Email.Text = e.NewTextValue.ToUpper();
                string _text = Email.Text;
                bool EsValido = (Regex.IsMatch(e.NewTextValue, EmailRegex, RegexOptions.IgnoreCase,
                    TimeSpan.FromMilliseconds(250)));
                if (!EsValido)
                {
                    if (_text.Length > 1)
                    {
                        _text = _text.Remove(_text.Length - 1);
                        Email.Text = _text.ToUpper();
                    }
                    else
                    {
                        Email.Text = "";
                    }

                }
            }
        }
    }
}