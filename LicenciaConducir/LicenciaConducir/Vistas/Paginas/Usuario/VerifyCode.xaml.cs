﻿using LicenciaConducir.Lib.MTCUsuario;
using LicenciaConducir.Vistas.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LicenciaConducir.Vistas.Paginas.Usuario
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class VerifyCode : BasePage
    {
        public bool Cargado = false;
        public VerifyCode()
        {
            InitializeComponent();
            lblTelefono.Text = MTCPersonaApp.NroTelefono;
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            if (Cargado) return;

            MTCPersonaEstado resultado = MTCPersonaEstado.ErrorSistema;
            await Task.Run(async () =>
            {
                MTCPersonaApp.UsuarioPhone += _UsuarioPhone; ;
                resultado = MTCPersonaApp.ValidarPhone();
            });
            if (resultado != MTCPersonaEstado.EsperandoRespuesta)
            {
                switch (resultado)
                {
                    case MTCPersonaEstado.ErrorInternet:
                        await DisplayAlert("Aviso", "Su dispositivo no cuenta con internet en estos momentos.", "Aceptar");
                        break;
                    case MTCPersonaEstado.ErrorSistema:
                        await DisplayAlert("Aviso", "Hubo un problema en la comunicación con el servidor, por favor intente después.", "Aceptar");
                        break;
                }
                Navigation.RemovePage(this);
            }
        }

        private void _UsuarioPhone(MTCPersona respuesta)
        {
            MTCPersonaApp.UsuarioPhone -= _UsuarioPhone;
            Device.BeginInvokeOnMainThread(async () =>
            {
                await Task.Delay(500);
                if (respuesta.EstadoRespuesta == MTCPersonaEstado.ErrorSistema)
                {
                    await DisplayAlert("Aviso", "Hubo un problema en la comunicación con el servidor, por favor intente después.", "Aceptar");
                    return;
                }


                if (respuesta.EstadoRespuesta == MTCPersonaEstado.ExistePersona)
                {
                    await DisplayAlert("Aviso", "Ud. ya se encuentra registrado.", "Aceptar");
                    App.Current.MainPage = new Vistas.Inc.MasterDetailLicencia();
                    return;
                }

                if (respuesta.EstadoRespuesta == MTCPersonaEstado.PhoneExiste)
                {
                    await DisplayAlert("Aviso", "El número de teléfono ya se encuentra registrado.", "Aceptar");
                    App.Current.MainPage = new VerifyPhone();
                    return;
                }
                if ((respuesta.EstadoRespuesta == MTCPersonaEstado.NoExistePhone) || (respuesta.EstadoRespuesta == MTCPersonaEstado.Registrado))
                {
                    IndicadorDeCarga.IsRunning = false;
                    IndicadorDeCarga.IsVisible = false;
                    frmValidaToken.IsVisible = true;
                    stkBotones.IsVisible = true;
                }

            });
        }

        protected override bool OnBackButtonPressed()
        {
            base.OnBackButtonPressed();
            App.Current.MainPage = new VerifyPhone();
            return true;
        }
        private async void BtnNext_OnClicked(object sender, EventArgs e)
        {
            MTCPersonaApp.CodigoVerificado = NroCode.Text;
            long n2 = 0;
            if (NroCode.Text == null)
            {
                await DisplayAlert("Aviso", "Ingrese el código de 6 digitos.", "Aceptar");
                return;
            }
            if (NroCode.Text.Length <= 0 | NroCode.Text == string.Empty)
            {
                await DisplayAlert("Aviso", "Ingrese el código de 6 digitos.", "Aceptar");
                return;
            }
            bool IsDigito = long.TryParse(NroCode.Text.Trim(), out n2);

            if (IsDigito)
            {
                if ((NroCode.Text.Length < 6))
                {
                    await DisplayAlert("Aviso", "Ingrese el código de 6 digitos.", "Aceptar");
                    return;
                }
            }
            else
            {
                await DisplayAlert("Aviso", "Ingrese el código de 6 digitos.", "Aceptar");
                return;
            }
            App.Current.MainPage = new Completed();
        }

        private void BtnBack_OnClicked(object sender, EventArgs e)
        {
            App.Current.MainPage = new VerifyPhone();
        }
    }
}