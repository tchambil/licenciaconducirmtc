﻿using LicenciaConducir.Lib.MTCConducir;
using LicenciaConducir.Lib.MTCUsuario;
using LicenciaConducir.Vistas.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LicenciaConducir.Vistas.Paginas.Usuario
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UsuarioCierre : BasePage
    {
        public bool Cargado = false;
        public UsuarioCierre()
        {
            InitializeComponent();
            // NavigationPage.SetBackButtonTitle(this, ""); 
            //MostrarNavegacion = false;
        }
        protected override async void OnAppearing()
        {
            base.OnAppearing();
            if (Cargado) return;
            MTCPersonaEstado resultadoEnvio = MTCPersonaEstado.ErrorSistema;
            await Task.Run(async () =>
            {
                MTCPersonaApp.InsertConsultado += RespuestaInsertUsuario;
                resultadoEnvio = MTCPersonaApp.EnviarFormulario();
            });

            if (resultadoEnvio != MTCPersonaEstado.EsperandoRespuesta)
            {
                switch (resultadoEnvio)
                {
                    case MTCPersonaEstado.ErrorInternet:
                        await DisplayAlert("Aviso", "Asegurese de contar con internet en su dispositivo.", "Aceptar");
                        App.Current.MainPage = new Register();
                        break;
                    case MTCPersonaEstado.ErrorSistema:
                        await DisplayAlert("Aviso", "Hubo un problema al procesar su información, por favor cierre y abra la aplicación e intente de nuevo.", "Aceptar");
                        App.Current.MainPage = new Register();
                        break;
                    case MTCPersonaEstado.DatosInvalidos:
                        await DisplayAlert("Aviso", "No se ha podido validar la información enviada. Por favor revise los campos e intente de nuevo", "Aceptar");
                        App.Current.MainPage = new Register();
                        break;
                    case MTCPersonaEstado.ErrorEnBD:
                        await DisplayAlert("Aviso", "Ud. ya se encuentra registrado en la aplicación.", "Aceptar");
                        App.Current.MainPage = new Register();
                        break;

                }

            }


        }

        private void RespuestaInsertUsuario(MTCPersona respuesta)
        {
            MTCPersonaApp.UsuarioConsultado -= RespuestaInsertUsuario;
            Device.BeginInvokeOnMainThread(async () =>
            {
                await Task.Delay(500);
                if (respuesta.EstadoRespuesta == MTCPersonaEstado.ErrorEnBD)
                {
                    await DisplayAlert("Aviso", "Ud. ya se encuentra registrado en la aplicación.", "Aceptar");
                    App.Current.MainPage = new Account();
                    return;
                }
                if (respuesta.EstadoRespuesta == MTCPersonaEstado.ErrorSistema)
                {
                    await DisplayAlert("Aviso", "Hubo un problema en la comunicación con el servidor, por favor intente después.", "Aceptar");
                    return;
                }

                if (respuesta.EstadoRespuesta == MTCPersonaEstado.Registrado)
                {
                    App.Current.Properties["NDocumento"] = MTCPersonaApp.Numero_Documento;
                    App.Current.Properties["TDocucumento"] = MTCPersonaApp.Tipo;
                    App.Current.Properties["dni"] = MTCPersonaApp.Numero_Documento;
                    App.Current.Properties["typedoc"] = MTCPersonaApp.Tipo;
                    IndicadorDeCarga.IsVisible = false;
                    Grilla.IsVisible = true;

                }
            });
        }

        public async void IrAlInicio(object sender, EventArgs e)
        {
            var dni = App.Current.Properties.ContainsKey("NDocumento") ? App.Current.Properties["NDocumento"].ToString() : "";
            var tdoc = App.Current.Properties.ContainsKey("TDocucumento") ? Convert.ToInt32(App.Current.Properties["TDocucumento"]) : 0;
            MTCConducirApp.Tipo = tdoc;
            MTCConducirApp.TipoBusquedaConsultaPuntos = MTCConducirApp.EnumTipoBusquedaConsulta.PorNroDocumento;
            MTCConducirApp.NroDocumento = dni.Trim();

            //  App.Current.MainPage = new Vistas.Inc.MasterDetail();
        }
    }
}