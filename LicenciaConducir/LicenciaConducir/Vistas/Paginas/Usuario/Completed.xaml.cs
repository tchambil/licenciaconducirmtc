﻿using LicenciaConducir.Lib.MTCUsuario;
using LicenciaConducir.Vistas.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LicenciaConducir.Vistas.Paginas.Usuario
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Completed : BasePage
    {
        public bool Cargado = false;
        public Completed()
        {
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            if (Cargado) return;
            MTCPersonaEstado resultado = MTCPersonaEstado.ErrorSistema;
            await Task.Run(async () =>
            {
                MTCPersonaApp.UsuarioToken += _UsuarioToken;
                resultado = MTCPersonaApp.ValidarToken();
            });
            if (resultado != MTCPersonaEstado.EsperandoRespuesta)
            {
                switch (resultado)
                {
                    case MTCPersonaEstado.ErrorInternet:
                        await DisplayAlert("Aviso", "Su dispositivo no cuenta con internet en estos momentos.", "Aceptar");
                        break;
                    case MTCPersonaEstado.ErrorSistema:
                        await DisplayAlert("Aviso", "Hubo un problema en la comunicación con el servidor, por favor intente después.", "Aceptar");
                        break;
                }
                Navigation.RemovePage(this);
            }

        }

        private void _UsuarioToken(MTCPersona respuesta)
        {
            {
                MTCPersonaApp.UsuarioToken -= _UsuarioToken;
                Device.BeginInvokeOnMainThread(async () =>
                {
                    await Task.Delay(500);
                    if (respuesta.EstadoRespuesta == MTCPersonaEstado.ErrorSistema)
                    {
                        await DisplayAlert("Aviso",
                            "Hubo un problema en la comunicación con el servidor, por favor intente después.",
                            "Aceptar");
                        return;
                    }
                    if (respuesta.EstadoRespuesta == MTCPersonaEstado.NoEncontrado)
                    {
                        await DisplayAlert("Aviso",
                            "El código ingresado no es correcto. Por favor, intente nuevamente",
                            "Aceptar");
                        App.Current.MainPage = new VerifyCode();
                        return;
                    }
                    if (respuesta.EstadoRespuesta == MTCPersonaEstado.Registrado)
                    {
                        IndicadorDeCarga.IsRunning = false;
                        IndicadorDeCarga.IsVisible = false;
                        Grilla.IsVisible = true;
                    }

                });
            }
        }

        private void Button_OnClicked(object sender, EventArgs e)
        {
            App.Current.MainPage = new Vistas.Inc.MasterDetailLicencia();
        }
    }
}