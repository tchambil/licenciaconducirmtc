﻿using LicenciaConducir.Lib.MTCUsuario;
using LicenciaConducir.Vistas.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LicenciaConducir.Vistas.Paginas.Usuario
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TimeNotification : BasePage
    {

        public TimeNotification()
        {
            InitializeComponent();
            MTCPersonaApp.NotificacionDias = 0;

        }

        private void OnTapped15(object sender, EventArgs e)
        {


            img15.Source = "img15dias.png";
            img30.Source = "img30diasGris.png";
            img45.Source = "img45diasGris.png";
            MTCPersonaApp.NotificacionDias = 15;

        }
        private void OnTapped30(object sender, EventArgs e)
        {
            img30.Source = "img30dias.png";

            img15.Source = "img15diasGris.png";
            img45.Source = "img45diasGris.png";
            MTCPersonaApp.NotificacionDias = 30;
        }
        private void OnTapped45(object sender, EventArgs e)
        {
            img45.Source = "img45dias.png";

            img15.Source = "img15diasGris.png";
            img30.Source = "img30diasGris.png";
            MTCPersonaApp.NotificacionDias = 45;
        }
        private void OnImg15(object sender, EventArgs e)
        {
            OnTapped15(sender, e);
        }
        private void OnImg30(object sender, EventArgs e)
        {
            OnTapped30(sender, e);
        }
        private void OnImg45(object sender, EventArgs e)
        {
            OnTapped45(sender, e);
        }
        private async void BtnNext_OnClicked(object sender, EventArgs e)
        {
            if (MTCPersonaApp.NotificacionDias <= 0)
            {
                await DisplayAlert("Aviso", "Seleccione el número de días.", "Aceptar");
                return;
            }

            App.Current.MainPage = new VerifyPhone();
        }
    }
}