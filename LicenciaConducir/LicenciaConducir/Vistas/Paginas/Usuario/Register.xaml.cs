﻿using LicenciaConducir.Lib.Include;
using LicenciaConducir.Lib.MTCConducir;
using LicenciaConducir.Lib.MTCUsuario;
using LicenciaConducir.Services;
using LicenciaConducir.Vistas.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LicenciaConducir.Vistas.Paginas.Usuario
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Register : BasePage
    {
        public bool Cargado = false;
        private int _isOwned = 0;
        public NavigationService NavigationService { get; set; }
        public Register()
        {

            //  NavigationPage.SetBackButtonTitle(this, "");
            NavigationService = new NavigationService(Navigation);
            InitializeComponent();
            //MostrarNavegacion = false;
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (selectorTipoDocumento.Items.Count <= 0)
            {
                foreach (var item in MTCConducirTipoDocumento.Tipos())
                {
                    selectorTipoDocumento.Items.Add(item.Value);
                }
                selectorTipoDocumento.SelectedIndex = -1;
            }
            if (selectorDias.Items.Count <= 0)
            {
                foreach (var item in MTCConducirNroDias.Tipos())
                {
                    selectorDias.Items.Add(item.Value);
                }
                selectorDias.SelectedIndex = 0;
            }

        }

        public void CambiarTipoDoc(object sender, EventArgs e)
        {
            try
            {
                TipoDocumento.Text = selectorTipoDocumento.Items[selectorTipoDocumento.SelectedIndex];
            }
            catch (Exception xe)
            {
            }
        }
        public void CambiarDias(object sender, EventArgs e)
        {
            try
            {
                NDias.Text = selectorDias.Items[selectorDias.SelectedIndex];
            }
            catch (Exception xe)
            {
            }
        }
        public void CambiarFecha(object sender, DateChangedEventArgs e)
        {
            try
            {
                FEmision.Text = SelectorFecha.Date.ToString("dd/MM/yyyy");
            }
            catch (Exception xe)
            {
            }
        }
        public void AbrirPickerDias(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                NDias.Unfocus();
                selectorDias.Focus();

            });
        }
        public void AbrirPicker(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                TipoDocumento.Unfocus();
                selectorTipoDocumento.Focus();
            });
        }
        void switcher_Toggled(object sender, ToggledEventArgs e)
        {

            if (e.Value)
            {
                LayoutLabelFEmision.IsVisible = true;
                LayoutFEmision.IsVisible = true;
                LayoutLabelDias.IsVisible = true;
                LayoutDias.IsVisible = true;
                _isOwned = 1;
            }
            else
            {
                LayoutLabelFEmision.IsVisible = false;
                LayoutFEmision.IsVisible = false;
                LayoutLabelDias.IsVisible = false;
                LayoutDias.IsVisible = false;
                _isOwned = 0;
            }

        }
        public void OnBack(object sender, EventArgs e)
        {
            App.Current.MainPage = new Account();
        }
        public async void OnRegister(object sender, EventArgs e)
        {
            if (selectorTipoDocumento.SelectedIndex < 0)
            {
                await DisplayAlert("Aviso", "Seleccione el tipo de documento.", "Aceptar");
                return;
            }
            var valido = NombresValidar.Validar(Nombres.Text);
            valido = valido & ApellidosValidar.Validar(Apellidos.Text);
            valido = valido & EmailValidar.Validar(Email.Text);
            valido = valido & DNIValidar.Validar(DNI.Text);
            //  valido = valido & NDiasValidar.Validar(selectorDias.SelectedIndex.ToString());
            valido = valido & TipoDocumentoValidar.Validar(selectorTipoDocumento.SelectedIndex.ToString());

            if (!valido)
            {
                await DisplayAlert("Aviso", "Por favor llene los campos según se indican.", "Aceptar");
            }
            else
            {

                MTCPersonaApp.Nombres = Nombres.Text.Trim();
                MTCPersonaApp.Apellidos = Apellidos.Text.Trim();
                MTCPersonaApp.Email = Email.Text.Trim();
                MTCPersonaApp.Token = App.Current.Properties["token"].ToString();
                MTCPersonaApp.Tipo = MTCConducirTipoDocumento.Tipos().Where(p => p.Value == selectorTipoDocumento.Items.ElementAt(selectorTipoDocumento.SelectedIndex)).Select(p => p.Key).FirstOrDefault();
                MTCPersonaApp.Numero_Documento = DNI.Text.Trim();
                MTCPersonaApp.IsNotification = _isOwned.ToString();
                MTCPersonaApp.NotificacionDias = _isOwned == 0 ? 0 : MTCConducirNroDias.Tipos().Where(p => p.Value == selectorDias.Items.ElementAt(selectorDias.SelectedIndex)).Select(p => p.Key).FirstOrDefault();
                MTCPersonaApp.Fecha_Emision = _isOwned == 0 ? "" : FEmision.Text;
                if (_isOwned == 0)
                {
                    App.Current.MainPage = new UsuarioCierre();
                }
                else
                {
                    waiting.IsVisible = true;
                    MTCConducirApp.Tipo = MTCConducirTipoDocumento.Tipos().Where(p => p.Value == selectorTipoDocumento.Items.ElementAt(selectorTipoDocumento.SelectedIndex)).Select(p => p.Key).FirstOrDefault();
                    MTCConducirApp.TipoBusquedaConsultaPuntos = MTCConducirApp.EnumTipoBusquedaConsulta.PorNroDocumento;
                    MTCConducirApp.NroDocumento = DNI.Text.Trim();
                    MTCConductorEstado resultado = MTCConductorEstado.ErrorSistema;
                    await Task.Run(async () =>
                    {
                        MTCConducirApp.ConductorConsultado += RespuestaConsultaPuntos;
                        resultado = MTCConducirApp.ConsultarPuntos();
                    });
                    if (resultado != MTCConductorEstado.EsperandoRespuesta)
                    {
                        switch (resultado)
                        {
                            case MTCConductorEstado.ErrorInternet:
                                waiting.IsVisible = false;
                                await DisplayAlert("Aviso", "Su dispositivo no cuenta con internet en estos momentos.", "Aceptar");
                                break;
                            case MTCConductorEstado.ErrorSistema:
                                waiting.IsVisible = false;
                                await DisplayAlert("Aviso", "Hubo un problema de comunicación con el servidor, por favor intente después.", "Aceptar");
                                break;
                        }
                    }
                }


            }
        }
        private async void RespuestaConsultaPuntos(MTCConductor respuesta)
        {
            MTCConducirApp.ConductorConsultado -= RespuestaConsultaPuntos;
            Device.BeginInvokeOnMainThread(async () =>
            {
                await Task.Delay(500);
                if (respuesta.EstadoRespuesta == MTCConductorEstado.NoEncontrado)
                {
                    waiting.IsVisible = false;
                    await DisplayAlert("Aviso", "No hemos encontrado licencia de conducir para el usuario", "Aceptar");

                    return;
                }
                if (respuesta.EstadoRespuesta == MTCConductorEstado.ErrorSistema)
                {
                    waiting.IsVisible = false;
                    await DisplayAlert("Aviso", "Hubo un problema en la comunicación con el servidor, por favor intente después.", "Aceptar");

                    return;
                }
                if (respuesta.EstadoRespuesta == MTCConductorEstado.SinLicencia)
                {
                    waiting.IsVisible = false;
                    await DisplayAlert("Información", "El usuario no cuenta con licencia de conducir", "Aceptar");

                    return;
                }

                if (dalidateDate(respuesta.Persona.Fecha_Revalidacion))
                {
                    waiting.IsVisible = false;
                    App.Current.MainPage = new UsuarioCierre();
                }
                else
                {
                    waiting.IsVisible = false;
                    await DisplayAlert("Información", "Para recibir notificaciones debe ingresar la fecha de revalidación correcta", "Aceptar");

                    return;
                }
                waiting.IsVisible = false;
            });
        }
        bool dalidateDate(string date)
        {
            try
            {
                var stringDate = Helper.ConvertirAFechaStringLocal(date.Substring(0, date.IndexOf(' ')));
                DateTime DateRevalida = Convert.ToDateTime(stringDate);
                if (DateRevalida == SelectorFecha.Date)
                {
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                return false;
            }

        }
        void Entry_TextChangedDocumento(object sender, TextChangedEventArgs e)
        {
            if (selectorTipoDocumento.SelectedIndex < 0)
            {
                if (DNI.Text != "")
                {
                    string _text = DNI.Text;
                    _text = _text.Remove(_text.Length - 1);
                    DNI.Text = _text;
                }

                return;
            }

            if (DNI.Text.Trim().Length > 12)
            {
                return;
            }
            var tipodoc = MTCConducirTipoDocumento.Tipos().Where(p => p.Value == selectorTipoDocumento.Items.ElementAt(selectorTipoDocumento.SelectedIndex)).Select(p => p.Key).FirstOrDefault();
            if (tipodoc == 2)
            {

                DNI.Text = e.NewTextValue.Replace(".", "");
                string _text = DNI.Text;
                if (_text.Length > 8)
                {
                    _text = _text.Remove(_text.Length - 1);
                    DNI.Text = _text;
                }

            }
            if (tipodoc == 4)
            {

                DNI.Text = e.NewTextValue.Replace(".", "");
                string _text = DNI.Text;
                if (_text.Length > 11)
                {
                    _text = _text.Remove(_text.Length - 1);
                    DNI.Text = _text;
                }

            }
            if (tipodoc == 5)
            {

                DNI.Text = e.NewTextValue.Replace(".", "");
                string _text = DNI.Text;
                if (_text.Length > 5)
                {
                    _text = _text.Remove(_text.Length - 1);
                    DNI.Text = _text;
                }

            }
            if (tipodoc == 13)
            {

                DNI.Text = e.NewTextValue.Replace(".", "");
                string _text = DNI.Text;
                if (_text.Length > 9)
                {
                    _text = _text.Remove(_text.Length - 1);
                    DNI.Text = _text;
                }

            }

        }
        void Entry_NombresTextChanged(object sender, TextChangedEventArgs e)
        {
            if (Nombres.Text.Trim().Length > 30)
            {
                return;
            }


            var NombreRegex = @"^[\p{L}\s?]+$";
            Nombres.Text = e.NewTextValue.TrimStart().ToUpper().Replace("  ", " ");
            string _text = Nombres.Text;
            bool EsValido = (Regex.IsMatch(e.NewTextValue, NombreRegex, RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250)));
            if (!EsValido)
            {
                if (_text.Length > 1)
                {
                    _text = _text.Remove(_text.Length - 1);
                    Nombres.Text = _text;
                }
                else
                {
                    Nombres.Text = "";
                }

            }


        }
        void Entry_ApellidosTextChanged(object sender, TextChangedEventArgs e)
        {
            if (Apellidos.Text.Trim().Length > 25)
            {
                return;
            }

            var NombreRegex = @"^[\p{L}\s?]+$";
            Apellidos.Text = e.NewTextValue.TrimStart().ToUpper().Replace("  ", " ");
            string _text = Apellidos.Text;
            bool EsValido = (Regex.IsMatch(e.NewTextValue, NombreRegex, RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250)));
            if (!EsValido)
            {
                if (_text.Length > 1)
                {
                    _text = _text.Remove(_text.Length - 1);
                    Apellidos.Text = _text;
                }
                else
                {
                    Apellidos.Text = "";
                }

            }

        }
        void Entry_EmailTextChanged(object sender, TextChangedEventArgs e)
        {
            if (Email.Text.Trim().Length > 50)
            {
                return;
            }

            var EmailRegex = @"^[\p{L}\p{N}\.@_-\-_]+$";
            Email.Text = e.NewTextValue.ToUpper();
            string _text = Email.Text;
            bool EsValido = (Regex.IsMatch(e.NewTextValue, EmailRegex, RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250)));
            if (!EsValido)
            {
                if (_text.Length > 1)
                {
                    _text = _text.Remove(_text.Length - 1);
                    Email.Text = _text.ToUpper();
                }
                else
                {
                    Email.Text = "";
                }

            }


        }
        public void AbrirFecha(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                FEmision.Unfocus();
                SelectorFecha.Focus();
                FEmision.Text = SelectorFecha.Date.ToString("dd/MM/yyy");
            });
        }
    }
}