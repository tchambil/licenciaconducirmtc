﻿using LicenciaConducir.Lib.MTCUsuario;
using LicenciaConducir.Vistas.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LicenciaConducir.Vistas.Paginas.Usuario
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Question : BasePage
    {
        public Question()
        {
            InitializeComponent();
            Nombres.Text = "!Hola " + MTCPersonaApp.Nombres + " !";
            Vencimiento.Text = MTCPersonaApp.Vencimiento;
        }

        private void BtnNo_OnClicked(object sender, EventArgs e)
        {
            App.Current.MainPage = new Vistas.Inc.MasterDetailLicencia();
        }
        private void BtnYes_OnClicked(object sender, EventArgs e)
        {
            App.Current.MainPage = new TimeNotification();
        }
    }
}