﻿using LicenciaConducir.Lib.MTCUsuario;
using LicenciaConducir.Vistas.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LicenciaConducir.Vistas.Paginas.Usuario
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class VerifyPhone : BasePage
    {
        public VerifyPhone()
        {
            InitializeComponent();
            NroPhone.Text = MTCPersonaApp.NroTelefono;
        }

        private async void BtnNext_OnClicked(object sender, EventArgs e)
        {

            MTCPersonaApp.NroTelefono = NroPhone.Text;
            long n2 = 0;
            if (NroPhone.Text == null)
            {
                await DisplayAlert("Aviso", "Ingrese número de Teléfono.", "Aceptar");
                return;
            }
            if (NroPhone.Text.Length <= 0 | NroPhone.Text == string.Empty)
            {
                await DisplayAlert("Aviso", "Ingrese número de Teléfono.", "Aceptar");
                return;
            }
            bool IsDigito = long.TryParse(NroPhone.Text.Trim(), out n2);

            if (IsDigito)
            {
                if ((NroPhone.Text.Length < 9))
                {
                    await DisplayAlert("Aviso", "Ingrese número de Teléfono de 9 dígitos.", "Aceptar");
                    return;
                }
            }
            else
            {
                await DisplayAlert("Aviso", "Ingrese número de Teléfono de 9 dígitos.", "Aceptar");
                return;
            }

            App.Current.MainPage = new VerifyCode();
        }
    }
}