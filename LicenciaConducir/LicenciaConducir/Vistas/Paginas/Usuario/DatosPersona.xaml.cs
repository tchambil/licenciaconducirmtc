﻿using LicenciaConducir.Lib.Include;
using LicenciaConducir.Lib.MTCConducir;
using LicenciaConducir.Lib.MTCUsuario;
using LicenciaConducir.Services;
using LicenciaConducir.Vistas.Inc;
using LicenciaConducir.Vistas.Lib;
using LicenciaConducir.Vistas.Paginas.Puntos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LicenciaConducir.Vistas.Paginas.Usuario
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DatosPersona : BasePage
    {
        static int contador = 0;
        public bool Cargado = false;
        public DatosPersona()
        {
            InitializeComponent();
        }
        protected override bool OnBackButtonPressed()
        {
            base.OnBackButtonPressed();
            MasterDetailLicencia.DetalleActual.CargarPagina(typeof(Principal), false);
            return true;
        }
        protected override async void OnAppearing()
        {
            base.OnAppearing();
            if (Cargado) return;
            var current = Connectivity.NetworkAccess;
            if (current == NetworkAccess.Internet)
            {


                MTCConducirApp.Tipo = MTCPersonaApp.Tipo;
                MTCConducirApp.NroDocumento = MTCPersonaApp.Numero_Documento;
                MTCConductorEstado resultado = MTCConductorEstado.ErrorSistema;
                await Task.Run(async () =>
                {
                    MTCConducirApp.ConductorConsultado += RespuestaConsultaPuntos;
                    resultado = MTCConducirApp.ConsultarPuntos();
                });
                if (resultado != MTCConductorEstado.EsperandoRespuesta)
                {
                    switch (resultado)
                    {
                        case MTCConductorEstado.ErrorInternet:
                            await DisplayAlert("Aviso", "Su dispositivo no cuenta con internet en estos momentos.", "Aceptar");
                            break;
                        case MTCConductorEstado.ErrorSistema:
                            await DisplayAlert("Aviso", "Hubo un problema de comunicación con el servidor, por favor intente después.", "Aceptar");
                            break;
                    }
                }
            }
            else
            {
                var db = TodoItemDatabase.getDatabase();
                var Persona = await db.GetCiudadano(new ViewModels.Ciudadano()
                {
                    NumeroDocumento = MTCConducirApp.NroDocumento,
                    TipoDocumento = MTCConducirApp.Tipo.ToString(),
                });
                if (Persona == null)
                {
                    await DisplayAlert("Aviso de modo Offline (Desconectado)", "No se encontró información del número documento ingresado.", "Aceptar");
                    App.Current.Properties["NDocumento"] = "";
                    App.Current.Properties["TDocucumento"] = 0;
                    MTCPersonaApp.SinLicencia = true;
                    App.Current.MainPage = new Account();
                    return;
                }
                var lista = new List<ConductorDetalleItem>();
                lista.Add(new ConductorDetalleItem() { Campo = "Nro. de licencia", Valor = Persona.NumeroLicencia });
                lista.Add(new ConductorDetalleItem() { Campo = "Clase y categoría", Valor = Persona.ClaseCategoria });
                lista.Add(new ConductorDetalleItem() { Campo = "Vigente desde", Valor = Helper.ConvertirAFechaString(Persona.FechaExpedicion) });
                lista.Add(new ConductorDetalleItem() { Campo = "Fecha de revalidación", Valor = Helper.ConvertirAFechaString(Persona.FechaRevalidacion) });
                lista.Add(new ConductorDetalleItem() { Campo = "Estado de licencia", Valor = Persona.Estado });

                string sfecha = VencimientoDate(Persona.FechaRevalidacion);
                Nombres.Text = "Sr(a). " + Persona.NombresCompletos;
                Vencimiento.Text = Persona.MensajeVecimiento;
                listaDetalle.ItemsSource = lista;
                await IndicadorDeCarga.FadeTo(0, 25);
                IndicadorDeCarga.IsVisible = false;
                administrado_wrapper.IsVisible = true;
                listaDetalle.IsVisible = true;
                framedatos.IsVisible = true;
                datoslicencia_wrapper.IsVisible = true;
                titulovencimiento_wrapper.IsVisible = true;
                administrado_wrapper.Opacity = 0;
                listaDetalle.Opacity = 0;
                stackbutton.IsVisible = true;
                await administrado_wrapper.FadeTo(1, 150);
                await listaDetalle.FadeTo(1, 150);
                Cargado = true;

            }
        }
        public async void OnPrincipal(object sender, EventArgs e)
        {
            App.Current.MainPage = new Vistas.Inc.MasterDetailLicencia();
        }
        void PrevenirSeleccion(object sender, ItemTappedEventArgs e)
        {
            if (e == null) return; // has been set to null, do not 'process' tapped event 
            ((ListView)sender).SelectedItem = null; // de-select the row
        }
        private async void RespuestaConsultaPuntos(MTCConductor respuesta)
        {

            MTCConducirApp.ConductorConsultado -= RespuestaConsultaPuntos;
            Device.BeginInvokeOnMainThread(async () =>
            {
                await Task.Delay(500);
                if (respuesta.EstadoRespuesta == MTCConductorEstado.NoEncontrado)
                {
                    await DisplayAlert("Aviso", "Ud. no cuenta con licencia de conducir.", "Aceptar");
                    MasterDetailLicencia.DetalleActual.CargarPagina(typeof(Principal), true);
                    return;
                }
                if (respuesta.EstadoRespuesta == MTCConductorEstado.ErrorSistema)
                {
                    await DisplayAlert("Aviso", "Hubo un problema en la comunicación con el servidor, por favor intente después.", "Aceptar");

                    return;
                }
                if (respuesta.EstadoRespuesta == MTCConductorEstado.SinLicencia)
                {
                    await DisplayAlert("Información", "Ud. no cuenta con licencia de conducir.", "Aceptar");
                    MasterDetailLicencia.DetalleActual.CargarPagina(typeof(Principal), true);
                    return;
                }
                var lista = new List<ConductorDetalleItem>();
                lista.Add(new ConductorDetalleItem() { Campo = "Nro. de licencia", Valor = respuesta.Persona.Numero_Licencia });
                lista.Add(new ConductorDetalleItem() { Campo = "Clase y categoría", Valor = respuesta.Persona.Clase_Categoria });
                lista.Add(new ConductorDetalleItem() { Campo = "Vigente desde", Valor = Helper.ConvertirAFechaString(respuesta.Persona.Fecha_Expedicion) });
                lista.Add(new ConductorDetalleItem() { Campo = "Fecha de revalidación", Valor = Helper.ConvertirAFechaString(respuesta.Persona.Fecha_Revalidacion) });
                lista.Add(new ConductorDetalleItem() { Campo = "Estado de licencia", Valor = respuesta.Persona.Estado });

                string sfecha = VencimientoDate(respuesta.Persona.Fecha_Revalidacion);
                Nombres.Text = "Sr(a). " + respuesta.Persona.Nombres_Completos;

                Vencimiento.Text = respuesta.Persona.MensajeVecimiento;
                listaDetalle.ItemsSource = lista;
                await IndicadorDeCarga.FadeTo(0, 25);
                IndicadorDeCarga.IsVisible = false;
                administrado_wrapper.IsVisible = true;
                listaDetalle.IsVisible = true;
                framedatos.IsVisible = true;
                datoslicencia_wrapper.IsVisible = true;
                titulovencimiento_wrapper.IsVisible = true;
                administrado_wrapper.Opacity = 0;
                listaDetalle.Opacity = 0;
                stackbutton.IsVisible = true;
                await administrado_wrapper.FadeTo(1, 150);
                await listaDetalle.FadeTo(1, 150);
                Cargado = true;
            });
        }
        static string VencimientoDate(string date)
        {
            try
            {
                var stringDate = Helper.ConvertirAFechaStringLocal(date.Substring(0, date.IndexOf(' ')));
                DateTime DateRevalida = Convert.ToDateTime(stringDate);
                DateTime newDate = DateTime.Now;
                return Diferencia(DateRevalida, newDate);
            }
            catch (Exception e)
            {
                return "Error";
            }

        }
        static string Diferencia(DateTime newdt, DateTime olddt)
        {
            int anos;
            int meses;
            int dias;
            string str = "";

            anos = (newdt.Year - olddt.Year);
            meses = (newdt.Month - olddt.Month);
            dias = (newdt.Day - olddt.Day);

            if (meses <= 0)
            {
                anos -= 1;
                meses += 12;
            }

            if (dias < 0)
            {
                meses -= 1;
                int DiasAno = newdt.Year;
                int DiasMes = newdt.Month;

                if ((newdt.Month - 1) == 0)
                {
                    DiasAno = DiasAno - 1;
                    DiasMes = 12;
                }
                else
                {
                    DiasMes = DiasMes - 1;
                }

                dias += DateTime.DaysInMonth(DiasAno, DiasMes);
            }

            if (meses == 12)
            {
                anos++;
                meses = 0;
            }

            if (anos < 0)
            {
                return "Vencida";
            }

            if (anos > 0)
            {
                if (anos == 1)
                    str = str + anos.ToString() + " año ";
                else
                    str = str + anos.ToString() + " años ";
            }

            if (meses > 0)
            {
                if (meses == 1)
                    str = str + meses.ToString() + " mes y ";
                else
                    str = str + meses.ToString() + " meses y ";
            }

            if (dias > 0)
            {
                if (dias == 1)
                    str = str + dias.ToString() + " día ";
                else
                    str = str + dias.ToString() + " días ";
            }

            return str;
        }
        static String DiferenciaFechas(DateTime newdt, DateTime olddt)
        { //Adaptado de http://valentingt.blogspot.pe/2010/09/c-diferencia-de-fechas-en-anos-meses-y_30.html 
            Int32 anios;
            Int32 meses;
            Int32 dias;
            String str = "";

            anios = (newdt.Year - olddt.Year);
            meses = (newdt.Month - olddt.Month);
            dias = (newdt.Day - olddt.Day);

            if (meses < 0)
            {
                if (contador == 0)
                {
                    anios -= 1;
                }
                meses += 12;
            }
            if (dias < 0)
            {
                if (contador == 0)
                {
                    meses -= 1;
                }
                dias += DateTime.DaysInMonth(newdt.Year, newdt.Month);
            }

            if (anios <= 0)
            {

                if (contador == 0)
                {
                    contador = 1;
                    return DiferenciaFechas(olddt, newdt);
                }

            }
            if (anios > 0)
            {
                if (anios == 1)
                    str = str + anios.ToString() + " año ";
                else
                    str = str + anios.ToString() + " años ";
            }
            if (meses > 0)
            {
                if (meses == 1)
                {
                    str = str + meses.ToString() + " mes";
                }

                else
                {
                    str = str + meses.ToString() + " meses";
                }
                if (dias > 0)
                {
                    str = str + " y ";
                }
            }
            if (dias > 0)
            {
                if (dias == 1)
                    str = str + dias.ToString() + " día ";
                else
                    str = str + dias.ToString() + " días ";
            }
            return str;
        }
    }
}