﻿using LicenciaConducir.Lib.MTCConducir;
using LicenciaConducir.Vistas.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LicenciaConducir.Vistas.Paginas.Puntos
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ConsultaPuntosCurso : BasePage
    {
        public ConsultaPuntosCurso()
        {
            InitializeComponent();
            NavigationPage.SetBackButtonTitle(this, "");
        }
        public MTC.ServiciosPuntos.Dominio.Entidades.LC_TM_CursoJornada Curso
        {
            get
            {
                return MTCConducirApp.CurrentCurso;
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            var lista = new List<ConductorDetalleItem>();
            lista.Add(new ConductorDetalleItem() { Campo = "Capacitación", Valor = Curso.Capacitacion });
            lista.Add(new ConductorDetalleItem() { Campo = "Entidad", Valor = Curso.Entidad });
            lista.Add(new ConductorDetalleItem() { Campo = "Nro. de certificado", Valor = Curso.Numero_Certificado });
            lista.Add(new ConductorDetalleItem() { Campo = "Fecha de capacitación", Valor = Curso.Fecha_Capacitacion });
            lista.Add(new ConductorDetalleItem() { Campo = "Estado", Valor = Curso.Estado });
            lista.Add(new ConductorDetalleItem() { Campo = "Puntos acumulados", Valor = Curso.Puntos_Acumula.ToString() });
            lista.Add(new ConductorDetalleItem() { Campo = "Puntos - Saldo", Valor = Curso.Puntos_Saldo.ToString() });

            lista.Add(new ConductorDetalleItem() { Campo = "Nro. de papeleta vinculada", Valor = Curso.Numero_Papeleta });
            lista.Add(new ConductorDetalleItem() { Campo = "Código de falta en papeleta", Valor = Curso.Cod_Falta });
            lista.Add(new ConductorDetalleItem() { Campo = "Fecha de infracción", Valor = Curso.Fecha_Infraccion });

            listaDetalle.ItemsSource = lista;
            LabelCapacitacion.Text = Curso.Capacitacion.Length > 28 ? (Curso.Capacitacion.Substring(0, 25) + "...") : Curso.Capacitacion;
            LabelEntidad.Text = Curso.Entidad.Length > 38 ? (Curso.Entidad.Substring(0, 35) + "...") : Curso.Entidad;
            LabelPuntosFirmes.Text = Curso.Puntos_Acumula.ToString();
        }
        void PrevenirSeleccion(object sender, ItemTappedEventArgs e)
        {
            if (e == null) return; // has been set to null, do not 'process' tapped event 
            ((ListView)sender).SelectedItem = null; // de-select the row
        }
    }
}