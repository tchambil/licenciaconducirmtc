﻿using LicenciaConducir.Lib.MTCConducir;
using LicenciaConducir.Services;
using LicenciaConducir.Vistas.Inc;
using LicenciaConducir.Vistas.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LicenciaConducir.Vistas.Paginas.Puntos
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SearchPuntos : BasePage
    {
        public NavigationService NavigationService { get; set; }
        public SearchPuntos()
        {
            InitializeComponent();
            if (Device.OS == TargetPlatform.iOS)
            {
                this.Icon = "IconoDni.png";
            }
            MostrarAnimacion = false;
            NavigationService = new NavigationService(Navigation);
            frPorLicencia.IsVisible = false;
        }
        protected async override void OnAppearing()
        {
            base.OnAppearing();
            var current = Connectivity.NetworkAccess;
            if (current == NetworkAccess.Internet)
            {
                if (selectorTipoDocumento.Items.Count <= 0)
                {
                    foreach (var item in MTCConducirTipoDocumento.Tipos())
                    {
                        selectorTipoDocumento.Items.Add(item.Value);
                    }
                    selectorTipoDocumento.SelectedIndex = -1;
                }
                var _tipodocumento = App.Current.Properties.ContainsKey("TDocucumento") ? Convert.ToInt32(App.Current.Properties["TDocucumento"]) : 0;
                if (_tipodocumento != 0)
                {
                    selectorTipoDocumento.SelectedItem = MTCConducirTipoDocumento.Tipos().Where(p => p.Key == 2).Select(p => p.Value).FirstOrDefault(); ;
                    NroDocumento.Text = App.Current.Properties.ContainsKey("NDocumento") ? App.Current.Properties["NDocumento"].ToString() : "";
                }
            }
            else
            {
                await DisplayAlert("Aviso", "Disponible para modo conectado, su dispositivo debe estar conectado a internet.", "Aceptar");
                App.Current.MainPage = new Vistas.Inc.MasterDetailLicencia();
                return;
            }
           
        }
        public async void EnviarConsulta(object sender, EventArgs e)
        {
            if (selectorTipoDocumento.SelectedIndex < 0)
            {
                await DisplayAlert("Aviso", "Seleccione el tipo de documento.", "Aceptar");
                return;
            }

            var tipodoc = MTCConducirTipoDocumento.Tipos().Where(p => p.Value == selectorTipoDocumento.Items.ElementAt(selectorTipoDocumento.SelectedIndex)).Select(p => p.Key).FirstOrDefault();

            long n2 = 0;
            if (NroDocumento.Text == null)
            {
                await DisplayAlert("Aviso", "Ingrese número de documento.", "Aceptar");
                return;
            }

            if (NroDocumento.Text.Length <= 0 | NroDocumento.Text == string.Empty)
            {
                await DisplayAlert("Aviso", "Ingrese número de documento.", "Aceptar");
                return;
            }
            bool IsDigito = long.TryParse(NroDocumento.Text.Trim(), out n2);
            if (tipodoc == MTCConducirTipoDocumento.DNI)
            {
                if (IsDigito)
                {
                    if ((NroDocumento.Text.Length < 8 | NroDocumento.Text.Length > 8))
                    {
                        await DisplayAlert("Aviso", "Ingrese número de documento de 8 dígitos.", "Aceptar");
                        return;
                    }
                }
                else
                {
                    await DisplayAlert("Aviso", "Ingrese número de documento de 8 dígitos.", "Aceptar");
                    return;
                }
            }
            if (tipodoc == MTCConducirTipoDocumento.CARNET_EXTRANJERIA)
            {
                if (IsDigito)
                {
                    if ((NroDocumento.Text.Length < 5 | NroDocumento.Text.Length > 11))
                    {
                        await DisplayAlert("Aviso", "Ingrese número de documento válido.", "Aceptar");
                        return;
                    }
                }
                else
                {
                    await DisplayAlert("Aviso", "Ingrese número de documento válido.", "Aceptar");
                    return;
                }
            }
            if (tipodoc == MTCConducirTipoDocumento.CARNET_SOLICITANTE)
            {
                if (IsDigito)
                {
                    if ((NroDocumento.Text.Length < 5 | NroDocumento.Text.Length > 5))
                    {
                        await DisplayAlert("Aviso", "Ingrese número de documento de 5 dígitos.", "Aceptar");
                        return;
                    }
                }
                else
                {
                    await DisplayAlert("Aviso", "Ingrese número de documento de 5 dígitos.", "Aceptar");
                    return;
                }
            }
            if (tipodoc == MTCConducirTipoDocumento.TARJETA_IDENTIDAD)
            {
                if (IsDigito)
                {
                    if ((NroDocumento.Text.Length < 9 | NroDocumento.Text.Length > 9))
                    {
                        await DisplayAlert("Aviso", "Ingrese número de documento de 9 dígitos.", "Aceptar");
                        return;
                    }
                }
                else
                {
                    await DisplayAlert("Aviso", "Ingrese número de documento de 9 dígitos.", "Aceptar");
                    return;
                }
            }

            MTCConducirApp.Tipo = MTCConducirTipoDocumento.Tipos().Where(p => p.Value == selectorTipoDocumento.Items.ElementAt(selectorTipoDocumento.SelectedIndex)).Select(p => p.Key).FirstOrDefault();
            MTCConducirApp.TipoBusquedaConsultaPuntos = MTCConducirApp.EnumTipoBusquedaConsulta.PorNroDocumento;
            MTCConducirApp.NroDocumento = NroDocumento.Text.Trim();
            MTCConducirApp.TipoConsultaUsuario = MTCTipoConsulta.DOCUMENTO;
            // MasterDetail.DetalleActual.CargarPagina(typeof(ConsultaPuntosDetalle), true);
            await NavigationService.PushAsync(new ConsultaPuntosDetalle());

        }
        public async void EnviarConsultaLicencia(object sender, EventArgs e)
        {
            if (NroLicencia.Text == null)
            {
                await DisplayAlert("Aviso", "Ingrese número de licencia.", "Aceptar");
                return;
            }

            var valido = NroLicenciaValidar.Validar(NroLicencia.Text.Trim().ToUpper());
            if (!valido)
            {
                DisplayAlert("Aviso", "Por favor ingrese número de licencia válida.", "Aceptar");
            }
            else
            {
                MTCConducirApp.TipoBusquedaConsultaPuntos = MTCConducirApp.EnumTipoBusquedaConsulta.PorNroLicencia;
                MTCConducirApp.NroLicencia = NroLicencia.Text.Trim().ToUpper();
                MTCConducirApp.TipoConsultaUsuario = MTCTipoConsulta.LICENCIACONDUCIR;
                // MasterDetail.DetalleActual.CargarPagina(typeof(ConsultaPuntosDetalle), true);
                await NavigationService.PushAsync(new ConsultaPuntosDetalle());
            }
        }
        void Entry_LicenciaTextChanged(object sender, TextChangedEventArgs e)
        {
            if (NroLicencia.Text.Trim().Length > 12)
            {
                return;
            }
            var NombreRegex = @"^[\p{L}\p{N}]+$";
            NroLicencia.Text = e.NewTextValue.ToUpper().Replace(".", "");
            string _text = NroLicencia.Text;
            bool EsValido = (Regex.IsMatch(e.NewTextValue, NombreRegex, RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250)));
            if (!EsValido)
            {
                if (_text.Length > 1)
                {
                    _text = _text.Remove(_text.Length - 1);
                    NroLicencia.Text = _text.ToUpper();
                }
                else
                {
                    NroLicencia.Text = "";
                }

            }


        }
        void Entry_TextChangedDocumento(object sender, TextChangedEventArgs e)
        {
            if (selectorTipoDocumento.SelectedIndex < 0)
            {
                if (NroDocumento.Text != "")
                {
                    string _text = NroDocumento.Text;
                    _text = _text.Remove(_text.Length - 1);
                    NroDocumento.Text = _text;
                }

                return;
            }
            if (NroDocumento.Text.Trim().Length > 12)
            {
                return;
            }

            var tipodoc = MTCConducirTipoDocumento.Tipos().Where(p => p.Value == selectorTipoDocumento.Items.ElementAt(selectorTipoDocumento.SelectedIndex)).Select(p => p.Key).FirstOrDefault();
            if (tipodoc == 2)
            {

                NroDocumento.Text = e.NewTextValue.Replace(".", "");
                string _text = NroDocumento.Text;
                if (_text.Length > 8)
                {
                    _text = _text.Remove(_text.Length - 1);
                    NroDocumento.Text = _text;
                }

            }
            if (tipodoc == 4)
            {

                NroDocumento.Text = e.NewTextValue.Replace(".", "");
                string _text = NroDocumento.Text;
                if (_text.Length > 11)
                {
                    _text = _text.Remove(_text.Length - 1);
                    NroDocumento.Text = _text;
                }

            }
            if (tipodoc == 5)
            {

                NroDocumento.Text = e.NewTextValue.Replace(".", "");
                string _text = NroDocumento.Text;
                if (_text.Length > 5)
                {
                    _text = _text.Remove(_text.Length - 1);
                    NroDocumento.Text = _text;
                }

            }
            if (tipodoc == 13)
            {

                NroDocumento.Text = e.NewTextValue.Replace(".", "");
                string _text = NroDocumento.Text;
                if (_text.Length > 9)
                {
                    _text = _text.Remove(_text.Length - 1);
                    NroDocumento.Text = _text;
                }

            }

        }
        public void CambiarTipoDoc(object sender, EventArgs e)
        {
            try
            {
                TipoDocumento.Text = selectorTipoDocumento.Items[selectorTipoDocumento.SelectedIndex];
            }
            catch (Exception xe)
            {
            }
        }

        public void AbrirPicker(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                TipoDocumento.Unfocus();
                selectorTipoDocumento.Focus();
            });
        }
        public void SalirDeLaAplicacion(object obj)
        {
#if __ANDROID__
            var activity = (Android.App.Activity)Forms.Context;
            activity.FinishAffinity();
#elif __IOS__
            throw new Exception();
#endif
        }
        protected override bool OnBackButtonPressed()
        {
            base.OnBackButtonPressed();
            MasterDetailLicencia.DetalleActual.CargarPagina(typeof(Principal), false);
            return true;
        }
        private void TapSearchDocumento(object sender, EventArgs e)
        {
            frPorDocumento.IsVisible = true;
            frPorLicencia.IsVisible = false;
            lblPorDocumentoSB.IsVisible = false;
            lblPorDocumentoB.IsVisible = true;
            lblPorLicenciaSB.IsVisible = true;
            lblPorLicenciaB.IsVisible = false;
            boxPorLicencia.Color = Color.FromHex("#cdcfd0");
            boxPorDocumento.Color = Color.FromHex("#f01E41");
        }

        private void TapSearchLicencia(object sender, EventArgs e)
        {
            frPorLicencia.IsVisible = true;
            frPorDocumento.IsVisible = false;
            lblPorDocumentoSB.IsVisible = true;
            lblPorDocumentoB.IsVisible = false;
            lblPorLicenciaSB.IsVisible = false;
            lblPorLicenciaB.IsVisible = true;
            boxPorDocumento.Color = Color.FromHex("#cdcfd0");
            boxPorLicencia.Color = Color.FromHex("#f01E41");
        }

    }
}