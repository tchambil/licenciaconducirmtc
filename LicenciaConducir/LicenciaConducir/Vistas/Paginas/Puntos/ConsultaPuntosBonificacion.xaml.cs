﻿using LicenciaConducir.Lib.Include;
using LicenciaConducir.Lib.MTCConducir;
using LicenciaConducir.Vistas.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LicenciaConducir.Vistas.Paginas.Puntos
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ConsultaPuntosBonificacion : BasePage
    {
        public MTC.ServiciosPuntos.Dominio.Entidades.LC_TM_Bonificacion Bonificacion
        {
            get
            {
                return MTCConducirApp.CurrentBonificacion;
            }
        }
        public ConsultaPuntosBonificacion()
        {
            InitializeComponent();
            NavigationPage.SetBackButtonTitle(this, "");
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            LabelNroBonificacion.Text = Bonificacion.Nro_Documento;
            LabelFecha.Text = Bonificacion.Fecha_Bonificacion;
            LabelPuntos.Text = Bonificacion.Puntos_Bonificacion.ToString();

            var lista = new List<ConductorDetalleItem>();
            lista.Add(new ConductorDetalleItem() { Campo = "Nro. de documento", Valor = Bonificacion.Nro_Documento });
            lista.Add(new ConductorDetalleItem() { Campo = "Fecha de bonificación", Valor = Helper.ConvertirAFechaString(Bonificacion.Fecha_Bonificacion) });
            lista.Add(new ConductorDetalleItem() { Campo = "Vigente hasta", Valor = Helper.ConvertirAFechaString(MTCConducirApp.CurrentResultado.Persona.Fecha_Revalidacion) }); //TODO: Cuál es el campo de vigencia?
            lista.Add(new ConductorDetalleItem() { Campo = "Disponible", Valor = (Bonificacion.Puntos_Saldo ?? 0).ToString() });
            lista.Add(new ConductorDetalleItem() { Campo = "Utilizado", Valor = (Bonificacion.Puntos_Utilizados ?? 0).ToString() });

            if ((Bonificacion.Puntos_Utilizados ?? 0) > 0)
            {
                var listaAsociada = new List<ConductorDetalleItem>();
                listaAsociada.Add(new ConductorDetalleItem() { Campo = "Entidad", Valor = Bonificacion.Entidad });
                listaAsociada.Add(new ConductorDetalleItem() { Campo = "Papeleta", Valor = Bonificacion.Numero_Papeleta });
                listaAsociada.Add(new ConductorDetalleItem() { Campo = "Fecha de infracción", Valor = Helper.ConvertirAFechaString(Bonificacion.Fecha_Infraccion) });
                listaAsociada.Add(new ConductorDetalleItem() { Campo = "Fecha firme", Valor = Helper.ConvertirAFechaString(Bonificacion.Fecha_Firme) });
                listaAsociada.Add(new ConductorDetalleItem() { Campo = "Falta", Valor = Bonificacion.Cod_Falta });
                listaAsociada.Add(new ConductorDetalleItem() { Campo = "Puntos", Valor = (Bonificacion.Puntos_Infraccion ?? 0).ToString() });
                papeleta_asociada_list.ItemsSource = listaAsociada;
                papeleta_asociada_list.HeightRequest = listaAsociada.Count() * (papeleta_asociada_list.RowHeight + 1);
                papeleta_asociada_list.IsVisible = false;
                papeleta_asociada.IsVisible = false;
            }

            listaDetalle.ItemsSource = lista;
            listaDetalle.HeightRequest = lista.Count() * (listaDetalle.RowHeight + 1);
        }
        void PrevenirSeleccion(object sender, ItemTappedEventArgs e)
        {
            if (e == null) return; // has been set to null, do not 'process' tapped event 
            ((ListView)sender).SelectedItem = null; // de-select the row
        }
    }
}