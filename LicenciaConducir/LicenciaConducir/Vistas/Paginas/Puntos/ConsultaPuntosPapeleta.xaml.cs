﻿using LicenciaConducir.Lib.MTCConducir;
using LicenciaConducir.Vistas.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LicenciaConducir.Vistas.Paginas.Puntos
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ConsultaPuntosPapeleta : BasePage
    {

        public MTC.ServiciosPuntos.Dominio.Entidades.LC_TM_Papeletas Papeleta
        {
            get
            {
                return MTCConducirApp.CurrentPapeleta;
            }
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            LabelCodFalta.Text = Papeleta.Cod_Falta;
            // LabelEntidad.Text = Papeleta.Entidad;
            LabelFechaInfraccion.Text = "Fecha: " + Papeleta.Fecha_Infraccion;
            LabelPuntosFirmes.Text = Papeleta.Puntos_Firmes.ToString();
            var lista = new List<ConductorDetalleItem>();
            lista.Add(new ConductorDetalleItem() { Campo = "Nro. de papeleta", Valor = Papeleta.Nro_Papeleta });
            lista.Add(new ConductorDetalleItem() { Campo = "Nro. de resolución", Valor = Papeleta.Nro_Resolucion });
            lista.Add(new ConductorDetalleItem() { Campo = "Entidad", Valor = Papeleta.Entidad });
            lista.Add(new ConductorDetalleItem() { Campo = "Fecha", Valor = Papeleta.Fecha_Infraccion });
            lista.Add(new ConductorDetalleItem() { Campo = "Fecha firme", Valor = Papeleta.Fecha_Firma });
            lista.Add(new ConductorDetalleItem() { Campo = "Falta", Valor = Papeleta.Cod_Falta });
            lista.Add(new ConductorDetalleItem() { Campo = "Puntos firmes", Valor = Papeleta.Puntos_Firmes.ToString() });
            lista.Add(new ConductorDetalleItem() { Campo = "Puntos en proceso", Valor = Papeleta.Puntos_Saldo.ToString() });
            listaDetalle.ItemsSource = lista;
        }
        public ConsultaPuntosPapeleta()
        {
            InitializeComponent();
            NavigationPage.SetBackButtonTitle(this, "");
        }
        void PrevenirSeleccion(object sender, ItemTappedEventArgs e)
        {
            if (e == null) return; // has been set to null, do not 'process' tapped event 
            ((ListView)sender).SelectedItem = null; // de-select the row
        }
    }
}