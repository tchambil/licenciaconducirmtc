﻿using LicenciaConducir.Lib.Include;
using LicenciaConducir.Lib.MTCConducir;
using LicenciaConducir.Services;
using LicenciaConducir.Vistas.Lib;
using MTC.ServiciosPuntos.Dominio.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LicenciaConducir.Vistas.Paginas.Puntos
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public class ConductorDetalleItem
    {
        public string Campo { get; set; }
        public double FontSize { get; set; }
        public string Valor { get; set; }
        public ConductorDetalleItem()
        {
            FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label));
        }
    }
    public partial class ConsultaPuntosDetalle : BasePage
    {
        public NavigationService NavigationService { get; set; }
        public ConsultaPuntosDetalle()
        {
            InitializeComponent();
            NavigationService = new NavigationService(Navigation);
            NavigationPage.SetBackButtonTitle(this, "");
        }

        public bool Cargado = false;
        protected override async void OnAppearing()
        {
            base.OnAppearing();
            if (Cargado) return;
            MTCConductorEstado resultado = MTCConductorEstado.ErrorSistema;
            await Task.Run(async () =>
            {
                MTCConducirApp.ConductorConsultado += RespuestaConsultaPuntos;
                resultado = MTCConducirApp.ConsultarPuntos();
            });
            if (resultado != MTCConductorEstado.EsperandoRespuesta)
            {
                switch (resultado)
                {
                    case MTCConductorEstado.ErrorInternet:
                        await DisplayAlert("Aviso", "Su dispositivo no cuenta con internet en estos momentos.", "Aceptar");
                        break;
                    case MTCConductorEstado.ErrorSistema:
                        await DisplayAlert("Aviso", "Hubo un problema de comunicación con el servidor, por favor intente después.", "Aceptar");
                        break;
                }
                Navigation.RemovePage(this);
            }
        }

        public void OnPapeletas(object sender, EventArgs e)
        {


            listaPapeletas.IsVisible = !listaPapeletas.IsVisible;
            stackTitlePapeletas.IsVisible = !stackTitlePapeletas.IsVisible;
            if (listaPapeletas.IsVisible)
            {
                img1.Source = "iconCollapseOut.png";

                //datos
                listaDetalle.IsVisible = false;
                imgdatos.Source = "iconCollapse.png";
                //Bonificaciones 
                listaBonificaciones.IsVisible = false;
                img3.Source = "iconCollapse.png";
                //Cursos
                listaCursos.IsVisible = false;
                img2.Source = "iconCollapse.png";
            }
            else
            {
                img1.Source = "iconCollapse.png";
            }

        }
        public void OnCursos(object sender, EventArgs e)
        {
            listaCursos.IsVisible = !listaCursos.IsVisible;
            if (listaCursos.IsVisible)
            {
                img2.Source = "iconCollapseOut.png";
                //datos
                listaDetalle.IsVisible = false;
                imgdatos.Source = "iconCollapse.png";
                //Bonificaciones 
                listaBonificaciones.IsVisible = false;

                img3.Source = "iconCollapse.png";
                //Papeletas
                listaPapeletas.IsVisible = false;
                stackTitlePapeletas.IsVisible = false;
                img1.Source = "iconCollapse.png";


            }
            else
            {
                img2.Source = "iconCollapse.png";
            }

        }
        public void OnBonificaciones(object sender, EventArgs e)
        {

            listaBonificaciones.IsVisible = !listaBonificaciones.IsVisible;
            if (listaBonificaciones.IsVisible)
            {
                img3.Source = "iconCollapseOut.png";

                //datos
                listaDetalle.IsVisible = false;
                imgdatos.Source = "iconCollapse.png";

                //Papeletas
                listaPapeletas.IsVisible = false;
                stackTitlePapeletas.IsVisible = false;
                img1.Source = "iconCollapse.png";
                //Cursos
                listaCursos.IsVisible = false;
                img2.Source = "iconCollapse.png";
            }
            else
            {
                img3.Source = "iconCollapse.png";
            }

        }


        public void OnDatos(object sender, EventArgs e)
        {
            listaDetalle.IsVisible = !listaDetalle.IsVisible;
            if (listaDetalle.IsVisible)
            {
                imgdatos.Source = "iconCollapseOut.png";
                //Bonificaciones 
                listaBonificaciones.IsVisible = false;
                img3.Source = "iconCollapse.png";
                //Papeletas
                listaPapeletas.IsVisible = false;
                stackTitlePapeletas.IsVisible = false;
                img1.Source = "iconCollapse.png";
                //Cursos
                listaCursos.IsVisible = false;
                img2.Source = "iconCollapse.png";
            }
            else
            {
                imgdatos.Source = "iconCollapse.png";
            }
        }
        void PrevenirSeleccion(object sender, ItemTappedEventArgs e)
        {
            if (e == null) return; // has been set to null, do not 'process' tapped event 
            ((ListView)sender).SelectedItem = null; // de-select the row
        }

        private async void RespuestaConsultaPuntos(MTCConductor respuesta)
        {
            MTCConducirApp.ConductorConsultado -= RespuestaConsultaPuntos;
            Device.BeginInvokeOnMainThread(async () =>
            {
                string menssage = string.Empty;
                if (MTCConducirApp.TipoConsultaUsuario == MTCTipoConsulta.APELLIDOS)
                {
                    menssage = "No se encontró información que coincida con los parámetro ingresados.";
                }
                else if (MTCConducirApp.TipoConsultaUsuario == MTCTipoConsulta.DOCUMENTO)
                {
                    menssage = "No se encontró información del número de documento ingresado.";
                }

                else if (MTCConducirApp.TipoConsultaUsuario == MTCTipoConsulta.RECORD)
                {
                    menssage = "No se encontró información del número de record ingresado.";
                }
                else if (MTCConducirApp.TipoConsultaUsuario == MTCTipoConsulta.LICENCIACONDUCIR)
                {
                    menssage = "No se encontró información del número de licencia ingresado.";
                }
                else
                {
                    menssage = "No se encontró información del número de documento ingresado.";
                }

                await Task.Delay(500);
                if (respuesta.EstadoRespuesta == MTCConductorEstado.NoEncontrado)
                {
                    await DisplayAlert("Aviso", menssage, "Aceptar");
                    Navigation.RemovePage(this);
                    return;
                }
                if (respuesta.EstadoRespuesta == MTCConductorEstado.ErrorSistema)
                {
                    await DisplayAlert("Aviso", "Hubo un problema en la comunicación con el servidor, por favor intente después.", "Aceptar");
                    Navigation.RemovePage(this);
                    return;
                }
                if (respuesta.EstadoRespuesta == MTCConductorEstado.SinLicencia)
                {
                    await DisplayAlert("Información", menssage, "Aceptar");
                    Navigation.RemovePage(this);
                    return;
                }


                var lista = new List<ConductorDetalleItem>();
                lista.Add(new ConductorDetalleItem() { Campo = "Nombre completo", Valor = respuesta.Persona.Nombres_Completos });
                // lista.Add(new ConductorDetalleItem() { Campo = "Nro. de documento", Valor = respuesta.Persona.Numero_Documento });
                lista.Add(new ConductorDetalleItem() { Campo = "Nro. de licencia", Valor = respuesta.Persona.Numero_Licencia });
                lista.Add(new ConductorDetalleItem() { Campo = "Clase y categoría", Valor = respuesta.Persona.Clase_Categoria });
                lista.Add(new ConductorDetalleItem() { Campo = "Vigente desde", Valor = Helper.ConvertirAFechaString(respuesta.Persona.Fecha_Expedicion) });
                lista.Add(new ConductorDetalleItem() { Campo = "Fecha de revalidación", Valor = Helper.ConvertirAFechaString(respuesta.Persona.Fecha_Revalidacion) });
                //lista.Add(new ConductorDetalleItem() { Campo = "Emitido", Valor = Helper.ConvertirAFechaString(respuesta.Persona.Fecha_Emision) });
                lista.Add(new ConductorDetalleItem() { Campo = "Estado de licencia", Valor = respuesta.Persona.Estado });
                lblNameCiudadano.Text = "¡Hola " + respuesta.Persona.Nombres_Completos + "!"; ;
                lblDniCiudadano.Text = respuesta.Persona.Numero_Documento;
                int countMGraves = 0;
                int countGraves = 0;
                int countPFirmes = 0;
                int countTPapeletas = 0;
                //listaDetalle.ItemsSource = lista;
                //  listaDetalle.HeightRequest = lista.Count() * (listaDetalle.RowHeight + 1);
                if (respuesta.Papeletas != null)
                {
                    int count = 0;
                    foreach (var item in respuesta.Papeletas)
                    {
                        count++;
                        item.Fecha_Firma = "(" + Helper.ConvertirAFechaString(item.Fecha_Firma) + ")";
                        item.Fecha_Firma_Dato = Helper.ConvertirAFechaString(item.Fecha_Firma_Dato);
                        item.Fecha_Infraccion = Helper.ConvertirAFechaString(item.Fecha_Infraccion);
                        if (item.Nivel_Gravedad == "G")
                        {
                            countGraves++;
                        }
                        if (item.Nivel_Gravedad == "M")
                        {
                            countMGraves++;
                        }
                        countPFirmes += item.Puntos_Firmes == null ? 0 : Convert.ToInt32(item.Puntos_Firmes);
                        item.Cod_Falta = "Falta " + item.Cod_Falta;
                        countTPapeletas++;
                    }
                    if (count > 0)
                    {
                        listaPapeletas.ItemsSource = respuesta.Papeletas;
                        listaPapeletas.HeightRequest = respuesta.Papeletas.Count() * (listaPapeletas.RowHeight + 1);
                        listaPapeletas.ItemTapped += MostrarPapeleta;
                    }
                    else
                    {
                        var AnyPapeletas = new[]
                        { new LC_TM_Papeletas() {
                            Entidad = "",
                            Fecha_Infraccion = "",
                            Cod_Falta = "No Registra Papeletas",
                            Puntos_Firmes = 0 }
                        };
                        listaPapeletas.ItemsSource = AnyPapeletas;
                    }

                }
                else
                {
                    var AnyPapeletas = new[]
                                           { new LC_TM_Papeletas() {
                            Entidad = "",
                            Fecha_Infraccion = "",
                            Cod_Falta = "No Registra Papeletas",
                            Puntos_Firmes = 0 }
                        };
                    listaPapeletas.ItemsSource = AnyPapeletas;
                }

                if (respuesta.Bonificaciones != null)
                {
                    int count = 0;
                    foreach (var itemBonificacion in respuesta.Bonificaciones)
                    {
                        count++;
                        itemBonificacion.Fecha_Bonificacion = Helper.ConvertirAFechaString(itemBonificacion.Fecha_Bonificacion);
                        itemBonificacion.Fecha_Firma = Helper.ConvertirAFechaString(itemBonificacion.Fecha_Firma);
                        itemBonificacion.Fecha_Firme = Helper.ConvertirAFechaString(itemBonificacion.Fecha_Firme);
                        itemBonificacion.Fecha_Infraccion = Helper.ConvertirAFechaString(itemBonificacion.Fecha_Infraccion);
                        itemBonificacion.Nro_Documento = "Nro " + itemBonificacion.Nro_Documento;
                    }
                    if (count > 0)
                    {
                        listaBonificaciones.HeightRequest = respuesta.Bonificaciones.Count() * (listaBonificaciones.RowHeight + 1);
                        listaBonificaciones.ItemsSource = respuesta.Bonificaciones;
                        listaBonificaciones.ItemTapped += MostrarBonificacion;
                    }
                    else
                    {
                        var AnyBonificaciones = new[]
                       { new LC_TM_Bonificacion() {
                        Puntos_Saldo = 0,
                        Fecha_Bonificacion = "",
                        Nro_Documento = "No Registra Bonificaciones" }
                    };
                        listaBonificaciones.ItemsSource = AnyBonificaciones;
                    }

                }
                else
                {
                    var AnyBonificaciones = new[]
                                        { new LC_TM_Bonificacion() {
                        Puntos_Saldo = 0,
                        Fecha_Bonificacion = "",
                        Nro_Documento = "No Registra Bonificaciones" }
                    };
                    listaBonificaciones.ItemsSource = AnyBonificaciones;
                }


                if (respuesta.CursosJornadas != null)
                {
                    int count = 0;
                    foreach (var jornada in respuesta.CursosJornadas)
                    {
                        count++;
                        jornada.CapacitacionShort = jornada.Capacitacion.Length > 28 ? (jornada.Capacitacion.Substring(0, 25) + "...") : jornada.Capacitacion;
                        jornada.EntidadShort = jornada.Entidad.Length > 38 ? (jornada.Entidad.Substring(0, 35) + "...") : jornada.Entidad;
                        jornada.Fecha_Capacitacion = Helper.ConvertirAFechaString(jornada.Fecha_Capacitacion);
                        jornada.Fecha_Firme = Helper.ConvertirAFechaString(jornada.Fecha_Firme);
                        jornada.Fecha_Infraccion = Helper.ConvertirAFechaString(jornada.Fecha_Infraccion);
                    }
                    if (count > 0)
                    {
                        listaCursos.ItemsSource = respuesta.CursosJornadas;
                        if (respuesta.CursosJornadas != null)
                        {
                            listaCursos.HeightRequest = respuesta.CursosJornadas.Count() * (listaCursos.RowHeight + 1);
                        }
                        listaCursos.ItemTapped += MostrarCursos;

                    }
                    else
                    {
                        var AnyCursos = new[]
                      { new LC_TM_CursoJornada() {
                            CapacitacionShort = "No Registra Cursos",
                            EntidadShort = "",
                            Puntos_Acumula = 0 }
                        };
                        listaCursos.ItemsSource = AnyCursos;
                    }
                }
                else
                {
                    var AnyCursos = new[]
                   { new LC_TM_CursoJornada() {
                            CapacitacionShort = "No Registra Cursos",
                            EntidadShort = "",
                            Puntos_Acumula = 0 }
                        };
                    listaCursos.ItemsSource = AnyCursos;
                }


                await IndicadorDeCarga.FadeTo(0, 25);
                IndicadorDeCarga.IsVisible = false;
                administrado_wrapper.IsVisible = true;

                framedatos.IsVisible = true;

                lblmesage1.Text = "A UD. LE CORRESPONDE RETENCION DE LICENCIA, MEDIDA PREVENTIVA SEGÚN ART. 299 INCISO 1 del D.S. N 003-2014-MTC";
                lblmesage2.Text = "UD. HA SUPERADO EN " + (countPFirmes - 100) + " PUNTOS EL LIMITE MÁXIMO ESTABLECIDO 100 PUNTOS.";
                if (countPFirmes > 100)
                {
                    lblmesage1.IsVisible = true;
                    lblmesage2.IsVisible = true;
                }
                lblMGraves.Text = countMGraves.ToString();
                lblGraves.Text = countGraves.ToString();
                lblPFirmes.Text = countPFirmes.ToString();
                lblTotalPapeletas.Text = "INFRACCIONES ACUMULADAS: " + countTPapeletas.ToString();

                papeletas.IsVisible = true;
                listaPapeletas.IsVisible = false;
                stackTitlePapeletas.IsVisible = false;
                cursos.IsVisible = true;
                listaCursos.IsVisible = false;
                listaBonificaciones.IsVisible = false;
                bonificaciones.IsVisible = true;
                framedatos.IsVisible = true;
                administrado_wrapper.Opacity = 0;
                listaDetalle.IsVisible = false;
                frpapeletas.IsVisible = true;
                frcursos.IsVisible = true;
                frbonificaciones.IsVisible = true;
                await administrado_wrapper.FadeTo(1, 150);

                Cargado = true;
            });
        }

        private async void MostrarPapeleta(object sender, ItemTappedEventArgs e)
        {
            if (listaPapeletas.SelectedItem != null)
            {
                MTCConducirApp.CurrentPapeleta = (MTC.ServiciosPuntos.Dominio.Entidades.LC_TM_Papeletas)e.Item;
                //MasterDetail.DetalleActual.CargarPagina(typeof(ConsultaPuntosPapeleta));
                await NavigationService.PushAsync(new ConsultaPuntosPapeleta());

                ((ListView)sender).SelectedItem = null; // de-select the row 
            }
        }

        private async void MostrarBonificacion(object sender, ItemTappedEventArgs e)
        {
            if (listaBonificaciones.SelectedItem != null)
            {
                var test = (MTC.ServiciosPuntos.Dominio.Entidades.LC_TM_Bonificacion)e.Item;
                MTCConducirApp.CurrentBonificacion = (MTC.ServiciosPuntos.Dominio.Entidades.LC_TM_Bonificacion)e.Item;
                // MasterDetail.DetalleActual.CargarPagina(typeof(ConsultaPuntosBonificacion));
                await NavigationService.PushAsync(new ConsultaPuntosBonificacion());
                ((ListView)sender).SelectedItem = null; // de-select the row 
            }
        }
        private async void MostrarCursos(object sender, ItemTappedEventArgs e)
        {
            if (listaCursos.SelectedItem != null)
            {
                MTCConducirApp.CurrentCurso = (MTC.ServiciosPuntos.Dominio.Entidades.LC_TM_CursoJornada)e.Item;
                //  MasterDetail.DetalleActual.CargarPagina(typeof(ConsultaPuntosCurso));
                await NavigationService.PushAsync(new ConsultaPuntosCurso());
                ((ListView)sender).SelectedItem = null; // de-select the row 
            }
        }
        private async void MostrarAyudaPapeletas(object sender, EventArgs e)
        {
            // MasterDetail.DetalleActual.CargarPagina(typeof(FaltaAyuda));
            //  await NavigationService.PushAsync(new FaltaAyuda());
        }
    }
}