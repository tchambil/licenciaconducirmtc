﻿using LicenciaConducir.Vistas.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LicenciaConducir.Vistas.Paginas.Puntos
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FaltaAyuda : BasePage
    {
        protected async override void OnAppearing()
        {
            base.OnAppearing();
            var lista = new List<ConductorDetalleItem>();
            await Task.Delay(200);
            await Task.Run(async () =>
            {
                lista.Add(new ConductorDetalleItem() { Campo = "G01", Valor = "Adelantar o sobrepasar en forma indebida a otro vehículo." });
                lista.Add(new ConductorDetalleItem() { Campo = "G02", Valor = "No hacer señales, ni tomar las precauciones para girar, voltear en U, pasar de un carril de la calzada a otro o detener el vehículo." });
                lista.Add(new ConductorDetalleItem() { Campo = "G03", Valor = "Detener el vehículo bruscamente sin motivo." });
                lista.Add(new ConductorDetalleItem() { Campo = "G04", Valor = "No detenerse antes de la línea de parada o antes de las áreas de intersección de calzadas o no respetar el derecho de paso del peatón." });
                lista.Add(new ConductorDetalleItem() { Campo = "G05", Valor = "No mantener una distancia suficiente, razonable y prudente, de acuerdo al tipo de vehículo y la vía por la que se conduce, mientras se desplaza o al detenerse detrás de otro." });
                lista.Add(new ConductorDetalleItem() { Campo = "G06", Valor = "No ubicar el vehículo con la debida anticipación en el carril donde va efectuar el giro o volteo." });
                lista.Add(new ConductorDetalleItem() { Campo = "G07", Valor = "No conducir por el carril de extremo derecho de la calzada un vehículo del servicio de transporte público de pasajeros o de carga o de desplazamiento lento o un vehículo automotor menor." });
                lista.Add(new ConductorDetalleItem() { Campo = "G08", Valor = "No utilizar el carril derecho para recoger o dejar pasajeros o carga." });
                lista.Add(new ConductorDetalleItem() { Campo = "G09", Valor = "Retroceder, salvo casos indispensables para mantener libre la circulación, para incorporarse a ella o para estacionar el vehículo." });
                lista.Add(new ConductorDetalleItem() { Campo = "G10", Valor = "Incumplir las disposiciones sobre el uso de las vías de tránsito rápido y/o de acceso restringido." });
                lista.Add(new ConductorDetalleItem() { Campo = "G11", FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(Label)), Valor = "Circular, estacionar o detenerse sobre una isla de encauzamiento, canalizadora, de refugio o divisoria del tránsito, marcas delimitadoras de carriles, separadores centrales, bermas, aceras, áreas verdes, pasos peatonales, jardines o rampas para minusválidos." });
                lista.Add(new ConductorDetalleItem() { Campo = "G12", Valor = "Girar estando el semáforo con luz roja y flecha verde, sin respetar el derecho preferente de paso de los peatones." });
                lista.Add(new ConductorDetalleItem() { Campo = "G13", FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(Label)), Valor = "Conducir un vehículo con mayor número de personas al número de asientos señalado en la Tarjeta de Identificación Vehicular, con excepción de niños en brazos en los asientos posteriores; y, llevar pasajeros de pie en vehículos del servicio público de transporte urbano de pasajeros si la altura interior del vehículo es menor a 1.80 metros." });
                lista.Add(new ConductorDetalleItem() { Campo = "G14", Valor = "Tener la puerta, capot o maletera del vehículo abierta, cuando el vehículo está en marcha." });
                lista.Add(new ConductorDetalleItem() { Campo = "G15", Valor = "No utilizar las luces intermitentes de emergencia de un vehículo cuando se detiene por razones de fuerza mayor, obstaculizando el tránsito, o no colocar los dispositivos de seguridad reglamentarios cuando el vehículo queda inmovilizado en la vía pública." });
                lista.Add(new ConductorDetalleItem() { Campo = "G16", Valor = "Conducir un vehículo por una vía en la cual no está permitida la circulación o sobre mangueras contra incendio." });
                lista.Add(new ConductorDetalleItem() { Campo = "G17", Valor = "Conducir vehículos que tengan lunas o vidrios polarizados o acondicionados de modo tal que impidan la visibilidad del interior del vehículo, sin la autorización correspondiente." });
                lista.Add(new ConductorDetalleItem() { Campo = "G18", FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(Label)), Valor = "a) Conducir un vehículo sin que ambas manos estén sobre el volante de dirección, excepto cuando es necesario realizar los cambios de velocidad o accionar otros comandos. b) Conducir un vehículo usando algún dispositivo móvil u objeto portátil que implique dejar de conducir con ambas manos sobre el volante de dirección." });
                lista.Add(new ConductorDetalleItem() { Campo = "G19", FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(Label)), Valor = "Conducir un vehículo de la categoría M o N que carezca de vidrios de seguridad reglamentarios o que su parabrisas se encuentre deteriorado, trizado o con objetos impresos, calcomanías, carteles u otros elementos en el área de barrido del limpiaparabrisas y que impidan la visibilidad del conductor o un vehículo de la categoría L5 que contando con parabrisas, micas o similares, tengan objetos impresos, calcomanías, carteles u otros elementos que impidan la visibilidad del conductor." });
                lista.Add(new ConductorDetalleItem() { Campo = "G20", Valor = "Conducir un vehículo que no cuenta con las luces y dispositivos retrorreflectivos previstos en los reglamentos pertinentes." });
                lista.Add(new ConductorDetalleItem() { Campo = "G21", Valor = "Conducir un vehículo sin espejos retrovisores." });
                lista.Add(new ConductorDetalleItem() { Campo = "G22", Valor = "Conducir un vehículo cuando llueve, llovizne o garúe, sin tener operativo el sistema de limpiaparabrisas." });
                lista.Add(new ConductorDetalleItem() { Campo = "G23", Valor = "Conducir un vehículo del servicio de transporte público urbano de pasajeros con personas de pie, si la altura interior del vehículo no supera a 1,80 metros." });
                lista.Add(new ConductorDetalleItem() { Campo = "G24", Valor = "Conducir un vehículo con el motor en punto neutro o apagado." });
                lista.Add(new ConductorDetalleItem() { Campo = "G25", Valor = "Conducir un vehículo sin portar el certificado del Seguro Obligatorio de Accidentes de Tránsito o Certificado Contra Accidentes de Tránsito, o que éstos no correspondan al uso del vehículo." });
                lista.Add(new ConductorDetalleItem() { Campo = "G26", Valor = "Conducir un vehículo de la categoría M o N con la salida del tubo de escape en la parte lateral derecha, de modo tal que las emisiones o gases sean expulsados hacia la acera por donde circulan los peatones." });
                lista.Add(new ConductorDetalleItem() { Campo = "G27", Valor = "Conducir un vehículo cuya carga o pasajeros obstruya la visibilidad de los espejos laterales." });
                lista.Add(new ConductorDetalleItem() { Campo = "G28", FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(Label)), Valor = "En vehículos de las categorías M y N, no llevar puesto el cinturon de seguridad y/o permitir que los ocupantes del vehículo no lo utilicen en los casos en que, de acuerdo a las normas vigentes, exista tal obligación. En vehículos automotores de la categoria L5 no contar con cinturones de seguridad para los asientos de los pasajeros o no tener uno o más soportes fijados a su estructura que permitan a los pasajeros asirse de ellos mientras son transportados." });
                lista.Add(new ConductorDetalleItem() { Campo = "G29", Valor = "Circular en forma desordenada o haciendo maniobras peligrosas." });
                lista.Add(new ConductorDetalleItem() { Campo = "G30", Valor = "Circular transportando personas en la parte exterior de la carroceria o permitir que sobresalga parte del cuerpo de la(s) persona(s) transportada(s) en el vehículo." });
                lista.Add(new ConductorDetalleItem() { Campo = "G31", FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(Label)), Valor = "En las vías públicas urbanas, circular en la noche o cuando la luz natural sea insuficiente o cuando las condiciones de visibilidad sean escasas, sin tener encendido el sistema de luces reglamentarias; o en la red vial nacional y departamental o regional, circular sin tener las luces bajas encendidas durante las veinticuatro (24) horas." });
                lista.Add(new ConductorDetalleItem() { Campo = "G32", Valor = "Circular por vías o pistas exclusivas para bicicletas." });
                lista.Add(new ConductorDetalleItem() { Campo = "G33", Valor = "Circular transportando cargas que sobrepasen las dimensiones de la carrocería o que se encuentren ubicadas fuera de la misma; o transportar materiales sueltos, fluidos u otros sin adoptar las medidas de seguridad que impidan su caída a la vía." });
                lista.Add(new ConductorDetalleItem() { Campo = "G34", Valor = "Remolcar vehículos sin las medidas de seguridad." });
                lista.Add(new ConductorDetalleItem() { Campo = "G35", Valor = "Usar luces altas en vías urbanas o hacer mal uso de las luces." });
                lista.Add(new ConductorDetalleItem() { Campo = "G36", Valor = "Compartir el asiento de conducir con otra persona, animal o cosa." });
                lista.Add(new ConductorDetalleItem() { Campo = "G37", FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(Label)), Valor = "No reducir la velocidad al ingresar a un túnel o cruzar un puente, intersecciones o calles congestionadas, cuando transite por cuestas, cuando se aproxime y tome una curva o cambie de dirección, cuando circule por una vía estrecha o sinuosa, cuando se encuentre con un vehículo que circula en sentido contrario o cuando existan peligros especiales con respecto a los peatones u otros vehículos o por razones del clima o condiciones especiales de la vía." });
                lista.Add(new ConductorDetalleItem() { Campo = "G38", Valor = "Transitar lentamente por el carril de la izquierda, causando congestión o riesgo o rápidamente por el carril de la derecha." });
                lista.Add(new ConductorDetalleItem() { Campo = "G39", Valor = "Aumentar la velocidad cuando es alcanzado por otro vehículo que tiene la intención de sobrepasarlo o adelantarlo." });
                lista.Add(new ConductorDetalleItem() { Campo = "G40", Valor = "Estacionar el vehículo en zonas prohibidas o rigidas señalizadas o sin las señales de seguridad reglamentarias en caso de emergencia." });
                lista.Add(new ConductorDetalleItem() { Campo = "G41", Valor = "Estacionar o detener el vehículo sobre la línea demarcatoria de intersección, dentro de éstas o en el crucero peatonal (paso peatonal)." });
                lista.Add(new ConductorDetalleItem() { Campo = "G42", Valor = "Estacionar frente a la entrada o salida de garajes, estacionamientos públicos, vías privadas o en las salidas de salas, espectáculos, centros deportivos en funcionamiento." });
                lista.Add(new ConductorDetalleItem() { Campo = "G43", Valor = "Estacionar a una distancia menor de cinco (5) metros de una bocacalle, de las entradas de hospitales o centros de asistencia médica, cuerpos de bomberos o de hidrantes de servicio contra incendios, salvo los vehículos relacionados a la función del local." });
                lista.Add(new ConductorDetalleItem() { Campo = "G44", Valor = "Estacionar a menos de tres (3) metros de las puertas de establecimientos educacionales, teatros, iglesias y hoteles, salvo los vehículos relacionados a la función del local." });
                lista.Add(new ConductorDetalleItem() { Campo = "G45", Valor = "Estacionar a menos de veinte (20) metros de un cruce ferroviario a nivel." });
                lista.Add(new ConductorDetalleItem() { Campo = "G46", Valor = "Estacionar en zonas no permitidas por la autoridad competente, a menos de diez (10) metros de un cruce peatonal o de un paradero de buses, así como en el propio sitio determinado para la parada del bus." });
                lista.Add(new ConductorDetalleItem() { Campo = "G47", Valor = "Estacionar en lugar que afecte la operatividad del servicio de transporte público de pasajeros o carga o que afecte la seguridad, visibilidad o fluidez del tránsito o impida observar la señalización." });
                lista.Add(new ConductorDetalleItem() { Campo = "G48", Valor = "Estacionar un ómnibus, microbus, casa rodante, camión, remolque, semirremolque, plataforma, tanque, tractocamión, tráiler, volquete o furgón, en en vías públicas de zona urbana, excepto en los lugares que habilite para tal fin la autoridad competente, mediante la señalización pertinente." });
                lista.Add(new ConductorDetalleItem() { Campo = "G49", Valor = "Estacionar un vehículo de categorìa M, N u O a una distancia menor a un metro de la parte delantera o posterior de otro ya estacionado, salvo cuando se estacione en diagonal o perpendicular a la vía." });
                lista.Add(new ConductorDetalleItem() { Campo = "G50", Valor = "Estacionar en los terminales o estaciones de ruta, fuera de los estacionamientos externos determinados por la Autoridad competente." });
                lista.Add(new ConductorDetalleItem() { Campo = "G51", Valor = "Estacionar un vehículo automotor por la noche en lugares donde, por la falta de alumbrado público, se impide su visibilidad, o en el día, cuando, por lluvia, llovizna o neblina u otro factor, la visibilidad es escasa, sin mantener encendidas las luces de estacionamiento." });
                lista.Add(new ConductorDetalleItem() { Campo = "G52", Valor = "Estacionar un vehículo en vías con pendientes pronunciadas sin asegurar su inmovilización." });
                lista.Add(new ConductorDetalleItem() { Campo = "G53", Valor = "Desplazar o empujar un vehículo bien estacionado, con el propósito de ampliar un espacio o tratar de estacionar otro vehículo." });
                lista.Add(new ConductorDetalleItem() { Campo = "G54", Valor = "Abandonar el vehículo en la vía pública." });
                lista.Add(new ConductorDetalleItem() { Campo = "G55", Valor = "Utilizar la vía pública para efectuar reparaciones, salvo casos de emergencia." });
                lista.Add(new ConductorDetalleItem() { Campo = "G56", Valor = "Recoger o dejar pasajeros fuera de los paraderos de ruta autorizados, cuando existan." });
                lista.Add(new ConductorDetalleItem() { Campo = "G57", Valor = "No respetar las señales que rigen el tránsito, cuyo incumplimiento no se encuentre tipificado en otra infracción." });
                lista.Add(new ConductorDetalleItem() { Campo = "G58", Valor = "No presentar la Tarjeta de Identificación Vehicular, la Licencia de Conducir o el Documento Nacional de Identidad o documento de identidad, según corresponda." });
                lista.Add(new ConductorDetalleItem() { Campo = "G59", Valor = "Conducir un vehículo de categoría L, con excepción de la categoría L5, sin tener puesto el casco de seguridad o anteojos protectores, en caso de no tener parabrisas; o permitir que los demás ocupantes no tengan puesto el casco de seguridad." });
                lista.Add(new ConductorDetalleItem() { Campo = "G60", Valor = "Circular con placas ilegibles o sin iluminación o que tengan adherido algún material, que impida su lectura a través de medios electrónicos, computarizados u otro tipo de mecanismos tecnológicos que permitan verificar la comisión de las infracciones de tránsito." });
                lista.Add(new ConductorDetalleItem() { Campo = "G61", Valor = "No llevar las placas de rodaje en el lugar que corresponde." });
                lista.Add(new ConductorDetalleItem() { Campo = "G62", Valor = "Incumplir con devolver las placas de exhibición, rotativa o transitoria dentro de los plazos establecidos en el Reglamento de Placa Única Nacional de Rodaje." });
                lista.Add(new ConductorDetalleItem() { Campo = "G63", Valor = "Utilizar señales audibles o visibles iguales o similares a las que utilizan los vehículos de emergencia o vehículos oficiales." });
                lista.Add(new ConductorDetalleItem() { Campo = "G64", FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(Label)), Valor = "Conducir un vehículo cuyas características registrables o condiciones técnicas han sido modificadas, alteradas o agregadas, atentando contra la seguridad de los usuarios o por no corresponder los datos consignados en la Tarjeta de Identificación Vehicular con los del vehículo." });
                lista.Add(new ConductorDetalleItem() { Campo = "G65", Valor = "No ceder el paso a otros vehículos que tienen preferencia." });
                lista.Add(new ConductorDetalleItem() { Campo = "G66", Valor = "Seguir a los vehículos de emergencia y vehículos oficiales para avanzar más rápidamente." });
                lista.Add(new ConductorDetalleItem() { Campo = "G70", Valor = "Detener el vehiculo sobre la demarcación en el pavimento de la señal \"No bloquear cruce\"" });
                lista.Add(new ConductorDetalleItem() { Campo = "G71", FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(Label)), Valor = "Circular por las vias públicas terrestres donde se encuentran instaladas garitas o puntos de peaje, sin pagar la tarifa de peaje aprobada por la autoridad competente o el establecido en los contratos de concesión respectivos" });
                lista.Add(new ConductorDetalleItem() { Campo = "G72", FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(Label)), Valor = "a) Utilizar, mientras se conduce el vehículo, cualquier dispositivo electrónico que reproduzca imágenes o videos con fines de entretenimiento visual. b) Utilizar un vehículo que tenga instalados los dispositivos electrónicos permitidos obstaculizando la visibilidad del conductor mientras conduce o las señales emitidas por el tablero de control del vehículo." });
                lista.Add(new ConductorDetalleItem() { Campo = "L01", Valor = "Dejar mal estacionado el vehículo en lugares permitidos." });
                lista.Add(new ConductorDetalleItem() { Campo = "L02", Valor = "Estacionar un vehículo en zonas de parqueo destinadas a vehículos que transportan a personas con discapacidad o conducidos por éstos." });
                lista.Add(new ConductorDetalleItem() { Campo = "L03", Valor = "Utilizar el Permiso Especial de Parqueo para Personas con Discapacidad por parte de una persona a la cual no le corresponde." });
                lista.Add(new ConductorDetalleItem() { Campo = "L04", Valor = "Abrir o dejar abierta la puerta de un vehículo estacionado, dificultando la circulación vehicular." });
                lista.Add(new ConductorDetalleItem() { Campo = "L05", Valor = "Utilizar el carril de giro a la izquierda para continuar la marcha en cualquier dirección que no sea la específicamente señalada." });
                lista.Add(new ConductorDetalleItem() { Campo = "L06", Valor = "Arrojar, depositar o abandonar objetos o sustancias en la vía pública que dificulten la circulación." });
                lista.Add(new ConductorDetalleItem() { Campo = "L07", Valor = "Utilizar la bocina para llamar la atención en forma inncesaria." });
                lista.Add(new ConductorDetalleItem() { Campo = "L08", Valor = "Hacer uso de bocinas de descarga de aire comprimido en el ámbito urbano." });
                lista.Add(new ConductorDetalleItem() { Campo = "M01", FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(Label)), Valor = "Conducir con presencia de alcohol en la sangre en proporción mayor a lo previsto en el Código Penal, o bajo los efectos de estupefacientes, narcóticos y/o alucinógenos comprobado con el exámen respectivo o por negarse al mismo y que haya participado en un accidente de tránsito." });
                lista.Add(new ConductorDetalleItem() { Campo = "M02", FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(Label)), Valor = "Conducir con presencia de alcohol en la sangre en proporción mayor a lo previsto en el Código Penal, bajo los efectos de estupefacientes, narcóticos y/o alucinógenos comprobada con el exámen respectivo o por negarse al mismo." });
                lista.Add(new ConductorDetalleItem() { Campo = "M03", Valor = "Conducir un vehículo automotor sin tener licencia de conducir o permiso provisional." });
                lista.Add(new ConductorDetalleItem() { Campo = "M04", Valor = "Conducir vehículos estando la licencia de conducir retenida, suspendida o estando inhabilitado para obtener licencia de conducir." });
                lista.Add(new ConductorDetalleItem() { Campo = "M05", Valor = "Conducir un vehículo con Licencia de Conducir cuya clase o categoría no corresponde al vehículo que conduce." });
                lista.Add(new ConductorDetalleItem() { Campo = "M06", Valor = "Estacionar en las curvas, puentes, túneles, zonas estrechas de la vía, pasos a nivel, pasos a desnivel en cambios de rasante, pendientes y cruces de ferrocarril." });
                lista.Add(new ConductorDetalleItem() { Campo = "M07", Valor = "Participar en competencias de velocidad en eventos no autorizados." });
                lista.Add(new ConductorDetalleItem() { Campo = "M08", Valor = "Permitir a un menor de edad la conducción de un vehículo automotor, sin autorización o permiso provisional." });
                lista.Add(new ConductorDetalleItem() { Campo = "M09", Valor = "Conducir un vehículo con cualquiera de sus sistemas de dirección, frenos, suspensión, luces o eléctrico en mal estado, previa inspección técnica vehicular." });
                lista.Add(new ConductorDetalleItem() { Campo = "M10", Valor = "Abastecer de combustible un vehículo del servicio de transporte público de pasajeros con personas a bordo del vehículo." });
                lista.Add(new ConductorDetalleItem() { Campo = "M11", Valor = "Conducir vehículos de las categorías M o N sin parachoques o dispositivo antiempotramiento cuando corresponda; o un vehículo de la categoría L5 sin parachoques posterior, conforme a lo establecido en el Reglamento Nacional de Vehículos." });
                lista.Add(new ConductorDetalleItem() { Campo = "M12", Valor = "No detenerse al aproximarse a un vehículo de transporte escolar debidamente identificado que está recogiendo o dejando escolares." });
                lista.Add(new ConductorDetalleItem() { Campo = "M13", Valor = "Conducir un vehículo con neumático(s), cuya banda de rodadura presente desgaste inferior al establecido en el Reglamento Nacional de Vehículos." });
                lista.Add(new ConductorDetalleItem() { Campo = "M14", Valor = "No detenerse al llegar a un cruce ferroviario a nivel o reiniciar la marcha sin haber comprobado que no se aproxima tren o vehículo ferroviario, o cruzar la vía ferrea por lugares distintos a los cruces a nivel establecidos." });
                lista.Add(new ConductorDetalleItem() { Campo = "M15", Valor = "Circular produciendo contaminación en un índice superior a los límites máximos permisibles de emisión de gases contaminantes." });
                lista.Add(new ConductorDetalleItem() { Campo = "M16", Valor = "Circular en sentido contrario al tránsito autorizado." });
                lista.Add(new ConductorDetalleItem() { Campo = "M17", Valor = "Cruzar una intersección o girar, estando el semáforo con luz roja y no existiendo la indicación en contrario." });
                lista.Add(new ConductorDetalleItem() { Campo = "M18", Valor = "Desobedecer las indicaciones sobre el tránsito que ordene el efectivo de la Policía Nacional del Perú asignado al control del tránsito." });
                lista.Add(new ConductorDetalleItem() { Campo = "M19", Valor = "Conducir vehículos sin cumplir con las restricciones que consigna la Licencia de Conducir." });
                lista.Add(new ConductorDetalleItem() { Campo = "M20", Valor = "No respetar los límites máximo o mínimo de velocidad establecidos." });
                lista.Add(new ConductorDetalleItem() { Campo = "M21", Valor = "Estacionar interrumpiendo totalmente el tránsito." });
                lista.Add(new ConductorDetalleItem() { Campo = "M22", Valor = "Detenerse para cargar o descargar mercancías en la calzada y/o en los lugares que puedan constituir un peligro u obstáculo o interrumpan la circulación." });
                lista.Add(new ConductorDetalleItem() { Campo = "M23", Valor = "Estacionar o detener el vehículo en el carril de circulación, en carreteras o caminos donde existe berma lateral." });
                lista.Add(new ConductorDetalleItem() { Campo = "M24", Valor = "Circular sin placas de rodaje o sin el permiso correspondiente." });
                lista.Add(new ConductorDetalleItem() { Campo = "M25", Valor = "No dar preferencia de paso a los vehíuclos de emergencia y vehículos oficiales cuando hagan uso de sus señales audibles y visibles." });
                lista.Add(new ConductorDetalleItem() { Campo = "M26", Valor = "Conducir un vehículo especial que no se ajuste a las exigencias reglamentarias sin la autorización correspondiente." });
                lista.Add(new ConductorDetalleItem() { Campo = "M27", Valor = "Conducir un vehículo que no cuente con el certificado de aprobación de inspección técnica vehicular. Esta infracción no aplica para el caso de los vehiculos L5 de la clasificación vehicular" });
                lista.Add(new ConductorDetalleItem() { Campo = "M28", Valor = "Conducir un vehículo sin contar con la póliza del Seguro Obligatorio de Accidentes de Tránsito, o Certificado de Accidentes de Tránsito, cuando corresponda, o éstos no se encuentre vigente." });
                lista.Add(new ConductorDetalleItem() { Campo = "M29", Valor = "Deteriorar intencionalmente, adulterar, destruir o sustraer las Placas de exhibición, rotativa o transitoria ." });
                lista.Add(new ConductorDetalleItem() { Campo = "M30", Valor = "Usar las placas de exhibición, rotativa o transitoria fuera del plazo, horario o ruta establecida o cuando esta ha caducado o ha sido invalidada." });
                lista.Add(new ConductorDetalleItem() { Campo = "M31", Valor = "Utilizar las placas de exhibición, rotativa o transitoria en vehículos a los que no se encuentren asignadas." });
                lista.Add(new ConductorDetalleItem() { Campo = "M32", FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(Label)), Valor = "Tramitar u obtener duplicado, recategorización, revalidación, canje o nueva licencia de conducir de cualquier clase, por el infractor cuya licencia de conducir se encuentre retenida, suspendida o cancelada o se encuentre inhabilitado para obtenerla." });
                lista.Add(new ConductorDetalleItem() { Campo = "M33", Valor = "Operar maquinaria especial por la vía pública." });
                lista.Add(new ConductorDetalleItem() { Campo = "M34", Valor = "Circular produciendo ruidos que superen los límites máximos permisibles." });
                lista.Add(new ConductorDetalleItem() { Campo = "M35", Valor = "Voltear en U sobre la misma calzada, en las curvas, puentes, pasos a desnivel, vías expresas, túneles, estructuras elevadas, cima de cuesta, cruce ferroviario a nivel." });
                lista.Add(new ConductorDetalleItem() { Campo = "M36", Valor = "Transportar carga sin los dispositivos de sujeción o seguridad establecidos." });
                lista.Add(new ConductorDetalleItem() { Campo = "M37", Valor = "Conducir y ocasionar un accidente de tránsito con daños personales inobservando las normas de tránsito dispuestas en el presente Reglamento." });
                lista.Add(new ConductorDetalleItem() { Campo = "M38", Valor = "Conducir un vehículo para el servicio de transporte público y ocasionar un accidente de tránsito con daños personales inobservando las normas de tránsito dispuestas por el presente Reglamento." });
                lista.Add(new ConductorDetalleItem() { Campo = "M39", Valor = "Conducir y ocasionar un accidente de tránsito con lesiones graves o muerte inobservando las normas de tránsito dispuestas en el presente Reglamento." });
                lista.Add(new ConductorDetalleItem() { Campo = "M40", Valor = "Conducir un vehiculo con la licencia de conducir vencida" });
                lista.Add(new ConductorDetalleItem() { Campo = "M41", Valor = "Circular, interrumpir y/o impedir el tránsito, en situaciones de desastre natural o emergencia, incumpliendo las disposiciones de la autoridad competente para la restricción de acceso a las vías" });
                lista.Add(new ConductorDetalleItem() { Campo = "M42", Valor = "Conducir un vehiculo de la categoria L5 de la clasificación vehicular, que no cuente con el certificado de aprobacíón de inspección técnica vehicular" });

            });


            listaFaltas.ItemsSource = lista;

            //listaFaltas.ItemTapped += PrevenirSeleccion;

            // await Task.Delay(1100);
            IndicadorDeCarga.IsVisible = false;
            //labelFaltas.Opacity = 0;
            //listaFaltas.Opacity = 0;
            labelFaltas.IsVisible = true;
            listaFaltas.IsVisible = true;
            //await labelFaltas.FadeTo(1, 150);
            //await listaFaltas.FadeTo(1, 150);
        }
        public FaltaAyuda()
        {
            InitializeComponent();
            NavigationPage.SetBackButtonTitle(this, "");


        }
        void PrevenirSeleccion(object sender, ItemTappedEventArgs e)
        {
            if (e == null) return; // has been set to null, do not 'process' tapped event 
            ((ListView)sender).SelectedItem = null; // de-select the row
        }
    }
}