﻿using LicenciaConducir.Vistas.Inc;
using LicenciaConducir.Vistas.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LicenciaConducir.Vistas.Paginas.Pregunta
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PreguntasFrecuentes : BasePage
    {
        public PreguntasFrecuentes()
        {
            InitializeComponent();

        }
        protected override bool OnBackButtonPressed()
        {
            base.OnBackButtonPressed();
            MasterDetailLicencia.DetalleActual.CargarPagina(typeof(Principal), false);
            return true;
        }
        public void OnContViewQuestion1(object sender, EventArgs e)
        {
            ContViewQuestion1.IsVisible = !ContViewQuestion1.IsVisible;
            if (ContViewQuestion1.IsVisible)
            {
                img1.Source = "iconCollapseOut.png";
            }
            else
            {
                img1.Source = "iconCollapse.png";
            }

        }
        public void OnContViewQuestion2(object sender, EventArgs e)
        {
            ContViewQuestion2.IsVisible = !ContViewQuestion2.IsVisible;
            if (ContViewQuestion2.IsVisible)
            {
                img2.Source = "iconCollapseOut.png";
            }
            else
            {
                img2.Source = "iconCollapse.png";
            }
        }
        public void OnContViewQuestion3(object sender, EventArgs e)
        {
            ContViewQuestion3.IsVisible = !ContViewQuestion3.IsVisible;
            if (ContViewQuestion3.IsVisible)
            {
                img3.Source = "iconCollapseOut.png";
            }
            else
            {
                img3.Source = "iconCollapse.png";
            }
        }
        public void OnContViewQuestion4(object sender, EventArgs e)
        {
            ContViewQuestion4.IsVisible = !ContViewQuestion4.IsVisible;
            if (ContViewQuestion4.IsVisible)
            {
                img4.Source = "iconCollapseOut.png";
            }
            else
            {
                img4.Source = "iconCollapse.png";
            }
        }
        public void OnContViewQuestion5(object sender, EventArgs e)
        {
            ContViewQuestion5.IsVisible = !ContViewQuestion5.IsVisible;
            if (ContViewQuestion5.IsVisible)
            {
                img5.Source = "iconCollapseOut.png";
            }
            else
            {
                img5.Source = "iconCollapse.png";
            }
        }
        public void OnContViewQuestion6(object sender, EventArgs e)
        {
            ContViewQuestion6.IsVisible = !ContViewQuestion6.IsVisible;
            if (ContViewQuestion6.IsVisible)
            {
                img6.Source = "iconCollapseOut.png";
            }
            else
            {
                img6.Source = "iconCollapse.png";
            }
        }
        public void OnContViewQuestion7(object sender, EventArgs e)
        {
            ContViewQuestion7.IsVisible = !ContViewQuestion7.IsVisible;
            if (ContViewQuestion7.IsVisible)
            {
                img7.Source = "iconCollapseOut.png";
            }
            else
            {
                img7.Source = "iconCollapse.png";
            }
        }
        public void OnContViewQuestion8(object sender, EventArgs e)
        {
            ContViewQuestion8.IsVisible = !ContViewQuestion8.IsVisible;
            if (ContViewQuestion8.IsVisible)
            {
                img8.Source = "iconCollapseOut.png";
            }
            else
            {
                img8.Source = "iconCollapse.png";
            }
        }
    }
}