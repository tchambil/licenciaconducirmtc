﻿using LicenciaConducir.Lib.MTCConducir;
using LicenciaConducir.Lib.MTCUsuario;
using LicenciaConducir.Services;
using LicenciaConducir.Vistas.Inc;
using LicenciaConducir.Vistas.Lib;
using LicenciaConducir.Vistas.Paginas.EComplementarias;
using LicenciaConducir.Vistas.Paginas.Pregunta;
using LicenciaConducir.Vistas.Paginas.Puntos;
using LicenciaConducir.Vistas.Paginas.Record;
using LicenciaConducir.Vistas.Paginas.RSociales;
using LicenciaConducir.Vistas.Paginas.Usuario;
using Plugin.XamarinFormsSaveOpenPDFPackage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LicenciaConducir.Vistas.Paginas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Principal : BasePage
    {
        FeatureService FeatureService;
        public Principal()
        {
            InitializeComponent();
            FeatureService = DependencyService.Get<FeatureService>();
        }
        protected override async void OnAppearing()
        {
            base.OnAppearing();
            if (MTCPersonaApp.IsElectronica)
            {
                ImgElectronica.IsVisible = true;
                lblElectronica.IsVisible = true;
            }
#if __IOS__
            stackWelcome.IsVisible = false;
#endif

        }
        public void OnRSociales(object sender, EventArgs e)
        {
            MasterDetailLicencia.DetalleActual.CargarPagina(typeof(RedesSociales));
        }
        public async void OnCEntidades(object sender, EventArgs e)
        {
            if (MTCPersonaApp.IsElectronica)
            {
                var db = TodoItemDatabase.getDatabase();
                var Persona = await db.GetCiudadano(new ViewModels.Ciudadano()
                {
                    NumeroDocumento = MTCPersonaApp.NroDocumento,
                    TipoDocumento = MTCConducirApp.Tipo.ToString(),
                });
                using (var memoryStream = new MemoryStream(Persona.File))
                {
                    await CrossXamarinFormsSaveOpenPDFPackage.Current.SaveAndView(Guid.NewGuid() + ".pdf", "application/pdf", memoryStream, PDFOpenContext.ChooseApp);
                }
            }
                
        }
        public void OnPFrecuentes(object sender, EventArgs e)
        {
            MasterDetailLicencia.DetalleActual.CargarPagina(typeof(PreguntasFrecuentes));
            
        }
        public async void OnPuntosLicencia(object sender, EventArgs e)
        {
            var current = Connectivity.NetworkAccess;
            if (current == NetworkAccess.Internet)
            {
                MasterDetailLicencia.DetalleActual.CargarPagina(typeof(SearchPuntos));
            }
            else
            {
                await DisplayAlert("Aviso", "Disponible para modo conectado, su dispositivo debe estar conectado a internet.", "Aceptar");
                return;
            } 
        }
        public async void OnRecordConductor(object sender, EventArgs e)
        {
            var current = Connectivity.NetworkAccess;
            if (current == NetworkAccess.Internet)
            {
                MasterDetailLicencia.DetalleActual.CargarPagina(typeof(SearchRecord));
            }
            else
            {
                await DisplayAlert("Aviso", "Disponible para modo conectado, su dispositivo debe estar conectado a internet.", "Aceptar");
                return;
            }
           

        }
        public async void OnDatosUsuario(object sender, EventArgs e)
        {
            
            if (MTCPersonaApp.SinLicencia)
            {
                await DisplayAlert("Aviso", "Ud. no cuenta con licencia de conducir.", "Aceptar");
                return;
            }
            MTCConducirApp.Tipo = App.Current.Properties.ContainsKey("TDocucumento") ? (int)App.Current.Properties["TDocucumento"] : 0;
            MTCConducirApp.TipoBusquedaConsultaPuntos = MTCConducirApp.EnumTipoBusquedaConsulta.PorNroDocumento;
            MTCConducirApp.NroDocumento = App.Current.Properties.ContainsKey("NDocumento") ? App.Current.Properties["NDocumento"].ToString() : "";
            MasterDetailLicencia.DetalleActual.CargarPagina(typeof(DatosPersona));
        }


        protected override bool OnBackButtonPressed()
        {
            base.OnBackButtonPressed();
            Task<bool> action = DisplayAlert("Aviso", "¿Desea salir de la aplicación?", "Si", "No");
            action.ContinueWith(task =>
            {
                if (task.Result)
                {
#if __ANDROID__
                        var activity = (Android.App.Activity)Forms.Context;
                        activity.FinishAffinity();
#elif __IOS__
                    throw new Exception();
#endif
                }
            });
            return true;
        }
        public void SalirDeLaAplicacion(object obj)
        {
#if __ANDROID__
            var activity = (Android.App.Activity)Forms.Context;
            activity.FinishAffinity();
#elif __IOS__
            throw new Exception();
#endif
        }
    }
}