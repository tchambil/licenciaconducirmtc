﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LicenciaConducir.Services
{
    public interface FeatureService
    {
       
        bool StartMap(string Latitud, string longitud, string name);     
        void DownloadFile(string url, string folder);
        void OpenFile(string filePath);
        string CreateFile(string filename, byte[] bytes);
        string GetFilePath(string filename);
    }
}
