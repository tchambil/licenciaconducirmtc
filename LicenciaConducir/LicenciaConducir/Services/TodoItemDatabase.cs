﻿using LicenciaConducir.ViewModels;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace LicenciaConducir.Services
{
    public class TodoItemDatabase
    {
        readonly SQLiteAsyncConnection database;
        public TodoItemDatabase(string dbPath)
        {
            database = new SQLiteAsyncConnection(dbPath);
            database.CreateTableAsync<Ciudadano>().Wait();
        }
        public Task<List<Ciudadano>> GetItemsAsync()
        {
            return database.Table<Ciudadano>().ToListAsync();
        }
          
        public async Task<int> AddCiudadano(Ciudadano input)
        {
            var output = await database.Table<Ciudadano>().Where(i => i.NumeroDocumento == input.NumeroDocumento && i.TipoDocumento == input.TipoDocumento).CountAsync();

            if (output>0)
            {
                return await database.UpdateAsync(input);
            }
            else
            {
                return await database.InsertAsync(input);
            }
         
        }
        public async Task<int> UpdateCiudadano(Ciudadano input)
        {
            var output = await database.Table<Ciudadano>().Where(i => i.NumeroDocumento == input.NumeroDocumento && i.TipoDocumento == input.TipoDocumento).CountAsync();

            if (output > 0)
            {
                return await database.UpdateAsync(input);
            }
            return -1;
        }
        public async Task<int> UpdateCiudadanoArchivo(Ciudadano input)
        {
            var output = await database.Table<Ciudadano>().Where(i => i.NumeroDocumento == input.NumeroDocumento && i.TipoDocumento == input.TipoDocumento).CountAsync();

            if (output > 0)
            {
                 
                return await database.UpdateAsync(input);
            }
            return -1;
        }
        public Task DeleteCiudadano(Ciudadano input)
        {
            return database.DeleteAsync(input);
        }

        public Task<Ciudadano> GetCiudadano(Ciudadano input)
        {
            return database.Table<Ciudadano>().Where(i => i.NumeroDocumento == input.NumeroDocumento && i.TipoDocumento == input.TipoDocumento).FirstOrDefaultAsync();
        }
        public Task<Ciudadano> Login(Ciudadano input)
        {
            return database.Table<Ciudadano>().Where(i => i.NumeroDocumento == input.NumeroDocumento && i.TipoDocumento == input.TipoDocumento && i.Password == input.Password).FirstOrDefaultAsync();
        }
    
        private static TodoItemDatabase db = null;
        public static TodoItemDatabase getDatabase()
        {
            if (db == null)
            {
                db = new TodoItemDatabase(DependencyService.Get<IDataStoreSql>().GetDatabasePath());
            }
            return db;
        }

    }
}
