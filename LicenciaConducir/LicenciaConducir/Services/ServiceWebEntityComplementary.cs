﻿using System;
using System.Collections.Generic;
using System.Text;
using LicenciaConducir.Lib.MTCEntidadComplementaria;
using LicenciaConducir.Vistas.Lib;

namespace LicenciaConducir.Services
{
   public  interface ServiceWebEntityComplementary
    {
        List<EntidadComplementaria> GetListEComplementaria();
    }
}
