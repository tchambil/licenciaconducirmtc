﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace LicenciaConducir.Services
{
    public interface IDataStoreSql
    {
        void DeleteDatabase();
        void CloseConnection();
        SQLiteConnection GetConnection();
        string GetDatabasePath();
     
    }
}
