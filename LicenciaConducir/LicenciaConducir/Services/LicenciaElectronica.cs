﻿using LicenciaConducir.ViewModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace LicenciaConducir.Services
{
    public class LicenciaElectronica
    {
        private const string UrlServiceWeb = "https://wsmsnc.mtc.gob.pe/";
        public LicenciaElectronica()
        {

        }
        public async Task<LicenciaElectronicaFile> GetFileOutput(string nombre)
        {
            LicenciaElectronicaFile result = new LicenciaElectronicaFile();
            result.codeResultField = "-1";

            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json")); 

                httpClient.BaseAddress = new Uri(UrlServiceWeb);
                var content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string,string>("nombre",nombre)
                });
                var resul = await httpClient.PostAsync("/api/LicenciaElectronica", content);
                var json = await resul.Content.ReadAsStringAsync().ConfigureAwait(false);
                var r = JObject.Parse(json)["consultaDocsRespField"].ToString();
                if (!string.IsNullOrWhiteSpace(r))
                {
                     result = await Task.Run(() =>
                    {
                        return JsonConvert.DeserializeObject<LicenciaElectronicaFile>(r);
                    }).ConfigureAwait(false);
                }
                //return resultContent;
            }
            return result;
        }
    }
}
