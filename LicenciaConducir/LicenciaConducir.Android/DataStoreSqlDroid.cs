﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using LicenciaConducir.Droid;
using LicenciaConducir.Services;
using SQLite;
using Xamarin.Forms;

[assembly: Dependency(typeof(DataStoreSqlDroid))]
namespace LicenciaConducir.Droid
{
    public class DataStoreSqlDroid : IDataStoreSql
    {
        private SQLiteConnectionWithLock _conn;
        public DataStoreSqlDroid()
        {

        }
        public string GetDatabasePath()
        {
            const string sqliteFilename = "LicenciaElectronicas.db";
            string documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal); // Documents folder
            var path = Path.Combine(documentsPath, sqliteFilename);
            return path;
        }
        public SQLiteConnection GetConnection()
        {
            var dbPath = GetDatabasePath(); 
            return new SQLiteConnection(dbPath);
        }
      
        public void DeleteDatabase()
        {
            var path = GetDatabasePath();

            try
            {
                if (_conn != null)
                {
                    _conn.Close();

                }
            }
            catch
            {
                // Best effort close. No need to worry if throws an exception
            }

            if (File.Exists(path))
            {

                File.Delete(path);
            }

            _conn = null;
        }

        public void CloseConnection()
        {
            if (_conn != null)
            {
                _conn.Close();
                _conn.Dispose();
                _conn = null;

                // Must be called as the disposal of the connection is not released until the GC runs.
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }
     
    }
}