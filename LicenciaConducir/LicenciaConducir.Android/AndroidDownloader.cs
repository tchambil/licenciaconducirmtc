﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Webkit;
using Android.Widget;
using LicenciaConducir.Droid;
using LicenciaConducir.Services;
using Xamarin.Essentials;
using Xamarin.Forms;

[assembly: Dependency(typeof(AndroidDownloader))]
namespace LicenciaConducir.Droid
{
    public class AndroidDownloader: FeatureService
    {
        public string CreateFile(string filename, byte[] bytes)
        {

            string pathToNewFolder = Path.Combine(global::Android.OS.Environment.ExternalStorageDirectory.AbsolutePath,"LicenciaElectronica");

            if (!Directory.Exists(pathToNewFolder))
            {
                Directory.CreateDirectory(pathToNewFolder);
            }
            var path = Path.Combine(pathToNewFolder, Path.GetFileName(filename));
            File.WriteAllBytes(path, bytes);    
            return path;
        }

        public void DownloadFile(string url, string folder)
        {
            string pathToNewFolder = Path.Combine(Android.OS.Environment.ExternalStorageDirectory.AbsolutePath, folder);
            Directory.CreateDirectory(pathToNewFolder);
            string pathToNewFile = Path.Combine(pathToNewFolder, Path.GetFileName(url));         
        }

        public string GetFilePath(string filename)
        { 
            string documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal); // Documents folder
            var path = Path.Combine(documentsPath, filename);
            return path;
        }

        public void OpenFile(string filePath)
        {
            var localFile = new Java.IO.File(filePath);   // getting files from path
            var currentContext = Android.App.Application.Context;
            var uri = FileProvider.GetUriForFile(currentContext, "pe.gob.mtc.licenciaConducirMTC", localFile);
            Intent intent = new Intent(Intent.ActionView);
            intent.SetFlags(ActivityFlags.ClearWhenTaskReset | ActivityFlags.NewTask);
            var extension = MimeTypeMap.GetFileExtensionFromUrl(filePath);
            var mimeType = MimeTypeMap.Singleton.GetMimeTypeFromExtension(extension.ToLower());
            intent.AddFlags(ActivityFlags.GrantReadUriPermission);
            intent.SetDataAndType(uri, mimeType);
            currentContext.StartActivity(intent);
        }
        public bool StartMap(string Latitud, string longitud, string name)
        {
            throw new NotImplementedException();
        }              
    }
}