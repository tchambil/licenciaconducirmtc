﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

using Foundation;
using LicenciaConducir.iOS;
using LicenciaConducir.Services;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(IosDownloader))]
namespace LicenciaConducir.iOS
{
    public class IosDownloader : FeatureService
    {
        public string CreateFile(string filename, byte[] bytes)
        {
            string pathToNewFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "files");

            if (!Directory.Exists(pathToNewFolder))
            {
                Directory.CreateDirectory(pathToNewFolder);
            }
            var path = Path.Combine(pathToNewFolder, Path.GetFileName(filename));
            File.WriteAllBytes(path, bytes);
            return path;
        }

        public void DownloadFile(string url, string folder)
        {
            string pathToNewFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), folder);
            Directory.CreateDirectory(pathToNewFolder);

        }

        public string GetFilePath(string filename)
        {
            string documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal); // Documents folder
            var path = Path.Combine(documentsPath, filename);
            return path;
        }

        public void OpenFile(string filePath)
        {
            throw new NotImplementedException();
        }

        public bool StartMap(string Latitud, string longitud, string name)
        {
            throw new NotImplementedException();
        }
    }
}