﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Foundation;
using LicenciaConducir.iOS;
using LicenciaConducir.Services;
using SQLite;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(DataStoreSqlIOS))]
namespace LicenciaConducir.iOS
{
    public class DataStoreSqlIOS : IDataStoreSql
    {
        public void CloseConnection()
        {
            throw new NotImplementedException();
        }

        public void DeleteDatabase()
        {
            throw new NotImplementedException();
        }

        public SQLiteConnection GetConnection()
        {
            throw new NotImplementedException();
        }

        public string GetDatabasePath()
        {
            const string filename = "LicenciaElectronica.db";
            string docFolder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string libFolder = Path.Combine(docFolder, "..", "Library", "Databases");

            if (!Directory.Exists(libFolder))
            {
                Directory.CreateDirectory(libFolder);
            }

            return Path.Combine(libFolder, filename);
        }
    }
}