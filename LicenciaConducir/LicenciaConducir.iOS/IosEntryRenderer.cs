﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoreAnimation;
using Foundation;
using LicenciaConducir.iOS;
using LicenciaConducir.Vistas.Lib;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(BordeEntry), typeof(IosEntryRenderer))]
namespace LicenciaConducir.iOS
{
    public class IosEntryRenderer : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if (Control != null)
            {

                var borderLayer = new CALayer();
                borderLayer.MasksToBounds = false;
                //borderLayer.Frame = new CoreGraphics.CGRect(0f, Frame.Height / 2, Frame.Width, 1f);
                //borderLayer.Frame = new CoreGraphics.CGRect(0f, Frame.Height / 2, Frame.Width-75, 1f);
                borderLayer.BorderColor = UIColor.FromRGB(205, 207, 208).CGColor;
                borderLayer.BorderWidth = 2f;

                Control.Layer.AddSublayer(borderLayer);
                //  Control.BorderStyle = UITextBorderStyle.None;

                // No auto-correct
                Control.AutocorrectionType = UITextAutocorrectionType.No;
                Control.SpellCheckingType = UITextSpellCheckingType.No;
                Control.AutocapitalizationType = UITextAutocapitalizationType.Words;
                //Control.BorderStyle = UITextBorderStyle.None;

            }
        }
    }
}