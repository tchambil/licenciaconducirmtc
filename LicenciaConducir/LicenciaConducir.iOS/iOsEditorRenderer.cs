﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoreAnimation;
using Foundation;
using LicenciaConducir.iOS;
using LicenciaConducir.Vistas.Lib;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(BordeEditor), typeof(iOsEditorRenderer))]
namespace LicenciaConducir.iOS
{
    public class iOsEditorRenderer : EditorRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {
            base.OnElementChanged(e);
            if (Control != null)
            {

                var borderLayer = new CALayer();
                borderLayer.MasksToBounds = true;
                borderLayer.Frame = new CoreGraphics.CGRect(0f, Frame.Height / 2, Frame.Width, 1f);
                borderLayer.BorderColor = UIColor.FromRGB(134, 136, 137).CGColor;
                borderLayer.BorderWidth = 1f;
                Control.Layer.BorderColor = UIColor.FromRGB(134, 136, 137).CGColor;
                Control.Layer.BorderWidth = 1f;
                Control.Layer.AddSublayer(borderLayer);
            }
        }
    }
}